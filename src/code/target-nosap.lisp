;;;; target machine without System Area Pointers (SAPs)

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!KERNEL")

;;; Return T iff the SAP X points to a smaller address then the SAP Y.
(defun sap< (x y)
  (declare (type system-area-pointer x y))
  (sap< x y))

;;; Return T iff the SAP X points to a smaller or the same address as
;;; the SAP Y.
(defun sap<= (x y)
  (declare (type system-area-pointer x y))
  (sap<= x y))

;;; Return T iff the SAP X points to the same address as the SAP Y.
(defun sap= (x y)
  (declare (type system-area-pointer x y))
  (sap= x y))

;;; Return T iff the SAP X points to a larger or the same address as
;;; the SAP Y.
(defun sap>= (x y)
  (declare (type system-area-pointer x y))
  (sap>= x y))

;;; Return T iff the SAP X points to a larger address then the SAP Y.
(defun sap> (x y)
  (declare (type system-area-pointer x y))
  (sap> x y))

;;; Return a new SAP, OFFSET bytes from SAP.
(defun sap+ (sap offset)
  (declare (type system-area-pointer sap)
           (type (signed-byte #.sb!vm:n-word-bits) offset))
  (sap+ sap offset))

;;; Return the byte offset between SAP1 and SAP2.
(defun sap- (sap1 sap2)
  (declare (type system-area-pointer sap1 sap2))
  (sap- sap1 sap2))

;;; Convert SAP into an integer.
(defun sap-int (sap)
  (declare (type system-area-pointer sap))
  (sap-int sap))

;;; Convert an integer into a SAP.
(defun int-sap (int)
  (declare (type sap-int int))
  (int-sap int))
