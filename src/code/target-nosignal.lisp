;;;; stubs replacing target-signal.lisp for AVM2, which has no signals

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!UNIX")


;;;; etc.

(defmacro with-interrupt-bindings (&body body)
  (with-unique-names (empty)
    `(let*
         ;; KLUDGE: Whatever is on the PCL stacks before the interrupt
         ;; handler runs doesn't really matter, since we're not on the
         ;; same call stack, really -- and if we don't bind these (esp.
         ;; the cache one) we can get a bogus metacircle if an interrupt
         ;; handler calls a GF that was being computed when the interrupt
         ;; hit.
         ((sb!pcl::*cache-miss-values-stack* nil)
          (sb!pcl::*dfun-miss-gfs-on-stack* nil)
          ;; Unless we do this, ADJUST-ARRAY and SORT would need to
          ;; disable interrupts.
          (,empty (vector))
          (sb!impl::*zap-array-data-temp* ,empty)
          (sb!impl::*merge-sort-temp-vector* ,empty))
       ,@body)))

(defmacro in-interruption ((&key) &body body)
  #!+sb-doc
  "Convenience macro on top of INVOKE-INTERRUPTION."
  `(dx-flet ((interruption () ,@body))
     (invoke-interruption #'interruption)))

(defun invoke-interruption (function)
  (progn ;without-interrupts
    (with-interrupt-bindings
      (progn ;allow-with-interrupts
        (funcall function)))))

;;; CMU CL comment:
;;;   Magically converted by the compiler into a break instruction.
;;; SBCL/Win32 comment:
;;;   I don't know if we still need this or not. Better safe for now.
(defun receive-pending-interrupt ()
  (receive-pending-interrupt))
