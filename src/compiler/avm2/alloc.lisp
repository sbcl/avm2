;;;; allocation VOPs for the x86

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; CONS, LIST and LIST*
(define-vop (list-or-list*)
  (:args (things :more t))
  (:temporary (:sc unsigned-reg) previous-cons)
  (:info num)
  (:results (result :scs (descriptor-reg)))
  (:variant-vars star)
  (:policy :safe)
  (:generator 0
    (cond ((zerop num)
           (%find-property "getNil")
           (%call-property "getNil" 0))
          ((and star (= num 1))
           (avm2 :get-local/object (tn-ref-tn things))
           (avm2 :set-local result))
          (t
           (macrolet
               ((store-car (tn list)
                  `(progn
                     (%find-property "setCar")
                     (avm2 :get-local/object ,list)
                     (avm2 :get-local/object ,tn)
                     (%call-property "setCar" 2)))
                (store-cdr (tn list)
                  `(progn
                     (%find-property "setCdr")
                     (avm2 :get-local/object ,list)
                     (avm2 :get-local/object ,tn)
                     (%call-property "setCdr" 2))))
             (let ((cons-cells (if star (1- num) num)))
               (dotimes (i (if star
                               (1- cons-cells)
                               cons-cells))
                 ;; stack: .... (empty)
                 (%find-property "cons1")
                 (avm2 :get-local/object (tn-ref-tn things))
                 (setf things (tn-ref-across things))
                 (%call-property "cons1" 1)
                 ;; stack: .... CURRENT-CONS
                 (cond
                   ((zerop i)
                    (avm2 :dup)
                    (avm2 :set-local result))
                   (t
                    (avm2 :dup)
                                        ;CURRENT-CONS CURRENT-CONS
                    (%find-property "setCdr")
                                        ;CURRENT-CONS CURRENT-CONS <fn>
                    (avm2 :swap)
                                        ;CURRENT-CONS <fn> CURRENT-CONS
                    (avm2 :get-local/object previous-cons)
                                        ;CURRENT-CONS <fn> CURRENT-CONS PREV
                    (avm2 :swap)
                                        ;CURRENT-CONS <fn> PREV CURRENT-CONS
                    (%call-property "setCdr" 2)
                                        ;CURRENT-CONS
                    ))
                 (avm2 :set-local previous-cons))
               (when star
                 (%find-property "setCdr")
                 (avm2 :get-local/object previous-cons)
                 (setf things (tn-ref-across things))
                 (avm2 :get-local/object (tn-ref-tn things))
                 (%call-property "setCdr" 2))))))))

(define-vop (list list-or-list*)
  (:variant nil))

(define-vop (list* list-or-list*)
  (:variant t))

;;;; special-purpose inline allocators

;;; ALLOCATE-VECTOR
(define-vop (allocate-vector)
  (:translate allocate-vector)
  (:args (type :scs (#+(or) any-reg unsigned-reg))
         (length :scs (#+(or) any-reg unsigned-reg))
         (words :scs (any-reg immediate)))
  (:results (result :scs (descriptor-reg) :from :load))
  (:ignore words)
  (:arg-types positive-fixnum positive-fixnum positive-fixnum)
  (:policy :fast-safe)
  (:generator 100
    (%find-property "makeVector")
    (avm2 :get-local/unsigned type)
    (avm2 :get-local/unsigned length)
    (%call-property "makeVector" 2)
    (avm2 :set-local result)))

(define-vop (make-fdefn)
  (:policy :fast-safe)
  (:translate make-fdefn)
  (:args (name :scs (descriptor-reg) :to :eval))
  (:results (result :scs (descriptor-reg) :from :argument))
  (:generator 37
    (%find-property "makeFdefn")
    (avm2 :get-local/object name)
    (%call-property "makeFdefn" 1)
    (avm2 :set-local result)))

(define-vop (make-closure)
  (:args (function :to :save :scs (descriptor-reg)))
  (:info length stack-allocate-p)
  (:results (result :scs (descriptor-reg)))
  (:ignore stack-allocate-p)
  (:generator 10
    (%find-property "makeClosure")
    (avm2 :get-local/object function)
    (avm2 :push-int length)
    (%call-property "makeClosure" 2)
    (avm2 :set-local result)))

;;; The compiler likes to be able to directly make value cells.
(define-vop (make-value-cell)
  (:args (value :scs (descriptor-reg any-reg) :to :result))
  (:results (result :scs (descriptor-reg) :from :eval))
  (:info stack-allocate-p)
  (:ignore stack-allocate-p)
  (:generator 10
    (%find-property "makeValueCall")
    (avm2 :get-local/object value)
    (%call-property "makeValueCall" 1)
    (avm2 :set-local result)))

;;;; automatic allocators for primitive objects

(define-vop (make-unbound-marker)
  (:args)
  (:results (result :scs (any-reg)))
  (:ignore result)
  (:generator 1
    (avm2 :comment "fixme: make-unbound-marker")
    (avm2 :throw)))

(define-vop (make-funcallable-instance-tramp)
  (:args)
  (:results (result :scs (any-reg)))
  (:ignore result)
  (:generator 1
    (avm2 :comment "fixme: make-funcallable-instance-tramp")
    (avm2 :throw)))

(define-vop (fixed-alloc)
  (:args)
  (:info name words type lowtag stack-allocate-p)
  (:ignore name stack-allocate-p)
  (:results (result :scs (descriptor-reg)))
  (:generator 50
    (case lowtag
      (#.list-pointer-lowtag
       (%find-property "makeCons")
       (%call-property "makeCons" 0))
      (t
       (aver type)
       (%find-property "makeObjectWithWidetag")
       (avm2 :push-int type)
       (avm2 :push-int words)
       (%call-property "makeObjectWithWidetag" 2)))
    (avm2 :set-local result)))

(define-vop (var-alloc)
  (:args (extra :scs (any-reg)))
  (:arg-types positive-fixnum)
  (:info name words type lowtag)
  (:ignore name lowtag)
  (:results (result :scs (descriptor-reg) :from (:eval 1)))
  (:generator 50
    (%find-property "makeObjectWithWidetag")
    (avm2 :push-int type)
    (avm2 :push-int words)
    (avm2 :get-local/object extra)
    (avm2 :add-i)
    (%call-property "makeObjectWithWidetag" 2)
    (avm2 :set-local result)))
