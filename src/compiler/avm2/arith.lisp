;;;; the VM definition of arithmetic VOPs for the x86

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; unary operations

(define-vop (fast-safe-arith-op)
  (:policy :fast-safe)
  (:effects)
  (:affected))

(define-vop (fixnum-unop fast-safe-arith-op)
  (:args (x :scs (any-reg) :target res))
  (:results (res :scs (any-reg)))
  (:note "inline fixnum arithmetic")
  (:arg-types tagged-num)
  (:result-types tagged-num))

(define-vop (signed-unop fast-safe-arith-op)
  (:args (x :scs (signed-reg) :target res))
  (:results (res :scs (signed-reg)))
  (:note "inline (signed-byte 32) arithmetic")
  (:arg-types signed-num)
  (:result-types signed-num))

(defun qname (a b)
  (check-type a string)
  (check-type b string)
  `(:qname ,a ,b))

(defun multiname-l (&rest args)
  `(:multiname-l ,@args))

(defun %unbox-fixnum (&optional from-local-variable)
  (cond
    (from-local-variable
     ;; (%find-property "fixnumValue")
     (avm2 :get-local/object from-local-variable)
     (%get-property "value"))
    (t
     ;; (%find-property "fixnumValue")
     (avm2 :swap)
     (%get-property "value"))))

(defun %box-fixnum (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "boxFixnum")
     (avm2 :get-local/object from-local-variable)
     (%call-property "boxFixnum" 1))
    (t
     (%find-property "boxFixnum")
     (avm2 :swap)
     (%call-property "boxFixnum" 1))))

(defun %unbox-character (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "characterValue")
     (avm2 :get-local/object from-local-variable)
     (%get-property "characterValue"))
    (t
     (%find-property "characterValue")
     (avm2 :swap)
     (%get-property "characterValue"))))

(defun %box-character (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "boxCharacter")
     (avm2 :get-local/object from-local-variable)
     (%call-property "boxCharacter" 1))
    (t
     (%find-property "boxCharacter")
     (avm2 :swap)
     (%call-property "boxCharacter" 1))))

(defun %unbox-float (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "floatValue")
     (avm2 :get-local/object from-local-variable)
     (%get-property "floatValue"))
    (t
     (%find-property "floatValue")
     (avm2 :swap)
     (%get-property "floatValue"))))

(defun %box-float (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "boxFloat")
     (avm2 :get-local/object from-local-variable)
     (%call-property "boxFloat" 1))
    (t
     (%find-property "boxFloat")
     (avm2 :swap)
     (%call-property "boxFloat" 1))))

(defun %unbox-complex (&optional from-local-variable)
  (cond
    (from-local-variable
     (%find-property "realValue")
     (avm2 :get-local/object from-local-variable)
     (%get-property "realValue")
     (%find-property "imagValue")
     (avm2 :get-local/object from-local-variable)
     (%get-property "imagValue"))
    (t
     (avm2 :dup)
     (%find-property "realValue")
     (avm2 :swap)
     (%get-property "realValue")
     (avm2 :swap)
     (%find-property "imagValue")
     (avm2 :swap)
     (%get-property "imagValue"))))

(defun %box-complex (real-variable imag-variable)
  (aver (and real-variable imag-variable))
  (%find-property "boxComplex")
  (avm2 :get-local/object real-variable)
  (avm2 :get-local/object imag-variable)
  (%call-property "boxComplex" 2))

(define-vop (fast-negate/fixnum fixnum-unop)
  (:translate %negate)
  (:generator 1
    (%unbox-fixnum x)
    (avm2 :negate-i)
    (%box-fixnum)
    (avm2 :set-local res)))

(define-vop (fast-negate/signed signed-unop)
  (:translate %negate)
  (:generator 2
    (avm2 :get-local/signed x)
    (avm2 :negate-i)
    (avm2 :set-local res)))

(define-vop (fast-lognot/fixnum fixnum-unop)
  (:translate lognot)
  (:generator 1
    (%unbox-fixnum x)
    (avm2 :bit-not)
    (%box-fixnum)
    (avm2 :set-local res)))

(define-vop (fast-lognot/signed signed-unop)
  (:translate lognot)
  (:generator 2
    (avm2 :get-local/signed x)
    (avm2 :bit-not)
    (avm2 :set-local res)))

;;;; binary fixnum operations

;;; Assume that any constant operand is the second arg...

(define-vop (fast-fixnum-binop fast-safe-arith-op)
  (:args (x :target r :scs (any-reg)
            :load-if (not (and (sc-is x control-stack)
                               (sc-is y any-reg)
                               (sc-is r control-stack)
                               (location= x r))))
         (y :scs (any-reg control-stack)))
  (:arg-types tagged-num tagged-num)
  (:results (r :scs (any-reg) :from (:argument 0)
               :load-if (not (and (sc-is x control-stack)
                                  (sc-is y any-reg)
                                  (sc-is r control-stack)
                                  (location= x r)))))
  (:result-types tagged-num)
  (:note "inline fixnum arithmetic"))

(define-vop (fast-unsigned-binop fast-safe-arith-op)
  (:args (x :target r :scs (unsigned-reg)
            :load-if (not (and (sc-is x unsigned-stack)
                               (sc-is y unsigned-reg)
                               (sc-is r unsigned-stack)
                               (location= x r))))
         (y :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num)
  (:results (r :scs (unsigned-reg) :from (:argument 0)
            :load-if (not (and (sc-is x unsigned-stack)
                               (sc-is y unsigned-reg)
                               (sc-is r unsigned-stack)
                               (location= x r)))))
  (:result-types unsigned-num)
  (:note "inline (unsigned-byte 32) arithmetic"))

(define-vop (fast-signed-binop fast-safe-arith-op)
  (:args (x :target r :scs (signed-reg)
            :load-if (not (and (sc-is x signed-stack)
                               (sc-is y signed-reg)
                               (sc-is r signed-stack)
                               (location= x r))))
         (y :scs (signed-reg signed-stack)))
  (:arg-types signed-num signed-num)
  (:results (r :scs (signed-reg) :from (:argument 0)
            :load-if (not (and (sc-is x signed-stack)
                               (sc-is y signed-reg)
                               (sc-is r signed-stack)
                               (location= x r)))))
  (:result-types signed-num)
  (:note "inline (signed-byte 32) arithmetic"))

(define-vop (fast-fixnum-binop-c fast-safe-arith-op)
  (:args (x :target r :scs (any-reg control-stack)))
  (:info y)
  (:arg-types tagged-num (:constant (signed-byte 30)))
  (:results (r :scs (any-reg)
               :load-if (not (location= x r))))
  (:result-types tagged-num)
  (:note "inline fixnum arithmetic"))

(define-vop (fast-unsigned-binop-c fast-safe-arith-op)
  (:args (x :target r :scs (unsigned-reg unsigned-stack)))
  (:info y)
  (:arg-types unsigned-num (:constant (unsigned-byte 32)))
  (:results (r :scs (unsigned-reg)
               :load-if (not (location= x r))))
  (:result-types unsigned-num)
  (:note "inline (unsigned-byte 32) arithmetic"))

(define-vop (fast-signed-binop-c fast-safe-arith-op)
  (:args (x :target r :scs (signed-reg signed-stack)))
  (:info y)
  (:arg-types signed-num (:constant (signed-byte 32)))
  (:results (r :scs (signed-reg)
               :load-if (not (location= x r))))
  (:result-types signed-num)
  (:note "inline (signed-byte 32) arithmetic"))

(macrolet ((define-binop (translate untagged-penalty op)
             `(progn
                (define-vop (,(symbolicate "FAST-" translate "/FIXNUM=>FIXNUM")
                             fast-fixnum-binop)
                  (:translate ,translate)
                  (:generator 2
                    (%unbox-fixnum x)
                    (%unbox-fixnum y)
                    (avm2 ,op)
                    (%box-fixnum)
                    (avm2 :set-local r)))
                (define-vop (,(symbolicate 'fast- translate '-c/fixnum=>fixnum)
                             fast-fixnum-binop-c)
                  (:translate ,translate)
                  (:generator 1
                    (%unbox-fixnum x)
                    (avm2 :push-int y)
                    (avm2 ,op)
                    (%box-fixnum)
                    (avm2 :set-local r)))
                (define-vop (,(symbolicate "FAST-" translate "/SIGNED=>SIGNED")
                             fast-signed-binop)
                  (:translate ,translate)
                  (:generator ,(1+ untagged-penalty)
                    (avm2 :get-local/signed x)
                    (avm2 :get-local/signed y)
                    (avm2 ,op)
                    (avm2 :set-local r)))
                (define-vop (,(symbolicate 'fast- translate '-c/signed=>signed)
                             fast-signed-binop-c)
                  (:translate ,translate)
                  (:generator ,untagged-penalty
                    (avm2 :get-local/signed x)
                    (avm2 :push-int y)
                    (avm2 ,op)
                    (avm2 :set-local r)))
                (define-vop (,(symbolicate "FAST-"
                                           translate
                                           "/UNSIGNED=>UNSIGNED")
                fast-unsigned-binop)
                  (:translate ,translate)
                  (:generator ,(1+ untagged-penalty)
                    (avm2 :get-local/unsigned x)
                    (avm2 :get-local/unsigned y)
                    (avm2 ,op)
                    (avm2 :set-local r)))
                (define-vop (,(symbolicate 'fast-
                                           translate
                                           '-c/unsigned=>unsigned)
                             fast-unsigned-binop-c)
                  (:translate ,translate)
                  (:generator ,untagged-penalty
                  (avm2 :get-local/unsigned x)
                  (avm2 :push-uint y)
                  (avm2 ,op)
                  (avm2 :set-local r))))))
  (define-binop + 4 :add-i)
  (define-binop - 4 :subtract-i)
  (define-binop logand 2 :bit-and)
  (define-binop logior 2 :bit-or)
  (define-binop logxor 2 :bit-xor))

;; ;;; Special handling of add on the x86; can use lea to avoid a
;; ;;; register load, otherwise it uses add.
;; (define-vop (fast-+/fixnum=>fixnum fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :scs (any-reg) :target r
;;             :load-if (not (and (sc-is x control-stack)
;;                                (sc-is y any-reg)
;;                                (sc-is r control-stack)
;;                                (location= x r))))
;;          (y :scs (any-reg control-stack)))
;;   (:arg-types tagged-num tagged-num)
;;   (:results (r :scs (any-reg) :from (:argument 0)
;;                :load-if (not (and (sc-is x control-stack)
;;                                   (sc-is y any-reg)
;;                                   (sc-is r control-stack)
;;                                   (location= x r)))))
;;   (:result-types tagged-num)
;;   (:note "inline fixnum arithmetic")
;;   (:generator 2
;;     (cond ((and (sc-is x any-reg) (sc-is y any-reg) (sc-is r any-reg)
;;                 (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :index y :scale 1)))
;;           (t
;;            (move r x)
;;            (x86inst add r y)))))

;; (define-vop (fast-+-c/fixnum=>fixnum fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :target r :scs (any-reg control-stack)))
;;   (:info y)
;;   (:arg-types tagged-num (:constant (signed-byte 30)))
;;   (:results (r :scs (any-reg)
;;                :load-if (not (location= x r))))
;;   (:result-types tagged-num)
;;   (:note "inline fixnum arithmetic")
;;   (:generator 1
;;     (cond ((and (sc-is x any-reg) (sc-is r any-reg) (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :disp (fixnumize y))))
;;           (t
;;            (move r x)
;;            (x86inst add r (fixnumize y))))))

;; (define-vop (fast-+/signed=>signed fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :scs (signed-reg) :target r
;;             :load-if (not (and (sc-is x signed-stack)
;;                                (sc-is y signed-reg)
;;                                (sc-is r signed-stack)
;;                                (location= x r))))
;;          (y :scs (signed-reg signed-stack)))
;;   (:arg-types signed-num signed-num)
;;   (:results (r :scs (signed-reg) :from (:argument 0)
;;                :load-if (not (and (sc-is x signed-stack)
;;                                   (sc-is y signed-reg)
;;                                   (location= x r)))))
;;   (:result-types signed-num)
;;   (:note "inline (signed-byte 32) arithmetic")
;;   (:generator 5
;;     (cond ((and (sc-is x signed-reg) (sc-is y signed-reg) (sc-is r signed-reg)
;;                 (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :index y :scale 1)))
;;           (t
;;            (move r x)
;;            (x86inst add r y)))
;;     (avm2 `(:get-local ,x))
;;     (avm2 `(:get-local ,y))
;;     (avm2 `(:add-i))
;;     (avm2 `(:set-local ,r))))

;;;; Special logand cases: (logand signed unsigned) => unsigned

(define-vop (fast-logand/signed-unsigned=>unsigned
             fast-logand/unsigned=>unsigned)
  (:args (x :target r :scs (signed-reg)
            :load-if (not (and (sc-is x signed-stack)
                               (sc-is y unsigned-reg)
                               (sc-is r unsigned-stack)
                               (location= x r))))
         (y :scs (unsigned-reg unsigned-stack)))
  (:arg-types signed-num unsigned-num))

(define-vop (fast-logand-c/signed-unsigned=>unsigned
             fast-logand-c/unsigned=>unsigned)
  (:args (x :target r :scs (signed-reg signed-stack)))
  (:arg-types signed-num (:constant (unsigned-byte 32))))

(define-vop (fast-logand/unsigned-signed=>unsigned
             fast-logand/unsigned=>unsigned)
  (:args (x :target r :scs (unsigned-reg)
            :load-if (not (and (sc-is x unsigned-stack)
                               (sc-is y signed-reg)
                               (sc-is r unsigned-stack)
                               (location= x r))))
         (y :scs (signed-reg signed-stack)))
  (:arg-types unsigned-num signed-num))


;; (define-vop (fast-+-c/signed=>signed fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :target r :scs (signed-reg signed-stack)))
;;   (:info y)
;;   (:arg-types signed-num (:constant (signed-byte 32)))
;;   (:results (r :scs (signed-reg)
;;                :load-if (not (location= x r))))
;;   (:result-types signed-num)
;;   (:note "inline (signed-byte 32) arithmetic")
;;   (:generator 4
;;     (cond ((and (sc-is x signed-reg) (sc-is r signed-reg)
;;                 (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :disp y)))
;;           (t
;;            (move r x)
;;            (if (= y 1)
;;                (x86inst inc r)
;;              (x86inst add r y))))))

;; (define-vop (fast-+/unsigned=>unsigned fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :scs (unsigned-reg) :target r
;;             :load-if (not (and (sc-is x unsigned-stack)
;;                                (sc-is y unsigned-reg)
;;                                (sc-is r unsigned-stack)
;;                                (location= x r))))
;;          (y :scs (unsigned-reg unsigned-stack)))
;;   (:arg-types unsigned-num unsigned-num)
;;   (:results (r :scs (unsigned-reg) :from (:argument 0)
;;                :load-if (not (and (sc-is x unsigned-stack)
;;                                   (sc-is y unsigned-reg)
;;                                   (sc-is r unsigned-stack)
;;                                   (location= x r)))))
;;   (:result-types unsigned-num)
;;   (:note "inline (unsigned-byte 32) arithmetic")
;;   (:generator 5
;;     (cond ((and (sc-is x unsigned-reg) (sc-is y unsigned-reg)
;;                 (sc-is r unsigned-reg) (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :index y :scale 1)))
;;           (t
;;            (move r x)
;;            (x86inst add r y)))
;;     (avm2 `(:get-local ,x))
;;     (avm2 `(:get-local ,y))
;;     (avm2 `(:add-i))
;;     (avm2 `(:set-local ,r))))

;; (define-vop (fast-+-c/unsigned=>unsigned fast-safe-arith-op)
;;   (:translate +)
;;   (:args (x :target r :scs (unsigned-reg unsigned-stack)))
;;   (:info y)
;;   (:arg-types unsigned-num (:constant (unsigned-byte 32)))
;;   (:results (r :scs (unsigned-reg)
;;                :load-if (not (location= x r))))
;;   (:result-types unsigned-num)
;;   (:note "inline (unsigned-byte 32) arithmetic")
;;   (:generator 4
;;     (cond ((and (sc-is x unsigned-reg) (sc-is r unsigned-reg)
;;                 (not (location= x r)))
;;            (x86inst lea r (make-ea :dword :base x :disp y)))
;;           (t
;;            (move r x)
;;            (if (= y 1)
;;                (x86inst inc r)
;;              (x86inst add r y))))))

;;;; multiplication and division

(define-vop (fast-*/fixnum=>fixnum fast-safe-arith-op)
  (:translate *)
  ;; We need different loading characteristics.
  (:args (x :scs (any-reg) :target r)
         (y :scs (any-reg control-stack)))
  (:arg-types tagged-num tagged-num)
  (:results (r :scs (any-reg) :from (:argument 0)))
  (:result-types tagged-num)
  (:note "inline fixnum arithmetic")
  (:generator 4
    (%unbox-fixnum x)
    (%unbox-fixnum y)
    (avm2 :multiply-i)
    (%box-fixnum)
    (avm2 :set-local r)))

(define-vop (fast-*-c/fixnum=>fixnum fast-safe-arith-op)
  (:translate *)
  ;; We need different loading characteristics.
  (:args (x :scs (any-reg control-stack)))
  (:info y)
  (:arg-types tagged-num (:constant (signed-byte 30)))
  (:results (r :scs (any-reg)))
  (:result-types tagged-num)
  (:note "inline fixnum arithmetic")
  (:generator 3
    (%unbox-fixnum x)
    (avm2 :push-int y)
    (avm2 :multiply-i)
    (%box-fixnum)
    (avm2 :set-local r)))

(define-vop (fast-*/signed=>signed fast-safe-arith-op)
  (:translate *)
  ;; We need different loading characteristics.
  (:args (x :scs (signed-reg) :target r)
         (y :scs (signed-reg signed-stack)))
  (:arg-types signed-num signed-num)
  (:results (r :scs (signed-reg) :from (:argument 0)))
  (:result-types signed-num)
  (:note "inline (signed-byte 32) arithmetic")
  (:generator 5
    (avm2 :get-local/signed x)
    (avm2 :get-local/signed y)
    (avm2 :multiply-i)
    (avm2 :set-local r)))

(define-vop (fast-*-c/signed=>signed fast-safe-arith-op)
  (:translate *)
  ;; We need different loading characteristics.
  (:args (x :scs (signed-reg signed-stack)))
  (:info y)
  (:arg-types signed-num (:constant (signed-byte 32)))
  (:results (r :scs (signed-reg)))
  (:result-types signed-num)
  (:note "inline (signed-byte 32) arithmetic")
  (:generator 4
    (avm2 :get-local/signed x)
    (avm2 :push-int y)
    (avm2 :multiply-i)
    (avm2 :set-local r)))

(define-vop (fast-*/unsigned=>unsigned fast-safe-arith-op)
  (:translate *)
  (:args (x :scs (unsigned-reg))
         (y :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num)
  (:results (r :scs (unsigned-reg)))
  (:result-types unsigned-num)
  (:note "inline (unsigned-byte 32) arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 6
    (avm2 :get-local/unsigned x)
    (avm2 :push-uint y)
    (avm2 :multiply-i)
    (avm2 :set-local r)))


(define-vop (fast-truncate/fixnum=>fixnum fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (any-reg))
         (y :scs (any-reg control-stack)))
  (:arg-types tagged-num tagged-num)
  (:results (quo :scs (any-reg))
            (rem :scs (any-reg)))
  (:result-types tagged-num tagged-num)
  (:note "inline fixnum arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:temporary (:sc unsigned-reg) unboxed-y)
  (:generator 31
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (%unbox-fixnum x)
    (avm2 :dup)

    (%unbox-fixnum y)
    (avm2 :dup)
    (avm2 :set-local unboxed-y)
    (avm2 :divide)
    (%box-fixnum)
    (avm2 :set-local quo)

    (avm2 :get-local/nosign unboxed-y)
    (avm2 :modulo)
    (%box-fixnum)
    (avm2 :set-local rem)))

(define-vop (fast-truncate-c/fixnum=>fixnum fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (any-reg)))
  (:info y)
  (:arg-types tagged-num (:constant (signed-byte 30)))
  (:results (quo :scs (any-reg))
            (rem :scs (any-reg)))
  (:result-types tagged-num tagged-num)
  (:note "inline fixnum arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 30
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (%unbox-fixnum x)
    (avm2 :dup)

    (avm2 :push-int y)
    (avm2 :divide)
    (%box-fixnum)
    (avm2 :set-local quo)

    (avm2 :push-int y)
    (avm2 :modulo)
    (%box-fixnum)
    (avm2 :set-local rem)))

(define-vop (fast-truncate/unsigned=>unsigned fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (unsigned-reg))
         (y :scs (unsigned-reg signed-stack)))
  (:arg-types unsigned-num unsigned-num)
  (:results (quo :scs (unsigned-reg))
            (rem :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:note "inline (unsigned-byte 32) arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 33
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 :divide)
    (avm2 :set-local quo)

    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 :modulo)
    (avm2 :set-local rem)))

(define-vop (fast-truncate-c/unsigned=>unsigned fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (unsigned-reg)))
  (:info y)
  (:arg-types unsigned-num (:constant (unsigned-byte 32)))
  (:results (quo :scs (unsigned-reg))
            (rem :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:note "inline (unsigned-byte 32) arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 32
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (avm2 :get-local/unsigned x)
    (avm2 :push-uint y)
    (avm2 :divide)
    (avm2 :set-local quo)

    (avm2 :get-local/unsigned x)
    (avm2 :push-uint y)
    (avm2 :modulo)
    (avm2 :set-local rem)))

(define-vop (fast-truncate/signed=>signed fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (signed-reg))
         (y :scs (signed-reg signed-stack)))
  (:arg-types signed-num signed-num)
  (:results (quo :scs (signed-reg))
            (rem :scs (signed-reg)))
  (:result-types signed-num signed-num)
  (:note "inline (signed-byte 32) arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 33
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (avm2 :get-local/signed x)
    (avm2 :get-local/signed y)
    (avm2 :divide)
    (avm2 :set-local quo)

    (avm2 :get-local/signed x)
    (avm2 :get-local/signed y)
    (avm2 :modulo)
    (avm2 :set-local rem)))

(define-vop (fast-truncate-c/signed=>signed fast-safe-arith-op)
  (:translate truncate)
  (:args (x :scs (signed-reg)))
  (:info y)
  (:arg-types signed-num (:constant (signed-byte 32)))
  (:results (quo :scs (signed-reg))
            (rem :scs (signed-reg)))
  (:result-types signed-num signed-num)
  (:note "inline (signed-byte 32) arithmetic")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 32
    ;; FIXME: probably incorrect.  review later.
    ;;
    ;; also, signal division-by-zero-error
    (avm2 :get-local/signed x)
    (avm2 :push-int y)
    (avm2 :divide)
    (avm2 :set-local quo)

    (avm2 :get-local/signed x)
    (avm2 :push-int y)
    (avm2 :modulo)
    (avm2 :set-local rem)))



;;;; Shifting
(define-vop (fast-ash-c/fixnum=>fixnum)
  (:translate ash)
  (:policy :fast-safe)
  (:args (number :scs (any-reg) :target result
                 :load-if (not (and (sc-is number any-reg control-stack)
                                    (sc-is result any-reg control-stack)
                                    (location= number result)))))
  (:info amount)
  (:arg-types tagged-num (:constant integer))
  (:results (result :scs (any-reg)
                    :load-if (not (and (sc-is number control-stack)
                                       (sc-is result control-stack)
                                       (location= number result)))))
  (:result-types tagged-num)
  (:note "inline ASH")
  (:generator 2
    (cond ((< -32 amount 32)
           ;; this code is used both in ASH and ASH-SMOD30, so
           ;; be careful
           (%unbox-fixnum number)
           (cond
             ((plusp amount)
              (avm2 :push-int amount)
              (avm2 :lshift))
             (t
              (avm2 :push-int (- amount))
              (avm2 :rshift))))
          ((plusp amount)
           (avm2 :push-int 0))
          (t
           (%unbox-fixnum number)
           (avm2 :push-int 31)
           (avm2 :rshift)))
    (%box-fixnum)
    (avm2 :set-local result)))

(define-vop (fast-ash-left/fixnum=>fixnum)
  (:translate ash)
  (:args (number :scs (any-reg) :target result
                 :load-if (not (and (sc-is number control-stack)
                                    (sc-is result control-stack)
                                    (location= number result))))
         (amount :scs (unsigned-reg)))
  (:arg-types tagged-num positive-fixnum)
  (:results (result :scs (any-reg) :from (:argument 0)
                    :load-if (not (and (sc-is number control-stack)
                                       (sc-is result control-stack)
                                       (location= number result)))))
  (:result-types tagged-num)
  (:policy :fast-safe)
  (:note "inline ASH")
  (:generator 3
    (%unbox-fixnum number)
    ;; The result-type ensures us that this shift will not overflow.
    (avm2 :get-local/unsigned amount)
    (avm2 :lshift)
    (%box-fixnum)
    (avm2 :set-local result)))

(define-vop (fast-ash-c/signed=>signed)
  (:translate ash)
  (:policy :fast-safe)
  (:args (number :scs (signed-reg) :target result
                 :load-if (not (and (sc-is number signed-stack)
                                    (sc-is result signed-stack)
                                    (location= number result)))))
  (:info amount)
  (:arg-types signed-num (:constant integer))
  (:results (result :scs (signed-reg)
                    :load-if (not (and (sc-is number signed-stack)
                                       (sc-is result signed-stack)
                                       (location= number result)))))
  (:result-types signed-num)
  (:note "inline ASH")
  (:generator 3
    (avm2 :get-local/signed number)
    (cond
      ((plusp amount)
       (avm2 :push-int amount)
       (avm2 :lshift))
      (t
       (avm2 :push-int (min 31 (- amount)))
       (avm2 :rshift)))
    (avm2 :set-local result)))

(define-vop (fast-ash-c/unsigned=>unsigned)
  (:translate ash)
  (:policy :fast-safe)
  (:args (number :scs (unsigned-reg) :target result
                 :load-if (not (and (sc-is number unsigned-stack)
                                    (sc-is result unsigned-stack)
                                    (location= number result)))))
  (:info amount)
  (:arg-types unsigned-num (:constant integer))
  (:results (result :scs (unsigned-reg)
                    :load-if (not (and (sc-is number unsigned-stack)
                                       (sc-is result unsigned-stack)
                                       (location= number result)))))
  (:result-types unsigned-num)
  (:note "inline ASH")
  (:generator 3
   (cond ((< -32 amount 32)
          ;; this code is used both in ASH and ASH-MOD32, so
          ;; be careful
          (avm2 :get-local/unsigned number)
          (cond
            ((plusp amount)
             (avm2 :push-int amount)
             (avm2 :lshift))
            (t
             (avm2 :push-int (- amount))
             (avm2 :unsigned-rshift)))
          (avm2 :set-local result))
         (t
          (avm2 :push-int 0)
          (avm2 :set-local result)))))

(define-vop (fast-ash-left/signed=>signed)
  (:translate ash)
  (:args (number :scs (signed-reg) :target result
                 :load-if (not (and (sc-is number signed-stack)
                                    (sc-is result signed-stack)
                                    (location= number result))))
         (amount :scs (unsigned-reg)))
  (:arg-types signed-num positive-fixnum)
  (:results (result :scs (signed-reg) :from (:argument 0)
                    :load-if (not (and (sc-is number signed-stack)
                                       (sc-is result signed-stack)
                                       (location= number result)))))
  (:result-types signed-num)
  (:policy :fast-safe)
  (:note "inline ASH")
  (:generator 4
    (avm2 :get-local/signed number)
    (avm2 :get-local/unsigned amount)
    (avm2 :lshift)
    (avm2 :set-local result)))

(define-vop (fast-ash-left/unsigned=>unsigned)
  (:translate ash)
  (:args (number :scs (unsigned-reg) :target result
                 :load-if (not (and (sc-is number unsigned-stack)
                                    (sc-is result unsigned-stack)
                                    (location= number result))))
         (amount :scs (unsigned-reg)))
  (:arg-types unsigned-num positive-fixnum)
  (:results (result :scs (unsigned-reg) :from (:argument 0)
                    :load-if (not (and (sc-is number unsigned-stack)
                                       (sc-is result unsigned-stack)
                                       (location= number result)))))
  (:result-types unsigned-num)
  (:policy :fast-safe)
  (:note "inline ASH")
  (:generator 4
    (avm2 :get-local/unsigned number)
    (avm2 :get-local/unsigned amount)
    (avm2 :lshift)
    (avm2 :set-local result)))

(define-vop (fast-ash/signed=>signed)
  (:translate ash)
  (:policy :fast-safe)
  (:args (number :scs (signed-reg) :target result)
         (amount :scs (signed-reg)))
  (:arg-types signed-num signed-num)
  (:results (result :scs (signed-reg) :from (:argument 0)))
  (:result-types signed-num)
  (:note "inline ASH")
  (:generator 5
    (avm2 :get-local/signed amount)
    (avm2 :dup)
    (avm2 :push-int 0)
    (avm2 :if-ge :positive)
    (avm2 :negate)
    (avm2 :dup)
    (avm2 :push-int 31)
    (avm2 :if-le :okay)
    (avm2 :pop)
    (avm2 :push-int 31)
    (avm2 :%label :okay)
    (avm2 :get-local/signed number)
    (avm2 :swap)
    (avm2 :rshift)
    (avm2 :jump :done)

    (avm2 :%label :positive)
    ;; The result-type ensures us that this shift will not overflow.
    (avm2 :get-local/signed number)
    (avm2 :swap)
    (avm2 :lshift)

    (avm2 :%label :done)
    (avm2 :set-local result)))

(define-vop (fast-ash/unsigned=>unsigned)
  (:translate ash)
  (:policy :fast-safe)
  (:args (number :scs (unsigned-reg) :target result)
         (amount :scs (signed-reg)))
  (:arg-types unsigned-num signed-num)
  (:results (result :scs (unsigned-reg) :from (:argument 0)))
  (:result-types unsigned-num)
  (:note "inline ASH")
  (:generator 5
    (avm2 :get-local/signed amount)
    (avm2 :dup)
    (avm2 :push-int 0)
    (avm2 :if-ge :positive)
    (avm2 :negate)
    (avm2 :dup)
    (avm2 :push-int 31)
    (avm2 :if-le :okay)
    (avm2 :pop)
    (avm2 :push-int 31)
    (avm2 :%label :okay)
    (avm2 :get-local/unsigned number)
    (avm2 :swap)
    (avm2 :unsigned-rshift)
    (avm2 :jump :done)

    (avm2 :%label :positive)
    ;; The result-type ensures us that this shift will not overflow.
    (avm2 :get-local/unsigned number)
    (avm2 :swap)
    (avm2 :lshift)

    (avm2 :%label :done)
    (avm2 :set-local result)))

(in-package "SB!C")

;;; (defknown %lea (integer integer (member 1 2 4 8) (signed-byte 32))
;;;   integer
;;;   (foldable flushable movable))

;;; (defoptimizer (%lea derive-type) ((base index scale disp))
;;;   (when (and (constant-lvar-p scale)
;;;              (constant-lvar-p disp))
;;;     (let ((scale (lvar-value scale))
;;;           (disp (lvar-value disp))
;;;           (base-type (lvar-type base))
;;;           (index-type (lvar-type index)))
;;;       (when (and (numeric-type-p base-type)
;;;                  (numeric-type-p index-type))
;;;         (let ((base-lo (numeric-type-low base-type))
;;;               (base-hi (numeric-type-high base-type))
;;;               (index-lo (numeric-type-low index-type))
;;;               (index-hi (numeric-type-high index-type)))
;;;           (make-numeric-type :class 'integer
;;;                              :complexp :real
;;;                              :low (when (and base-lo index-lo)
;;;                                     (+ base-lo (* index-lo scale) disp))
;;;                              :high (when (and base-hi index-hi)
;;;                                      (+ base-hi (* index-hi scale) disp))))))))

;;; (defun %lea (base index scale disp)
;;;   (+ base (* index scale) disp))

(in-package "SB!VM")

;;; (define-vop (%lea/unsigned=>unsigned)
;;;   (:translate %lea)
;;;   (:policy :fast-safe)
;;;   (:args (base :scs (unsigned-reg))
;;;          (index :scs (unsigned-reg)))
;;;   (:info scale disp)
;;;   (:arg-types unsigned-num unsigned-num
;;;               (:constant (member 1 2 4 8))
;;;               (:constant (signed-byte 32)))
;;;   (:results (r :scs (unsigned-reg)))
;;;   (:result-types unsigned-num)
;;;   (:generator 5
;;;     (x86inst lea r (make-ea :dword :base base :index index
;;;                          :scale scale :disp disp))))

;;; (define-vop (%lea/signed=>signed)
;;;   (:translate %lea)
;;;   (:policy :fast-safe)
;;;   (:args (base :scs (signed-reg))
;;;          (index :scs (signed-reg)))
;;;   (:info scale disp)
;;;   (:arg-types signed-num signed-num
;;;               (:constant (member 1 2 4 8))
;;;               (:constant (signed-byte 32)))
;;;   (:results (r :scs (signed-reg)))
;;;   (:result-types signed-num)
;;;   (:generator 4
;;;     (x86inst lea r (make-ea :dword :base base :index index
;;;                          :scale scale :disp disp))))

;;; (define-vop (%lea/fixnum=>fixnum)
;;;   (:translate %lea)
;;;   (:policy :fast-safe)
;;;   (:args (base :scs (any-reg))
;;;          (index :scs (any-reg)))
;;;   (:info scale disp)
;;;   (:arg-types tagged-num tagged-num
;;;               (:constant (member 1 2 4 8))
;;;               (:constant (signed-byte 32)))
;;;   (:results (r :scs (any-reg)))
;;;   (:result-types tagged-num)
;;;   (:generator 3
;;;     (x86inst lea r (make-ea :dword :base base :index index
;;;                          :scale scale :disp disp))))

;;; FIXME: before making knowledge of this too public, it needs to be
;;; fixed so that it's actually _faster_ than the non-CMOV version; at
;;; least on my Celeron-XXX laptop, this version is marginally slower
;;; than the above version with branches.  -- CSR, 2003-09-04
;; (define-vop (fast-cmov-ash/unsigned=>unsigned)
;;   (:translate ash)
;;   (:policy :fast-safe)
;;   (:args (number :scs (unsigned-reg) :target result)
;;          (amount :scs (signed-reg) :target ecx))
;;   (:arg-types unsigned-num signed-num)
;;   (:results (result :scs (unsigned-reg) :from (:argument 0)))
;;   (:result-types unsigned-num)
;;   (:temporary (:sc signed-reg :offset ecx-offset :from (:argument 1)) ecx)
;;   (:temporary (:sc any-reg :from (:eval 0) :to (:eval 1)) zero)
;;   (:note "inline ASH")
;;   (:guard (member :cmov *backend-subfeatures*))
;;   (:generator 4
;;     (move result number)
;;     (move ecx amount)
;;     (x86inst or ecx ecx)
;;     (x86inst jmp :ns positive)
;;     (x86inst neg ecx)
;;     (x86inst xor zero zero)
;;     (x86inst shr result :cl)
;;     (x86inst cmp ecx 31)
;;     (x86inst cmov :nbe result zero)
;;     (x86inst jmp done)

;;     POSITIVE
;;     ;; The result-type ensures us that this shift will not overflow.
;;     (x86inst shl result :cl)

;;     DONE))

(define-vop (signed-byte-32-len)
  (:translate integer-length)
  (:note "inline (signed-byte 32) integer-length")
  (:policy :fast-safe)
  (:args (arg :scs (signed-reg) :target res))
  (:arg-types signed-num)
  (:results (res :scs (unsigned-reg)))
  (:result-types unsigned-num)
  (:generator 28
    (avm2 :get-local/signed arg)

    (avm2 :dup)
    (avm2 :push-int 0)
    (avm2 :if-ge :pos)
    (avm2 :bit-not)

    (avm2 :%label :pos)
    (avm2 :dup) (avm2 :push-int 1) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 2) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 4) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 8) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 16) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (%unsigned-byte-32-count)
    (avm2 :set-local res)))

(define-vop (unsigned-byte-32-len)
  (:translate integer-length)
  (:note "inline (unsigned-byte 32) integer-length")
  (:policy :fast-safe)
  (:args (arg :scs (unsigned-reg)))
  (:arg-types unsigned-num)
  (:results (res :scs (unsigned-reg)))
  (:result-types unsigned-num)
  (:generator 26
    (avm2 :get-local/unsigned arg)
    (avm2 :dup) (avm2 :push-int 1) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 2) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 4) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 8) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (avm2 :dup) (avm2 :push-int 16) (avm2 :unsigned-rshift) (avm2 :bit-or)
    (%unsigned-byte-32-count)
    (avm2 :set-local res)))

(defun %unsigned-byte-32-count ()
  ;;    x = x - ((x >> 1) & 0x55555555);
  (avm2 :dup)
  (avm2 :push-int 1)
  (avm2 :unsigned-rshift)
  (avm2 :push-int #x55555555)
  (avm2 :bit-and)
  (avm2 :subtract-i)

  ;;    x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
  (avm2 :dup)
  (avm2 :push-int #x33333333)
  (avm2 :bit-and)
  (avm2 :swap)
  (avm2 :push-int 2)
  (avm2 :unsigned-rshift)
  (avm2 :push-int #x33333333)
  (avm2 :bit-and)
  (avm2 :add-i)

  ;;    x = (x + (x >> 4)) & 0x0F0F0F0F;
  (avm2 :dup)
  (avm2 :push-int 4)
  (avm2 :unsigned-rshift)
  (avm2 :add-i)
  (avm2 :push-int #x0F0F0F0F)
  (avm2 :bit-and)

  ;;    x = x + (x >> 8);
  (avm2 :dup)
  (avm2 :push-int 8)
  (avm2 :unsigned-rshift)
  (avm2 :add-i)

  ;;    x = x + (x >> 16);
  (avm2 :dup)
  (avm2 :push-int 16)
  (avm2 :unsigned-rshift)
  (avm2 :add-i)

  ;;    return x & 0x0000003F;
  (avm2 :push-int #x0000003F)
  (avm2 :bit-and))

(define-vop (unsigned-byte-32-count)
  (:translate logcount)
  (:note "inline (unsigned-byte 32) logcount")
  (:policy :fast-safe)
  (:args (arg :scs (unsigned-reg) :target result))
  (:arg-types unsigned-num)
  (:results (result :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 14
    (avm2 :get-local/unsigned arg)
    (%unsigned-byte-32-count)
    (avm2 :set-local result)))

;;;; binary conditional VOPs

(define-vop (fast-conditional)
  (:conditional)
  (:info target not-p)
  (:effects)
  (:affected)
  (:policy :fast-safe))

(define-vop (fast-conditional/fixnum fast-conditional)
  (:args (x :scs (any-reg)
            :load-if (not (and (sc-is x control-stack)
                               (sc-is y any-reg))))
         (y :scs (any-reg control-stack)))
  (:arg-types tagged-num tagged-num)
  (:note "inline fixnum comparison"))

(define-vop (fast-conditional-c/fixnum fast-conditional/fixnum)
  (:args (x :scs (any-reg control-stack)))
  (:arg-types tagged-num (:constant (signed-byte 30)))
  (:info target not-p y))

(define-vop (fast-conditional/signed fast-conditional)
  (:args (x :scs (signed-reg)
            :load-if (not (and (sc-is x signed-stack)
                               (sc-is y signed-reg))))
         (y :scs (signed-reg signed-stack)))
  (:arg-types signed-num signed-num)
  (:note "inline (signed-byte 32) comparison"))

(define-vop (fast-conditional-c/signed fast-conditional/signed)
  (:args (x :scs (signed-reg signed-stack)))
  (:arg-types signed-num (:constant (signed-byte 32)))
  (:info target not-p y))

(define-vop (fast-conditional/unsigned fast-conditional)
  (:args (x :scs (unsigned-reg)
            :load-if (not (and (sc-is x unsigned-stack)
                               (sc-is y unsigned-reg))))
         (y :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num)
  (:note "inline (unsigned-byte 32) comparison"))

(define-vop (fast-conditional-c/unsigned fast-conditional/unsigned)
  (:args (x :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num (:constant (unsigned-byte 32)))
  (:info target not-p y))

(macrolet ((define-logtest-vop (cost suffix fixnump constantp)
             `(define-vop (,(symbolicate "FAST-LOGTEST" suffix)
                            ,(symbolicate "FAST-CONDITIONAL" suffix))
                (:translate logtest)
                (:generator ,cost
                            ,(if fixnump
                                 `(%unbox-fixnum x)
                                 `(avm2 :get-local/nosign x))
                            ,(cond
                              (constantp
                               `(avm2 :push-int y))
                              (fixnump
                               `(%unbox-fixnum y))
                              (t
                               `(avm2 :get-local/nosign y)))
                            (avm2 :bit-and)
                            (avm2 :push-int 0)
                            (avm2 (if not-p :if-eq :if-ne)
                                  (sb!assem::annotation-avm2name target))))))
  (define-logtest-vop 4 /fixnum t nil)
  (define-logtest-vop 3 -c/fixnum t t)
  (define-logtest-vop 6 /signed nil nil)
  (define-logtest-vop 5 -c/signed nil t)
  (define-logtest-vop 6 /unsigned nil nil)
  (define-logtest-vop 5 -c/unsigned nil t))

(defknown %logbitp (integer unsigned-byte) boolean
  (movable foldable flushable always-translatable))

;;; only for constant folding within the compiler
(defun %logbitp (integer index)
  (logbitp index integer))

(defun %%finish-logbitp (ifun not-p target)
  (avm2 :push-int 1)
  (funcall ifun)
  (avm2 :lshift)
  (avm2 :bit-and)
  (avm2 :push-int 0)
  (avm2 (if not-p :if-eq :if-ne)
        (sb!assem::annotation-avm2name target)))

(defmacro %finish-logbitp (i not-p target)
  `(%%finish-logbitp (lambda () ,i) ,not-p ,target))

;;; too much work to do the non-constant case (maybe?)
(define-vop (fast-logbitp-c/fixnum fast-conditional-c/fixnum)
  (:translate %logbitp)
  (:arg-types tagged-num (:constant (integer 0 29)))
  (:generator 4
    (%unbox-fixnum x)
    (%finish-logbitp (avm2 :push-int y) not-p target)))

(define-vop (fast-logbitp/signed fast-conditional/signed)
  (:args (x :scs (signed-reg signed-stack))
         (y :scs (signed-reg)))
  (:translate %logbitp)
  (:generator 6
    (avm2 :get-local/signed x)
    (%finish-logbitp (avm2 :get-local/signed y) not-p target)))

(define-vop (fast-logbitp-c/signed fast-conditional-c/signed)
  (:translate %logbitp)
  (:arg-types signed-num (:constant (integer 0 31)))
  (:generator 5
    (avm2 :get-local/signed x)
    (%finish-logbitp (avm2 :push-int y) not-p target)))

(define-vop (fast-logbitp/unsigned fast-conditional/unsigned)
  (:args (x :scs (unsigned-reg unsigned-stack))
         (y :scs (unsigned-reg)))
  (:translate %logbitp)
  (:generator 6
    (avm2 :get-local/unsigned x)
    (%finish-logbitp (avm2 :get-local/unsigned y) not-p target)))

(define-vop (fast-logbitp-c/unsigned fast-conditional-c/unsigned)
  (:translate %logbitp)
  (:arg-types unsigned-num (:constant (integer 0 31)))
  (:generator 5
    (avm2 :get-local/unsigned x)
    (%finish-logbitp (avm2 :push-uint y) not-p target)))

(macrolet ((define-conditional-vop (tran cond unsigned not-cond not-unsigned)
             `(progn
                ,@(mapcar
                   (lambda (suffix cost signed
                            ;; kludge
                            &aux (fixnump (search "FIXNUM" (string suffix)))
                                 (constantp (search "-C" (string suffix))))
                     `(define-vop (;; FIXME: These could be done more
                                   ;; cleanly with SYMBOLICATE.
                                   ,(intern (format nil "~:@(FAST-IF-~A~A~)"
                                                    tran suffix))
                                   ,(intern
                                     (format nil "~:@(FAST-CONDITIONAL~A~)"
                                             suffix)))
                        (:translate ,tran)
                        (:generator ,cost
;;                                     (x86inst jmp (if not-p
;;                                                   ,(if signed
;;                                                        not-cond
;;                                                        not-unsigned)
;;                                                   ,(if signed
;;                                                        cond
;;                                                        unsigned))
;;                                           target)

                            ,(if fixnump
                                 `(%unbox-fixnum x)
                                 `(avm2 ,(if signed
                                             :get-local/signed
                                             :get-local/unsigned)
                                        x))
                            ,(cond
                              (constantp
                               (if signed
                                   `(avm2 :push-int y)
                                   `(avm2 :push-uint y)))
                              (fixnump
                               `(%unbox-fixnum y))
                              (t
                               `(avm2 ,(if signed
                                           :get-local/signed
                                           :get-local/unsigned)
                                      y)))

                            (avm2 (if not-p
                                      ,(if signed
                                           not-cond
                                           not-unsigned)
                                      ,(if signed
                                           cond
                                           unsigned))
                                  target))))
                   '(/fixnum -c/fixnum /signed -c/signed /unsigned -c/unsigned)
                   '(4 3 6 5 6 5)
                   '(t t t t nil nil)))))

  ;; There's no difference between the signed and unsigned comparison opcodes,
  ;; since we actually do 64 bit float operations here.  We just make sure to
  ;; pop the values to the stack correctly.
  (define-conditional-vop < :if-lt :if-lt :if-ge :if-ge)
  (define-conditional-vop > :if-gt :if-gt :if-le :if-le))

(define-vop (fast-if-eql/signed fast-conditional/signed)
  (:translate eql)
  (:generator 6
    (avm2 :get-local/signed x)
    (avm2 :get-local/signed y)
    (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target))))

(define-vop (fast-if-eql-c/signed fast-conditional-c/signed)
  (:translate eql)
  (:generator 5
    (avm2 :get-local/signed x)
    (avm2 :push-int y)
    (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target))))

(define-vop (fast-if-eql/unsigned fast-conditional/unsigned)
  (:translate eql)
  (:generator 6
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target))))

(define-vop (fast-if-eql-c/unsigned fast-conditional-c/unsigned)
  (:translate eql)
  (:generator 5
    (avm2 :get-local/unsigned x)
    (avm2 :push-uint y)
    (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target))))

;;; EQL/FIXNUM is funny because the first arg can be of any type, not just a
;;; known fixnum.

;;; These versions specify a fixnum restriction on their first arg. We have
;;; also generic-eql/fixnum VOPs which are the same, but have no restriction on
;;; the first arg and a higher cost. The reason for doing this is to prevent
;;; fixnum specific operations from being used on word integers, spuriously
;;; consing the argument.

(define-vop (fast-eql/fixnum fast-conditional)
  (:args (x :scs (any-reg)
            :load-if (not (and (sc-is x control-stack)
                               (sc-is y any-reg))))
         (y :scs (any-reg control-stack)))
  (:arg-types tagged-num tagged-num)
  (:note "inline fixnum comparison")
  (:translate eql)
  (:generator 4
    (%unbox-fixnum x)
    (%unbox-fixnum y)
    (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target))))
(define-vop (generic-eql/fixnum fast-eql/fixnum)
  (:args (x :scs (any-reg descriptor-reg)
            :load-if (not (and (sc-is x control-stack)
                               (sc-is y any-reg))))
         (y :scs (any-reg control-stack)))
  (:arg-types * tagged-num)
  (:variant-cost 7))

(define-vop (fast-eql-c/fixnum fast-conditional/fixnum)
  (:args (x :scs (any-reg control-stack)))
  (:arg-types tagged-num (:constant (signed-byte 30)))
  (:info target not-p y)
  (:translate eql)
  (:generator 2
    (%unbox-fixnum x)
    (avm2 :push-int y)
    (avm2 (if not-p :if-ne :if-eq) target)))
(define-vop (generic-eql-c/fixnum fast-eql-c/fixnum)
  (:args (x :scs (any-reg descriptor-reg control-stack)))
  (:arg-types * (:constant (signed-byte 30)))
  (:variant-cost 6))

;;;; 32-bit logical operations

;;; unused
;; (define-vop (merge-bits)
;;   (:translate merge-bits)
;;   (:args (shift :scs (signed-reg unsigned-reg) :target ecx)
;;          (prev :scs (unsigned-reg) :target result)
;;          (next :scs (unsigned-reg)))
;;   (:arg-types tagged-num unsigned-num unsigned-num)
;;   (:temporary (:sc signed-reg :offset ecx-offset :from (:argument 0)) ecx)
;;   (:results (result :scs (unsigned-reg) :from (:argument 1)))
;;   (:result-types unsigned-num)
;;   (:policy :fast-safe)
;;   (:generator 4
;;     (move ecx shift)
;;     (move result prev)
;;     (x86inst shrd result next :cl)))

;;; Only the lower 5 bits of the shift amount are significant.
(define-vop (shift-towards-someplace)
  (:policy :fast-safe)
  (:args (num :scs (unsigned-reg) :target r)
         (amount :scs (signed-reg)))
  (:arg-types unsigned-num tagged-num)
  (:results (r :scs (unsigned-reg) :from (:argument 0)))
  (:result-types unsigned-num))

(define-vop (shift-towards-start shift-towards-someplace)
  (:translate shift-towards-start)
  (:note "SHIFT-TOWARDS-START")
  (:generator 1
    (avm2 :get-local/unsigned num)
    (avm2 :get-local/signed amount)
    (avm2 :unsigned-rshift)
    (avm2 :set-local r)))

(define-vop (shift-towards-end shift-towards-someplace)
  (:translate shift-towards-end)
  (:note "SHIFT-TOWARDS-END")
  (:generator 1
    (avm2 :get-local/unsigned num)
    (avm2 :get-local/signed amount)
    (avm2 :lshift)
    (avm2 :set-local r)))

;;;; Modular functions
(defmacro define-mod-binop ((name prototype) function)
  `(define-vop (,name ,prototype)
       (:args (x :target r :scs (unsigned-reg signed-reg)
                 :load-if (not (and (or (sc-is x unsigned-stack)
                                        (sc-is x signed-stack))
                                    (or (sc-is y unsigned-reg)
                                        (sc-is y signed-reg))
                                    (or (sc-is r unsigned-stack)
                                        (sc-is r signed-stack))
                                    (location= x r))))
              (y :scs (unsigned-reg signed-reg unsigned-stack signed-stack)))
     (:arg-types untagged-num untagged-num)
     (:results (r :scs (unsigned-reg signed-reg) :from (:argument 0)
                  :load-if (not (and (or (sc-is x unsigned-stack)
                                         (sc-is x signed-stack))
                                     (or (sc-is y unsigned-reg)
                                         (sc-is y unsigned-reg))
                                     (or (sc-is r unsigned-stack)
                                         (sc-is r unsigned-stack))
                                     (location= x r)))))
     (:result-types unsigned-num)
     (:translate ,function)))
(defmacro define-mod-binop-c ((name prototype) function)
  `(define-vop (,name ,prototype)
       (:args (x :target r :scs (unsigned-reg signed-reg)
                 :load-if (not (and (or (sc-is x unsigned-stack)
                                        (sc-is x signed-stack))
                                    (or (sc-is r unsigned-stack)
                                        (sc-is r signed-stack))
                                    (location= x r)))))
     (:info y)
     (:arg-types untagged-num (:constant (or (unsigned-byte 32) (signed-byte 32))))
     (:results (r :scs (unsigned-reg signed-reg) :from (:argument 0)
                  :load-if (not (and (or (sc-is x unsigned-stack)
                                         (sc-is x signed-stack))
                                     (or (sc-is r unsigned-stack)
                                         (sc-is r unsigned-stack))
                                     (location= x r)))))
     (:result-types unsigned-num)
     (:translate ,function)))

(macrolet ((def (name -c-p)
             (let ((fun32 (intern (format nil "~S-MOD32" name)))
                   (vopu (intern (format nil "FAST-~S/UNSIGNED=>UNSIGNED" name)))
                   (vopcu (intern (format nil "FAST-~S-C/UNSIGNED=>UNSIGNED" name)))
                   (vopf (intern (format nil "FAST-~S/FIXNUM=>FIXNUM" name)))
                   (vopcf (intern (format nil "FAST-~S-C/FIXNUM=>FIXNUM" name)))
                   (vop32u (intern (format nil "FAST-~S-MOD32/WORD=>UNSIGNED" name)))
                   (vop32f (intern (format nil "FAST-~S-MOD32/FIXNUM=>FIXNUM" name)))
                   (vop32cu (intern (format nil "FAST-~S-MOD32-C/WORD=>UNSIGNED" name)))
                   (vop32cf (intern (format nil "FAST-~S-MOD32-C/FIXNUM=>FIXNUM" name)))
                   (sfun30 (intern (format nil "~S-SMOD30" name)))
                   (svop30f (intern (format nil "FAST-~S-SMOD30/FIXNUM=>FIXNUM" name)))
                   (svop30cf (intern (format nil "FAST-~S-SMOD30-C/FIXNUM=>FIXNUM" name))))
               `(progn
                  (define-modular-fun ,fun32 (x y) ,name :untagged nil 32)
                  (define-modular-fun ,sfun30 (x y) ,name :tagged t 30)
                  (define-mod-binop (,vop32u ,vopu) ,fun32)
                  (define-vop (,vop32f ,vopf) (:translate ,fun32))
                  (define-vop (,svop30f ,vopf) (:translate ,sfun30))
                  ,@(when -c-p
                      `((define-mod-binop-c (,vop32cu ,vopcu) ,fun32)
                        (define-vop (,svop30cf ,vopcf) (:translate ,sfun30))))))))
  (def + t)
  (def - t)
  ;; (no -C variant as x86 MUL instruction doesn't take an immediate)
  (def * nil))


;;; (define-vop (fast-ash-left-mod32-c/unsigned=>unsigned
;;;              fast-ash-c/unsigned=>unsigned)
;;;   (:translate ash-left-mod32))

;;; (define-vop (fast-ash-left-mod32/unsigned=>unsigned
;;;              fast-ash-left/unsigned=>unsigned))
;;; (deftransform ash-left-mod32 ((integer count)
;;;                               ((unsigned-byte 32) (unsigned-byte 5)))
;;;   (when (sb!c::constant-lvar-p count)
;;;     (sb!c::give-up-ir1-transform))
;;;   '(%primitive fast-ash-left-mod32/unsigned=>unsigned integer count))

;;; (define-vop (fast-ash-left-smod30-c/fixnum=>fixnum
;;;              fast-ash-c/fixnum=>fixnum)
;;;   (:translate ash-left-smod30))

;;; (define-vop (fast-ash-left-smod30/fixnum=>fixnum
;;;              fast-ash-left/fixnum=>fixnum))
;;; (deftransform ash-left-smod30 ((integer count)
;;;                                ((signed-byte 30) (unsigned-byte 5)))
;;;   (when (sb!c::constant-lvar-p count)
;;;     (sb!c::give-up-ir1-transform))
;;;   '(%primitive fast-ash-left-smod30/fixnum=>fixnum integer count))

(in-package "SB!C")

;; (defknown sb!vm::%lea-mod32 (integer integer (member 1 2 4 8) (signed-byte 32))
;;   (unsigned-byte 32)
;;   (foldable flushable movable))
;; (defknown sb!vm::%lea-smod30 (integer integer (member 1 2 4 8) (signed-byte 32))
;;   (signed-byte 30)
;;   (foldable flushable movable))

;; (define-modular-fun-optimizer %lea ((base index scale disp) :untagged nil :width width)
;;   (when (and (<= width 32)
;;              (constant-lvar-p scale)
;;              (constant-lvar-p disp))
;;     (cut-to-width base :untagged width nil)
;;     (cut-to-width index :untagged width nil)
;;     'sb!vm::%lea-mod32))
;; (define-modular-fun-optimizer %lea ((base index scale disp) :tagged t :width width)
;;   (when (and (<= width 30)
;;              (constant-lvar-p scale)
;;              (constant-lvar-p disp))
;;     (cut-to-width base :tagged width t)
;;     (cut-to-width index :tagged width t)
;;     'sb!vm::%lea-smod30))

;; #+sb-xc-host
;; (progn
;;   (defun sb!vm::%lea-mod32 (base index scale disp)
;;     (ldb (byte 32 0) (%lea base index scale disp)))
;;   (defun sb!vm::%lea-smod30 (base index scale disp)
;;     (mask-signed-field 30 (%lea base index scale disp))))
;; #-sb-xc-host
;; (progn
;;   (defun sb!vm::%lea-mod32 (base index scale disp)
;;     (let ((base (logand base #xffffffff))
;;           (index (logand index #xffffffff)))
;;       ;; can't use modular version of %LEA, as we only have VOPs for
;;       ;; constant SCALE and DISP.
;;       (ldb (byte 32 0) (+ base (* index scale) disp))))
;;   (defun sb!vm::%lea-smod30 (base index scale disp)
;;     (let ((base (mask-signed-field 30 base))
;;           (index (mask-signed-field 30 index)))
;;       ;; can't use modular version of %LEA, as we only have VOPs for
;;       ;; constant SCALE and DISP.
;;       (mask-signed-field 30 (+ base (* index scale) disp)))))

(in-package "SB!VM")

;;; (define-vop (%lea-mod32/unsigned=>unsigned
;;;              %lea/unsigned=>unsigned)
;;;   (:translate %lea-mod32))
;;; (define-vop (%lea-smod30/fixnum=>fixnum
;;;              %lea/fixnum=>fixnum)
;;;   (:translate %lea-smod30))

;;; logical operations
(define-modular-fun lognot-mod32 (x) lognot :untagged nil 32)
(define-vop (lognot-mod32/word=>unsigned)
  (:translate lognot-mod32)
  (:args (x :scs (unsigned-reg signed-reg unsigned-stack signed-stack) :target r
            :load-if (not (and (or (sc-is x unsigned-stack)
                                   (sc-is x signed-stack))
                               (or (sc-is r unsigned-stack)
                                   (sc-is r signed-stack))
                               (location= x r)))))
  (:arg-types unsigned-num)
  (:results (r :scs (unsigned-reg)
               :load-if (not (and (or (sc-is x unsigned-stack)
                                      (sc-is x signed-stack))
                                  (or (sc-is r unsigned-stack)
                                      (sc-is r signed-stack))
                                  (sc-is r unsigned-stack)
                                  (location= x r)))))
  (:result-types unsigned-num)
  (:policy :fast-safe)
  (:generator 1
    (avm2 :get-local/unsigned x)
    (avm2 :bit-not)
    (avm2 :set-local r)))

(define-source-transform logeqv (&rest args)
  (if (oddp (length args))
      `(logxor ,@args)
      `(lognot (logxor ,@args))))
(define-source-transform logandc1 (x y)
  `(logand (lognot ,x) ,y))
(define-source-transform logandc2 (x y)
  `(logand ,x (lognot ,y)))
(define-source-transform logorc1 (x y)
  `(logior (lognot ,x) ,y))
(define-source-transform logorc2 (x y)
  `(logior ,x (lognot ,y)))
(define-source-transform lognor (x y)
  `(lognot (logior ,x ,y)))
(define-source-transform lognand (x y)
  `(lognot (logand ,x ,y)))

;;;; bignum stuff

(define-vop (bignum-length get-header-data)
  (:translate sb!bignum:%bignum-length)
  (:policy :fast-safe)
  (:args (x :scs (descriptor-reg)))
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 42
    (%find-property "getBignumLength")
    (avm2 :get-local/object x)
    (%call-property "getBignumLength" 1)
    (avm2 :set-local res)))

(define-vop (bignum-set-length set-header-data)
  (:translate sb!bignum:%bignum-set-length)
  (:policy :fast-safe)
  (:args (x :scs (descriptor-reg) :target res :to (:result 0))
         (data :scs (any-reg)))
  (:arg-types * positive-fixnum)
  (:results (res :scs (descriptor-reg)))
  (:generator 42
    (avm2 :get-local/object x)

    (avm2 :dup)
    (%find-property "setBignumLength")
    (avm2 :swap)
    (avm2 :get-local/object data)
    (%call-property "setBignumLength" 2)
    (avm2 :set-local res)))

(define-datavector-reffer bignum-ref * (unsigned-reg) unsigned-num
                    sb!bignum:%bignum-ref)
;; (define-datavector-reffer+offset bignum-ref-with-offset *
;;   (unsigned-reg) unsigned-num sb!bignum:%bignum-ref-with-offset)
(define-datavector-setter bignum-set *
  (unsigned-reg) unsigned-num sb!bignum:%bignum-set)

(define-vop (digit-0-or-plus)
  (:translate sb!bignum:%digit-0-or-plusp)
  (:policy :fast-safe)
  (:args (digit :scs (unsigned-reg)))
  (:arg-types unsigned-num)
  (:conditional)
  (:info target not-p)
  (:generator 3
    (avm2 :get-local/unsigned digit)
    (avm2 :push-int 0)
    (avm2 (if not-p :if-lt :if-ge) target)))


;;; c was any-reg on x86, but that's not convenient for us
(define-vop (add-w/carry)
  (:translate sb!bignum:%add-with-carry)
  (:policy :fast-safe)
  (:args (a :scs (unsigned-reg) :target result)
         (b :scs (unsigned-reg unsigned-stack) :to :eval)
         (c :scs (unsigned-reg)))
  (:arg-types unsigned-num unsigned-num positive-fixnum)
  (:results (result :scs (unsigned-reg) :from (:argument 0))
            (carry :scs (unsigned-reg)))
  (:result-types unsigned-num positive-fixnum)
  (:generator 4
    ;; use :ADD (i.e. double float addition) rather than :add-i,
    ;; then extract the lower 32 bit word and get the carry bit, which
    ;; sits in the upper bits of the result
    (avm2 :get-local/unsigned a)
    (avm2 :get-local/unsigned b)
    (avm2 :add)
    (avm2 :get-local/unsigned c)
    (avm2 :add)
    (avm2 :dup)
    (avm2 :convert-unsigned)
    (avm2 :set-local result)
    (avm2 :push-int 32)
    (avm2 :unsigned-rshift)
    (avm2 :set-local carry)))

;;; Note: the borrow is 1 for no borrow and 0 for a borrow, the opposite
;;; of the x86 convention.
(define-vop (sub-w/borrow)
  (:translate sb!bignum:%subtract-with-borrow)
  (:policy :fast-safe)
  (:args (a :scs (unsigned-reg) :to :eval :target result)
         (b :scs (unsigned-reg unsigned-stack) :to :result)
         (c :scs (unsigned-reg)))
  (:arg-types unsigned-num unsigned-num positive-fixnum)
  (:results (result :scs (unsigned-reg) :from :eval)
            (borrow :scs (unsigned-reg)))
  (:result-types unsigned-num positive-fixnum)
  (:generator 5
    (avm2 :get-local/unsigned a)
    (avm2 :get-local/unsigned b)
    (avm2 :subtract)
    (avm2 :decrement)
    (avm2 :get-local/unsigned c)
    (avm2 :add)
    (avm2 :dup)
    (avm2 :convert-unsigned)
    (avm2 :set-local result)
    (avm2 :push-int 32)
    (avm2 :rshift)
    (avm2 :increment-i)                 ;-1/0 -> 0/1
    (avm2 :set-local borrow)))

(define-vop (bignum-mult-and-add-3-arg)
  (:translate sb!bignum:%multiply-and-add)
  (:policy :fast-safe)
  (:args (x :scs (unsigned-reg))
         (y :scs (unsigned-reg unsigned-stack))
         (carry-in :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num unsigned-num)
  (:results (hi :scs (unsigned-reg))
            (lo :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:generator 20
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 :multiply)
    (avm2 :get-local/unsigned carry-in)
    (avm2 :add)

    (avm2 :dup)
    (avm2 :convert-unsigned)
    (avm2 :set-local lo)
    (avm2 :push-int 32)
    (avm2 :unsigned-rshift)
    (avm2 :set-local hi)))

(define-vop (bignum-mult-and-add-4-arg)
  (:translate sb!bignum:%multiply-and-add)
  (:policy :fast-safe)
  (:args (x :scs (unsigned-reg))
         (y :scs (unsigned-reg unsigned-stack))
         (prev :scs (unsigned-reg unsigned-stack))
         (carry-in :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num unsigned-num unsigned-num)
  (:results (hi :scs (unsigned-reg))
            (lo :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:generator 20
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 :multiply)
    (avm2 :get-local/unsigned prev)
    (avm2 :add)
    (avm2 :get-local/unsigned carry-in)
    (avm2 :add)

    (avm2 :dup)
    (avm2 :convert-unsigned)
    (avm2 :set-local lo)
    (avm2 :push-int 32)
    (avm2 :unsigned-rshift)
    (avm2 :set-local hi)))


(define-vop (bignum-mult)
  (:translate sb!bignum:%multiply)
  (:policy :fast-safe)
  (:args (x :scs (unsigned-reg))
         (y :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num)
  (:results (hi :scs (unsigned-reg))
            (lo :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:generator 20
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 :multiply)

    (avm2 :dup)
    (avm2 :convert-unsigned)
    (avm2 :set-local lo)
    (avm2 :push-int 32)
    (avm2 :unsigned-rshift)
    (avm2 :set-local hi)))

(define-vop (bignum-lognot lognot-mod32/word=>unsigned)
  (:translate sb!bignum:%lognot))

(define-vop (fixnum-to-digit)
  (:translate sb!bignum:%fixnum-to-digit)
  (:policy :fast-safe)
  (:args (fixnum :scs (any-reg control-stack) :target digit))
  (:arg-types tagged-num)
  (:results (digit :scs (unsigned-reg)
                   :load-if (not (and (sc-is fixnum control-stack)
                                      (sc-is digit unsigned-stack)
                                      (location= fixnum digit)))))
  (:result-types unsigned-num)
  (:generator 1
    (%unbox-fixnum fixnum)
    (avm2 :set-local digit)))

(define-vop (bignum-floor)
  (:translate sb!bignum:%floor)
  (:policy :fast-safe)
  (:args (div-high :scs (unsigned-reg))
         (div-low :scs (unsigned-reg))
         (divisor :scs (unsigned-reg unsigned-stack)))
  (:arg-types unsigned-num unsigned-num unsigned-num)
  (:results (quo :scs (unsigned-reg))
            (rem :scs (unsigned-reg)))
  (:result-types unsigned-num unsigned-num)
  (:vop-var vop)
  (:generator 300
    (%not-implemented vop div-high div-low divisor quo rem)))

(define-vop (signify-digit)
  (:translate sb!bignum:%fixnum-digit-with-correct-sign)
  (:policy :fast-safe)
  (:args (digit :scs (unsigned-reg unsigned-stack) :target res))
  (:arg-types unsigned-num)
  (:results (res :scs (any-reg signed-reg)))
  (:result-types signed-num)
  (:generator 1
    ;; /nosign, because this vop is only a no-op compiler cheat
    (avm2 :get-local/nosign digit)
    (avm2 :set-local res)))

(define-vop (digit-ashr)
  (:translate sb!bignum:%ashr)
  (:policy :fast-safe)
  (:args (digit :scs (unsigned-reg unsigned-stack) :target result)
         (count :scs (unsigned-reg)))
  (:arg-types unsigned-num positive-fixnum)
  (:results (result :scs (unsigned-reg) :from (:argument 0)))
  (:result-types unsigned-num)
  (:generator 2
    (avm2 :get-local/unsigned digit)
    (avm2 :get-local/unsigned count)
    (avm2 :rshift)
    (avm2 :set-local result)))

(define-vop (digit-ashr/c)
  (:translate sb!bignum:%ashr)
  (:policy :fast-safe)
  (:args (digit :scs (unsigned-reg unsigned-stack) :target result))
  (:arg-types unsigned-num (:constant (integer 0 31)))
  (:info count)
  (:results (result :scs (unsigned-reg) :from (:argument 0)))
  (:result-types unsigned-num)
  (:generator 1
    (avm2 :get-local/unsigned digit)
    (avm2 :push-uint count)
    (avm2 :rshift)
    (avm2 :set-local result)))

(define-vop (digit-lshr digit-ashr)
  (:translate sb!bignum:%digit-logical-shift-right)
  (:generator 1
    (avm2 :get-local/unsigned digit)
    (avm2 :get-local/unsigned count)
    (avm2 :unsigned-rshift)
    (avm2 :set-local result)))

(define-vop (digit-ashl digit-ashr)
  (:translate sb!bignum:%ashl)
  (:generator 1
    (avm2 :get-local/unsigned digit)
    (avm2 :get-local/unsigned count)
    (avm2 :lshift)
    (avm2 :set-local result)))

;;;; static functions

(macrolet ((define-static-fun (name translate)
             `(define-vop (,name)
                (:note ,(format nil "static-fun ~@(~S~)" name))
                (:translate ,translate)
                (:args (x :scs (any-reg descriptor-reg))
                       (y :scs (any-reg descriptor-reg)))
                (:results (result :scs (any-reg descriptor-reg)))
                (:policy :safe)
                (:generator 42
                            (%find-property ,(string name))
                            (avm2 :get-local/object x)
                            (avm2 :get-local/object y)
                            (%call-property ,(string name) 2)
                            (avm2 :set-local result)))))
  (define-static-fun two-arg-/ /)
  (define-static-fun two-arg-gcd gcd)
  (define-static-fun two-arg-lcm lcm)
  (define-static-fun two-arg-and logand)
  (define-static-fun two-arg-ior logior)
  (define-static-fun two-arg-xor logxor))

 
;; (in-package "SB!C")

;; (defun mask-result (class width result)
;;   (ecase class
;;     (:unsigned
;;      `(logand ,result ,(1- (ash 1 width))))
;;     (:signed
;;      `(mask-signed-field ,width ,result))))

;; ;;; This is essentially a straight implementation of the algorithm in
;; ;;; "Strength Reduction of Multiplications by Integer Constants",
;; ;;; Youfeng Wu, ACM SIGPLAN Notices, Vol. 30, No.2, February 1995.
;; (defun basic-decompose-multiplication (class width arg num n-bits condensed)
;;   (case (aref condensed 0)
;; ;;;     (0
;; ;;;      (let ((tmp (min 3 (aref condensed 1))))
;; ;;;        (decf (aref condensed 1) tmp)
;; ;;;        (mask-result class width
;; ;;;                     `(%lea ,arg
;; ;;;                            ,(decompose-multiplication class width
;; ;;;                              arg (ash (1- num) (- tmp)) (1- n-bits) (subseq condensed 1))
;; ;;;                            ,(ash 1 tmp) 0))))
;; ;;;     ((1 2 3)
;; ;;;      (let ((r0 (aref condensed 0)))
;; ;;;        (incf (aref condensed 1) r0)
;; ;;;        (mask-result class width
;; ;;;                     `(%lea ,(decompose-multiplication class width
;; ;;;                              arg (- num (ash 1 r0)) (1- n-bits) (subseq condensed 1))
;; ;;;                            ,arg
;; ;;;                            ,(ash 1 r0) 0))))
;;     (t (let ((r0 (aref condensed 0)))
;;          (setf (aref condensed 0) 0)
;;          (mask-result class width
;;                       `(ash ,(decompose-multiplication class width
;;                               arg (ash num (- r0)) n-bits condensed)
;;                             ,r0))))))

;; (defun decompose-multiplication (class width arg num n-bits condensed)
;;   (cond
;;     ((= n-bits 0) 0)
;;     ((= num 1) arg)
;;     ((= n-bits 1)
;;      (mask-result class width `(ash ,arg ,(1- (integer-length num)))))
;;     ((let ((max 0) (end 0))
;;        (loop for i from 2 to (length condensed)
;;              for j = (reduce #'+ (subseq condensed 0 i))
;;              when (and (> (- (* 2 i) 3 j) max)
;;                        (< (+ (ash 1 (1+ j))
;;                              (ash (ldb (byte (- 32 (1+ j)) (1+ j)) num)
;;                                   (1+ j)))
;;                           (ash 1 32)))
;;                do (setq max (- (* 2 i) 3 j)
;;                         end i))
;;        (when (> max 0)
;;          (let ((j (reduce #'+ (subseq condensed 0 end))))
;;            (let ((n2 (+ (ash 1 (1+ j))
;;                         (ash (ldb (byte (- 32 (1+ j)) (1+ j)) num) (1+ j))))
;;                  (n1 (1+ (ldb (byte (1+ j) 0) (lognot num)))))
;;            (mask-result class width
;;                         `(- ,(optimize-multiply class width arg n2)
;;                             ,(optimize-multiply  class width arg n1))))))))
;; ;;;     ((dolist (i '(9 5 3))
;; ;;;        (when (integerp (/ num i))
;; ;;;          (when (< (logcount (/ num i)) (logcount num))
;; ;;;            (let ((x (gensym)))
;; ;;;              (return `(let ((,x ,(optimize-multiply class width arg (/ num i))))
;; ;;;                        ,(mask-result class width
;; ;;;                                      `(%lea ,x ,x (1- ,i) 0)))))))))
;;     (t (basic-decompose-multiplication class width arg num n-bits condensed))))

;; (defun optimize-multiply (class width arg x)
;;   (let* ((n-bits (logcount x))
;;          (condensed (make-array n-bits)))
;;     (let ((count 0) (bit 0))
;;       (dotimes (i 32)
;;         (cond ((logbitp i x)
;;                (setf (aref condensed bit) count)
;;                (setf count 1)
;;                (incf bit))
;;               (t (incf count)))))
;;     (decompose-multiplication class width arg x n-bits condensed)))

;; (defun *-transformer (class width y)
;;   (cond
;;     ((= y (ash 1 (integer-length y)))
;;      ;; there's a generic transform for y = 2^k
;;      (give-up-ir1-transform))
;; ;;;     ((member y '(3 5 9))
;; ;;;      ;; we can do these multiplications directly using LEA
;; ;;;      `(%lea x x ,(1- y) 0))
;;     ((member :pentium4 *backend-subfeatures*)
;;      ;; the pentium4's multiply unit is reportedly very good
;;      (give-up-ir1-transform))
;;     ;; FIXME: should make this more fine-grained.  If nothing else,
;;     ;; there should probably be a cutoff of about 9 instructions on
;;     ;; pentium-class machines.
;;     (t (optimize-multiply class width 'x y))))

;; (deftransform * ((x y)
;;                  ((unsigned-byte 32) (constant-arg (unsigned-byte 32)))
;;                  (unsigned-byte 32))
;;   "recode as leas, shifts and adds"
;;   (let ((y (lvar-value y)))
;;     (*-transformer :unsigned 32 y)))
;; (deftransform sb!vm::*-mod32
;;     ((x y) ((unsigned-byte 32) (constant-arg (unsigned-byte 32)))
;;      (unsigned-byte 32))
;;   "recode as leas, shifts and adds"
;;   (let ((y (lvar-value y)))
;;     (*-transformer :unsigned 32 y)))

;; (deftransform * ((x y)
;;                  ((signed-byte 30) (constant-arg (unsigned-byte 32)))
;;                  (signed-byte 30))
;;   "recode as leas, shifts and adds"
;;   (let ((y (lvar-value y)))
;;     (*-transformer :signed 30 y)))
;; (deftransform sb!vm::*-smod30
;;     ((x y) ((signed-byte 30) (constant-arg (unsigned-byte 32)))
;;      (signed-byte 30))
;;   "recode as leas, shifts and adds"
;;   (let ((y (lvar-value y)))
;;     (*-transformer :signed 30 y)))

;; ;;; FIXME: we should also be able to write an optimizer or two to
;; ;;; convert (+ (* x 2) 17), (- (* x 9) 5) to a %LEA.

;;; generic arithmetic

(macrolet ((define-generic-arith-routine ((fun cost method))
             `(define-vop (,(symbolicate "GENERIC-" fun))
                (:translate ,fun)
                (:policy :safe)
                (:save-p t)
                (:args (x :scs (descriptor-reg any-reg))
                       (y :scs (descriptor-reg any-reg)))
                (:results (res :scs (descriptor-reg any-reg)))
                (:generator ,cost
                            (%find-property ,method)
                            (avm2 :get-local/object x)
                            (avm2 :get-local/object y)
                            (%call-property ,method 2)
                            (avm2 :set-local res)))))
  (define-generic-arith-routine (+ 10 "genericPlus"))
  (define-generic-arith-routine (- 10 "genericMinus"))
  (define-generic-arith-routine (* 30 "genericTimes")))

(define-vop (generic-negate)
  (:translate %negate)
  (:policy :safe)
  (:save-p t)
  (:args (x :scs (descriptor-reg any-reg)))
  (:results (res :scs (descriptor-reg any-reg)))
  (:generator 10
              (%find-property "genericNegate")
              (avm2 :get-local/object x)
              (%call-property "genericNegate" 1)
              (avm2 :set-local res)))

(macrolet ((define-cond-assem-rtn (name translate method)
             `(define-vop (,name)
                (:args (x :scs (descriptor-reg any-reg))
                       (y :scs (descriptor-reg any-reg)))
                (:policy :safe)
                (:translate ,translate)
                (:results (res :scs (descriptor-reg)))
                (:generator 10
                  (%find-property ,method)
                  (avm2 :get-local/object x)
                  (avm2 :get-local/object y)
                  (%call-property ,method 2)
                  (avm2 :set-local res)))))
  (define-cond-assem-rtn generic-< < "genericLess")
  (define-cond-assem-rtn generic-> > "genericGreater")
  (define-cond-assem-rtn generic-eql eql "genericEql")
  (define-cond-assem-rtn generic-= = "genericNumberequal"))
