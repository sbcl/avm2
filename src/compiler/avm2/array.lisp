;;;; array operations for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; allocator for the array header

(define-vop (make-array-header)
  (:translate make-array-header)
  (:policy :fast-safe)
  (:args (type :scs (any-reg))
         (rank :scs (any-reg)))
  (:arg-types positive-fixnum positive-fixnum)
  (:results (result :scs (descriptor-reg) :from :eval))
  (:generator 13
    (%find-property "makeArrayHeader")
    (avm2 :get-local/object type)
    (avm2 :get-local/object rank)
    (%call-property "makeArrayHeader" 2)
    (avm2 :set-local result)))

;;;; additional accessors and setters for the array header

(define-vop (%array-dimension)
  (:translate sb!kernel:%array-dimension)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate unsigned-reg)))
  (:arg-types * tagged-num)
  (:results (value :scs (any-reg)))
  (:result-types positive-fixnum)
  (:generator 3                      ; pw was 5
    (%find-property "getArrayDimension")
    (avm2 :get-local/object object)
    (sc-case index
      (immediate (avm2 :push-uint index))
      (any-reg (%unbox-fixnum index))
      (unsigned-reg (avm2 :get-local/unsigned index)))
    (%call-property "getArrayDimension" 2)
    (avm2 :set-local value)))

(define-vop (%set-array-dimension)
  (:translate sb!kernel:%set-array-dimension)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate unsigned-reg))
         (value :scs (any-reg)))
  (:arg-types * tagged-num positive-fixnum)
  (:results (result :scs (any-reg)))
  (:result-types positive-fixnum)
  (:generator 3                      ; pw was 5
    (%find-property "setArrayDimension")
    (avm2 :get-local/object object)
    (sc-case index
      (immediate (avm2 :push-uint index))
      (any-reg (%unbox-fixnum index))
      (unsigned-reg (avm2 :get-local/unsigned index)))
    (avm2 :get-local/object value)
    (%call-property "setArrayDimension" 3)
    (avm2 :pop)
    (avm2 :get-local/object value)
    (avm2 :set-local result)))

(define-avm2slot-reffer array-rank-vop *
  "arrayRank"
  (unsigned-reg) positive-fixnum sb!kernel:%array-rank)

;;;; bounds checking routine

(define-vop (check-bound)
  (:translate %check-bound)
  (:policy :fast-safe)
  (:args (array :scs (descriptor-reg))
         (bound :scs (unsigned-reg))
         (index :scs (any-reg) :target result))
  (:ignore array)
  (:arg-types * positive-fixnum tagged-num)
  (:results (result :scs (any-reg)))
  (:result-types positive-fixnum)
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 5
    (let ((ok-label (gen-label)))
      (%unbox-fixnum index)
      (avm2 :get-local/unsigned bound)
      (avm2 :if-lt ok-label)
      (avm2 :push-string "invalid-array-index-error")
      (avm2 :throw)

      (avm2 :%label ok-label)

      (avm2 :get-local/object index)
      (avm2 :set-local result))))

;;;; accessors/setters

;;; variants built on top of WORD-INDEX-REF, etc. I.e., those vectors
;;; whose elements are represented in integer registers and are built
;;; out of 8, 16, or 32 bit elements.
;;;
;;; kludge: we don't actually have specialized vectors on AVM2, so these
;;; are all the same.
(macrolet ((def-full-data-vector-frobs (type element-type &rest scs)
             `(progn
                (define-datavector-reffer+offset ,(symbolicate "DATA-VECTOR-REF-WITH-OFFSET/" type)
                  ,type ,scs
                  ,element-type data-vector-ref-with-offset)
                (define-datavector-setter+offset ,(symbolicate "DATA-VECTOR-SET-WITH-OFFSET/" type)
                  ,type ,scs
                  ,element-type data-vector-set-with-offset))))
  (def-full-data-vector-frobs simple-vector * descriptor-reg any-reg)
  (def-full-data-vector-frobs simple-array-unsigned-byte-32 unsigned-num
    unsigned-reg)
  (def-full-data-vector-frobs simple-array-signed-byte-30 tagged-num any-reg)
  (def-full-data-vector-frobs simple-array-unsigned-byte-29 positive-fixnum any-reg)
  (def-full-data-vector-frobs simple-array-signed-byte-32 signed-num
    signed-reg)
  (def-full-data-vector-frobs simple-array-unsigned-byte-31 unsigned-num
    unsigned-reg)
  #!+sb-unicode
  (def-full-data-vector-frobs simple-character-string character character-reg))

;; (define-full-compare-and-swap %compare-and-swap-svref simple-vector
;;   vector-data-offset other-pointer-lowtag
;;   (descriptor-reg any-reg) *
;;   %compare-and-swap-svref)

;;;; integer vectors whose elements are smaller than a byte, i.e.,
;;;; bit, 2-bit, and 4-bit vectors

(macrolet ((def-small-data-vector-frobs (type bits)
             (let* ((elements-per-word (floor n-word-bits bits))
                    (bit-shift (1- (integer-length elements-per-word))))
    `(progn
       (define-vop (,(symbolicate 'data-vector-ref-with-offset/ type))
         (:note "inline array access")
         (:translate data-vector-ref-with-offset)
         (:policy :fast-safe)
         (:args (object :scs (descriptor-reg))
                (index :scs (unsigned-reg)))
         (:info offset)
         (:arg-types ,type positive-fixnum (:constant (integer 0 0)))
         (:results (result :scs (unsigned-reg) :from (:argument 0)))
         (:result-types positive-fixnum)
         (:generator 20
           (aver (zerop offset))

           (%find-property "getData")
           (avm2 :get-local/object object)
           (%call-property "getData" 1)
           (avm2 :get-local/unsigned index)
           (avm2 :push-int ,bit-shift)
           (avm2 :rshift)
           (avm2 :get-property (multiname-l "" ""))

           (avm2 :get-local/unsigned index)
           ,@(unless (= elements-per-word n-word-bits)
               `((avm2 :push-int ,(1- elements-per-word))
                 (avm2 :bit-and)
                 (avm2 :push-int ,(1- (integer-length bits)))
                 (avm2 :lshift)))
           (avm2 :unsigned-rshift)

           (avm2 :push-int ,(1- (ash 1 bits)))
           (avm2 :bit-and)
           (avm2 :set-local result)))
       (define-vop (,(symbolicate 'data-vector-ref-c-with-offset/ type))
         (:translate data-vector-ref-with-offset)
         (:policy :fast-safe)
         (:args (object :scs (descriptor-reg)))
         (:arg-types ,type (:constant index) (:constant (integer 0 0)))
         (:info index offset)
         (:results (result :scs (unsigned-reg)))
         (:result-types positive-fixnum)
         (:generator 15
           (aver (zerop offset))
           (%find-property "getData")
           (avm2 :get-local/object object)
           (%call-property "getData" 1)
           (multiple-value-bind (word extra) (floor index ,elements-per-word)
             (avm2 :push-int word)
             (avm2 :get-property (multiname-l "" ""))
             (unless (zerop extra)
               (avm2 :push-int (* extra ,bits))
               (avm2 :unsigned-rshift))
             (unless (= extra ,(1- elements-per-word))
               (avm2 :push-int ,(1- (ash 1 bits)))
               (avm2 :bit-and))
             (avm2 :set-local result))))
       (define-vop (,(symbolicate 'data-vector-set-with-offset/ type))
         (:note "inline array store")
         (:translate data-vector-set-with-offset)
         (:policy :fast-safe)
         (:args (object :scs (descriptor-reg) :to (:argument 2))
                (index :scs (unsigned-reg))
                (value :scs (unsigned-reg #+(or) immediate) :target result))
         (:info offset)
         (:arg-types ,type positive-fixnum (:constant (integer 0 0))
                     positive-fixnum)
         (:results (result :scs (unsigned-reg)))
         (:result-types positive-fixnum)
         (:temporary (:sc unsigned-reg) word-index)
         (:temporary (:sc unsigned-reg) inner-word-shift)
         (:temporary (:sc descriptor-reg) data-vector)
         (:generator 25
           (aver (zerop offset))

           (%find-property "getData")
           (avm2 :get-local/object object)
           (%call-property "getData" 1)
                                        ;data-vector
           (avm2 :dup)
                                        ;data-vector data-vector
           (avm2 :set-local data-vector)
                                        ;data-vector

           (avm2 :get-local/unsigned index)
                                        ;data-vector index
           (avm2 :push-int ,bit-shift)
                                        ;data-vector index shift
           (avm2 :rshift)
                                        ;data-vector shifted-index
           (avm2 :dup)
                                        ;data-vector shifted-index shifted-index
           (avm2 :set-local word-index)
                                        ;data-vector shifted-index
           (avm2 :get-property (multiname-l "" ""))
                                        ;old

           (avm2 :push-int ,(lognot (1- (ash 1 bits))))
                                        ;old low-mask
           (avm2 :get-local/unsigned index)
                                        ;old low-mask index
           ,@(unless (= elements-per-word n-word-bits)
               `((avm2 :push-int ,(1- elements-per-word))
                 (avm2 :bit-and)
                 (avm2 :push-int ,(1- (integer-length bits)))
                 (avm2 :lshift)))
                                        ;old low-mask inner-word-shift
           (avm2 :dup)
                                        ;old low-mask inner-word-shift inner-word-shift
           (avm2 :set-local inner-word-shift)
                                        ;old low-mask inner-word-shift
           (avm2 :lshift)
                                        ;old shifted-mask
           (avm2 :bit-and)
                                        ;clean-old

           (avm2 :get-local/unsigned value)
                                        ;clean-old new-value-low
           (avm2 :get-local/unsigned inner-word-shift)
                                        ;clean-old new-value-low inner-word-shift
           (avm2 :lshift)
                                        ;clean-old new-value-shifted
           (avm2 :bit-or)
                                        ;new

           (avm2 :get-local/object data-vector)
                                        ;new data-vector
           (avm2 :swap)
                                        ;data-vector new
           (avm2 :get-local/object word-index)
                                        ;data-vector new word-index
           (avm2 :swap)
                                        ;data-vector word-index new
           (avm2 :set-property (multiname-l "" ""))
                                        ;(empty)

           (avm2 :get-local/object value)
           (avm2 :set-local result)))
       (define-vop (,(symbolicate 'data-vector-set-c-with-offset/ type))
         (:translate data-vector-set-with-offset)
         (:policy :fast-safe)
         (:args (object :scs (descriptor-reg))
                (value :scs (unsigned-reg) :target result))
         (:arg-types ,type (:constant index) (:constant (integer 0 0))
                     positive-fixnum)
         (:info index offset)
         (:results (result :scs (unsigned-reg)))
         (:result-types positive-fixnum)
         (:temporary (:sc unsigned-reg :to (:result 0)) data-vector)
         (:generator 20
           (aver (zerop offset))
           (%find-property "getData")
           (avm2 :get-local/object object)
           (%call-property "getData" 1)
                                        ;data-vector
           (avm2 :dup)
                                        ;data-vector data-vector
           (avm2 :set-local data-vector)
                                        ;data-vector

           (multiple-value-bind (word extra) (floor index ,elements-per-word)
             (avm2 :push-int word)
                                        ;data-vector word
             (avm2 :get-property (multiname-l "" ""))
                                        ;old
             (let ((shift (* extra ,bits)))
               (avm2 :push-int ,(lognot (1- (ash 1 bits))))
                                        ;old low-mask
               (avm2 :push-int shift)
                                        ;old low-mask shift
               (avm2 :lshift)
                                        ;old shifted-mask
               (avm2 :bit-and)
                                        ;clean-old
               (avm2 :get-local/unsigned value)
                                        ;clean-old new-value-low
               (avm2 :push-int shift)
                                        ;clean-old new-value-low shift
               (avm2 :lshift)
                                        ;clean-old new-value-shifted
               (avm2 :bit-or)
                                        ;new
               (avm2 :get-local/object data-vector)
                                        ;new data-vector
               (avm2 :swap)
                                        ;data-vector new
               (avm2 :push-int word)
                                        ;data-vector new word
               (avm2 :swap)
                                        ;data-vector word new
               (avm2 :set-property (multiname-l "" ""))
                                        ;(empty)
               (avm2 :get-local/object value)
               (avm2 :set-local result)))))))))
  (def-small-data-vector-frobs simple-bit-vector 1)
  (def-small-data-vector-frobs simple-array-unsigned-byte-2 2)
  (def-small-data-vector-frobs simple-array-unsigned-byte-4 4))

;;; And the float variants.

(define-vop (data-vector-ref-with-offset/simple-array-single-float)
  (:note "inline array access")
  (:translate data-vector-ref-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (unsigned-reg)))
  (:info offset)
  (:arg-types simple-array-single-float positive-fixnum
              (:constant integer))
  (:results (value :scs (single-reg)))
  (:result-types single-float)
  (:generator 5
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :get-local/unsigned index)
    (avm2 :get-property (multiname-l "" ""))
    (avm2 :set-local value)))

(define-vop (data-vector-set-with-offset/simple-array-single-float)
  (:note "inline array store")
  (:translate data-vector-set-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate))
         (value :scs (single-reg) :target result))
  (:info offset)
  (:arg-types simple-array-single-float positive-fixnum
              (:constant integer)
              single-float)
  (:results (result :scs (single-reg)))
  (:result-types single-float)
  (:generator 5
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :get-local/unsigned index)
    (avm2 :get-local/object value)
    (avm2 :set-property (multiname-l "" ""))
    (avm2 :get-local/object value)
    (avm2 :set-local result)))

(define-vop (data-vector-ref-with-offset/simple-array-double-float)
  (:note "inline array access")
  (:translate data-vector-ref-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate)))
  (:info offset)
  (:arg-types simple-array-double-float
              positive-fixnum
              (:constant integer))
  (:results (value :scs (double-reg)))
  (:result-types double-float)
  (:generator 7
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :get-local/unsigned index)
    (avm2 :get-property (multiname-l "" ""))
    (avm2 :set-local value)))

(define-vop (data-vector-set-with-offset/simple-array-double-float)
  (:note "inline array store")
  (:translate data-vector-set-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate))
         (value :scs (double-reg) :target result))
  (:info offset)
  (:arg-types simple-array-double-float positive-fixnum
              (:constant integer)
              double-float)
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:generator 20
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :get-local/unsigned index)
    (avm2 :get-local/object value)
    (avm2 :set-property (multiname-l "" ""))
    (avm2 :get-local/object value)
    (avm2 :set-local result)))

;;; complex float variants

(define-vop (data-vector-ref-with-offset/simple-array-complex-single-float)
  (:note "inline array access")
  (:translate data-vector-ref-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate)))
  (:info offset)
  (:arg-types simple-array-complex-single-float positive-fixnum
              (:constant integer))
  (:results (value :scs (complex-single-reg)))
  (:result-types complex-single-float)
  (:generator 5
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :dup)
    ;; vector vector

    (avm2 :get-local/unsigned index)
    ;; vector vector index
    (avm2 :get-property (multiname-l "" ""))
    ;; vector vector real
    (avm2 :set-local (complex-single-reg-real-tn value))
    ;; vector

    (avm2 :get-local/unsigned index)
    (avm2 :increment-i)
    ;; vector index+1
    (avm2 :get-property (multiname-l "" ""))
    ;; vector imag
    (avm2 :set-local (complex-single-reg-imag-tn value))))

(define-vop (data-vector-set-with-offset/simple-array-complex-single-float)
  (:note "inline array store")
  (:translate data-vector-set-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate))
         (value :scs (complex-single-reg) :target result))
  (:info offset)
  (:arg-types simple-array-complex-single-float positive-fixnum
              (:constant integer)
              complex-single-float)
  (:results (result :scs (complex-single-reg)))
  (:result-types complex-single-float)
  (:generator 5
    (aver (zerop offset))
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :dup)
    ;; vector vector

    (avm2 :get-local/unsigned index)
    ;; vector vector index
    (avm2 :get-local/object (complex-single-reg-real-tn value))
    ;; vector vector new-real
    (avm2 :set-property (multiname-l "" ""))
    ;; vector

    (avm2 :get-local/unsigned index)
    (avm2 :increment-i)
    ;; vector index+1
    (avm2 :get-local/object (complex-single-reg-imag-tn value))
    ;; vector index+1 new-imag
    (avm2 :set-property (multiname-l "" ""))

    (avm2 :get-local/object (complex-single-reg-real-tn value))
    (avm2 :set-local (complex-single-reg-real-tn result))
    (avm2 :get-local/object (complex-single-reg-imag-tn value))
    (avm2 :set-local (complex-single-reg-imag-tn result))))

(define-vop (data-vector-ref-with-offset/simple-array-complex-double-float
             data-vector-ref-with-offset/simple-array-complex-single-float)
  (:note "inline array access")
  (:translate data-vector-ref-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate)))
  (:info offset)
  (:arg-types simple-array-complex-double-float positive-fixnum
              (:constant integer))
  (:results (value :scs (complex-double-reg)))
  (:result-types complex-double-float))

(define-vop (data-vector-set-with-offset/simple-array-complex-double-float
             data-vector-set-with-offset/simple-array-complex-single-float)
  (:note "inline array store")
  (:translate data-vector-set-with-offset)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg immediate))
         (value :scs (complex-double-reg) :target result))
  (:info offset)
  (:arg-types simple-array-complex-double-float positive-fixnum
              (:constant integer)
              complex-double-float)
  (:results (result :scs (complex-double-reg)))
  (:result-types complex-double-float))


;;; {un,}signed-byte-8, simple-base-string

;; fixme: we store each byte as a double float here
(macrolet ((define-data-vector-frobs (ptype element-type &rest scs)
  `(progn
    (define-vop (,(symbolicate "DATA-VECTOR-REF-WITH-OFFSET/" ptype))
      (:translate data-vector-ref-with-offset)
      (:policy :fast-safe)
      (:args (object :scs (descriptor-reg))
             (index :scs (unsigned-reg immediate)))
      (:info offset)
      (:arg-types ,ptype positive-fixnum
                  (:constant integer))
      (:results (value :scs ,scs))
      (:result-types ,element-type)
      (:generator 5
        (aver (zerop offset))
        (%find-property "getData")
        (avm2 :get-local/object object)
        (%call-property "getData" 1)
        (sc-case index
          (immediate (avm2 :push-int (tn-value index)))
          (t (avm2 :get-local/unsigned index)))
        (avm2 :get-property (multiname-l "" ""))
        (avm2 :set-local value)))
    (define-vop (,(symbolicate "DATA-VECTOR-SET-WITH-OFFSET/" ptype))
      (:translate data-vector-set-with-offset)
      (:policy :fast-safe)
      (:args (object :scs (descriptor-reg) :to (:eval 0))
             (index :scs (unsigned-reg immediate) :to (:eval 0))
             (value :scs ,scs))
      (:info offset)
      (:arg-types ,ptype positive-fixnum
                  (:constant integer)
                  ,element-type)
      (:results (result :scs ,scs))
      (:result-types ,element-type)
      (:generator 5
        (aver (zerop offset))
        (%find-property "getData")
        (avm2 :get-local/object object)
        (%call-property "getData" 1)
        (sc-case index
          (immediate (avm2 :push-int (tn-value index)))
          (t (avm2 :get-local/unsigned index)))
        (avm2 :get-local/unsigned value)
        (avm2 :set-property (multiname-l "" ""))
        (avm2 :get-local/nosign value)
        (avm2 :set-local result))))))
  (define-data-vector-frobs simple-array-unsigned-byte-7 positive-fixnum
    unsigned-reg signed-reg)
  (define-data-vector-frobs simple-array-unsigned-byte-8 positive-fixnum
    unsigned-reg signed-reg)
  (define-data-vector-frobs simple-array-signed-byte-8 tagged-num
    signed-reg)
  (define-data-vector-frobs simple-base-string character
    character-reg)

  ;; {un,}signed-byte-16
  (define-data-vector-frobs simple-array-unsigned-byte-15 positive-fixnum
    unsigned-reg signed-reg)
  (define-data-vector-frobs simple-array-unsigned-byte-16 positive-fixnum
    unsigned-reg signed-reg)
  (define-data-vector-frobs simple-array-signed-byte-16 tagged-num
    signed-reg))

;;; {un,}signed-byte-16
;; (macrolet ((define-data-vector-frobs (ptype element-type ref-inst &rest scs)
;;     `(progn
;;       (define-vop (,(symbolicate "DATA-VECTOR-REF-WITH-OFFSET/" ptype))
;;         (:translate data-vector-ref-with-offset)
;;         (:policy :fast-safe)
;;         (:args (object :scs (descriptor-reg))
;;                (index :scs (unsigned-reg immediate)))
;;         (:info offset)
;;         (:arg-types ,ptype positive-fixnum
;;                     (:constant (constant-displacement other-pointer-lowtag
;;                                                       2 vector-data-offset)))
;;         (:results (value :scs ,scs))
;;         (:result-types ,element-type)
;;         (:generator 5
;;           (sc-case index
;;             (immediate
;;              (x86inst ,ref-inst value
;;                    (make-ea-for-vector-data object :size :word
;;                                             :offset (+ (tn-value index) offset))))
;;             (t
;;              (x86inst ,ref-inst value
;;                    (make-ea-for-vector-data object :size :word
;;                                             :index index :offset offset))))))
;;       (define-vop (,(symbolicate "DATA-VECTOR-SET-WITH-OFFSET/" ptype))
;;         (:translate data-vector-set-with-offset)
;;         (:policy :fast-safe)
;;         (:args (object :scs (descriptor-reg) :to (:eval 0))
;;                (index :scs (unsigned-reg immediate) :to (:eval 0))
;;                (value :scs ,scs :target eax))
;;         (:info offset)
;;         (:arg-types ,ptype positive-fixnum
;;                     (:constant (constant-displacement other-pointer-lowtag
;;                                                       2 vector-data-offset))
;;                     ,element-type)
;;         (:temporary (:sc unsigned-reg :offset eax-offset :target result
;;                          :from (:argument 2) :to (:result 0))
;;                     eax)
;;         (:results (result :scs ,scs))
;;         (:result-types ,element-type)
;;         (:generator 5
;;           (move eax value)
;;           (sc-case index
;;             (immediate
;;              (x86inst mov (make-ea-for-vector-data
;;                         object :size :word :offset (+ (tn-value index) offset))
;;                    ax-tn))
;;             (t
;;              (x86inst mov (make-ea-for-vector-data object :size :word
;;                                                 :index index :offset offset)
;;                    ax-tn)))
;;           (move result eax))))))
;;   (define-data-vector-frobs simple-array-unsigned-byte-15 positive-fixnum
;;     movzx unsigned-reg signed-reg)
;;   (define-data-vector-frobs simple-array-unsigned-byte-16 positive-fixnum
;;     movzx unsigned-reg signed-reg)
;;   (define-data-vector-frobs simple-array-signed-byte-16 tagged-num
;;     movsx signed-reg))


;;; These vops are useful for accessing the bits of a vector
;;; irrespective of what type of vector it is.
;;; (define-full-reffer+offset raw-bits-with-offset * 0 other-pointer-lowtag (unsigned-reg)
;;;   unsigned-num %raw-bits-with-offset)
;;; (define-full-setter+offset set-raw-bits-with-offset * 0 other-pointer-lowtag (unsigned-reg)
;;;   unsigned-num %set-raw-bits-with-offset)


;;;; miscellaneous array VOPs

(define-avm2slot-reffer get-vector-subtype *
  "widetag" (unsigned-reg) positive-fixnum)

(define-avm2slot-setter set-vector-subtype *
  "widetag" (unsigned-reg) positive-fixnum)
