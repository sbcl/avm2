;;;; function call for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; interfaces to IR2 conversion

;;; Return a wired TN describing the N'th full call argument passing
;;; location.
(!def-vm-support-routine standard-arg-location (n)
  (declare (type unsigned-byte n))
  (if (< n register-arg-count)
      (make-wired-tn *backend-t-primitive-type* descriptor-reg-sc-number
                     (nth n *register-arg-offsets*))
      (make-wired-tn *backend-t-primitive-type* control-stack-sc-number n)))

(defun make-silent-tn (ptype number offset)
  (let ((tn (make-wired-tn ptype number offset)))
;;     (setf (sb!c::tn-kind tn) :component)
    tn))

;;; Make a passing location TN for a local call return PC.
;;;
;;; Always wire the return PC location to the stack in its standard
;;; location.
(!def-vm-support-routine make-return-pc-passing-location (standard)
  (declare (ignore standard))
  (make-silent-tn (primitive-type-or-lose 'system-area-pointer)
                  fp/bp-sc-number return-pc-save-offset))

;;; This is similar to MAKE-RETURN-PC-PASSING-LOCATION, but makes a
;;; location to pass OLD-FP in.
;;;
;;; This is wired in both the standard and the local-call conventions,
;;; because we want to be able to assume it's always there. Besides,
;;; the x86 doesn't have enough registers to really make it profitable
;;; to pass it in a register.
(!def-vm-support-routine make-old-fp-passing-location (standard)
  (declare (ignore standard))
  (make-silent-tn *fixnum-primitive-type* fp/bp-sc-number
                 ocfp-save-offset))

;;; Make the TNs used to hold OLD-FP and RETURN-PC within the current
;;; function. We treat these specially so that the debugger can find
;;; them at a known location.
;;;
;;; Without using a save-tn - which does not make much sense if it is
;;; wired to the stack?
(!def-vm-support-routine make-old-fp-save-location (physenv)
  (declare (ignore physenv))
  (make-silent-tn *fixnum-primitive-type*
                  fp/bp-sc-number
                  ocfp-save-offset))
(!def-vm-support-routine make-return-pc-save-location (physenv)
  (declare (ignore physenv))
  (make-silent-tn (primitive-type-or-lose 'system-area-pointer)
                  fp/bp-sc-number return-pc-save-offset))

;;; Make a TN for the standard argument count passing location. We only
;;; need to make the standard location, since a count is never passed when we
;;; are using non-standard conventions.
(!def-vm-support-routine make-arg-count-location ()
  (make-wired-tn *fixnum-primitive-type* any-reg-sc-number ecx-offset))

;;; Make a TN to hold the number-stack frame pointer. This is allocated
;;; once per component, and is component-live.
(!def-vm-support-routine make-nfp-tn ()
  (make-silent-tn *fixnum-primitive-type* fp/bp-sc-number 0))

(!def-vm-support-routine make-stack-pointer-tn ()
  (make-normal-tn *fixnum-primitive-type*))

(!def-vm-support-routine make-number-stack-pointer-tn ()
  (make-silent-tn *fixnum-primitive-type* fp/bp-sc-number 0))

;;; Return a list of TNs that can be used to represent an unknown-values
;;; continuation within a function.
(!def-vm-support-routine make-unknown-values-locations ()
  (list (make-stack-pointer-tn)
        (make-normal-tn *fixnum-primitive-type*)))

;;; This function is called by the ENTRY-ANALYZE phase, allowing
;;; VM-dependent initialization of the IR2-COMPONENT structure. We
;;; push placeholder entries in the CONSTANTS to leave room for
;;; additional noise in the code object header.
(!def-vm-support-routine select-component-format (component)
  (declare (type component component))
  ;; The 1+ here is because for the x86 the first constant is a
  ;; pointer to a list of fixups, or NIL if the code object has none.
  ;; (If I understand correctly, the fixups are needed at GC copy
  ;; time because the X86 code isn't relocatable.)
  ;;
  ;; KLUDGE: It'd be cleaner to have the fixups entry be a named
  ;; element of the CODE (aka component) primitive object. However,
  ;; it's currently a large, tricky, error-prone chore to change
  ;; the layout of any primitive object, so for the foreseeable future
  ;; we'll just live with this ugliness. -- WHN 2002-01-02
  (dotimes (i (1+ code-constants-offset))
    (vector-push-extend nil
                        (ir2-component-constants (component-info component))))
  (values))

;;;; frame hackery

;;; This is used for setting up the Old-FP in local call.
(define-vop (current-fp)
  (:results (val :scs (any-reg control-stack)))
  (:generator 1
    (avm2 :push-int #xdeadbeef)
    (avm2 :set-local val)))

;;; We don't have a separate NFP, so we don't need to do anything here.
(define-vop (compute-old-nfp)
  (:results (val))
  (:ignore val)
  (:generator 1
    nil))

(define-vop (xep-allocate-frame)
  (:info start-lab copy-more-arg-follows)
  (:vop-var vop)
  (:generator 1
    (trace-table-entry trace-table-fun-prologue)
    (emit-label start-lab)
    (avm2 :comment "xep starts here")
    (unless copy-more-arg-follows
      ;; ?
      )
    (trace-table-entry trace-table-normal)))

;;; This is emitted directly before either a known-call-local, call-local,
;;; or a multiple-call-local. All it does is allocate stack space for the
;;; callee (who has the same size stack as us).
(define-vop (allocate-frame)
  (:results (res :scs (any-reg control-stack))
            (nfp))
  (:info callee)
  (:ignore nfp callee res)
  (:generator 2
    (avm2 :comment "allocate-frame")))

;;; Allocate a partial frame for passing stack arguments in a full
;;; call. NARGS is the number of arguments passed. We allocate at
;;; least 3 slots, because the XEP noise is going to want to use them
;;; before it can extend the stack.
(define-vop (allocate-full-call-frame)
  (:info nargs)
  (:results (res :scs (any-reg control-stack)))
  (:ignore nargs res)
  (:generator 2
    (avm2 :comment "allocate-full-call-frame")))

;;; Emit code needed at the return-point from an unknown-values call
;;; for a fixed number of values. Values is the head of the TN-REF
;;; list for the locations that the values are to be received into.
;;; Nvals is the number of values that are to be received (should
;;; equal the length of Values).
;;;
;;; MOVE-TEMP is a DESCRIPTOR-REG TN used as a temporary.
;;;
;;; This code exploits the fact that in the unknown-values convention,
;;; a single value return returns at the return PC + 2, whereas a
;;; return of other than one value returns directly at the return PC.
;;;
;;; If 0 or 1 values are expected, then we just emit an instruction to
;;; reset the SP (which will only be executed when other than 1 value
;;; is returned.)
;;;
;;; In the general case we have to do three things:
;;;  -- Default unsupplied register values. This need only be done
;;;     when a single value is returned, since register values are
;;;     defaulted by the called in the non-single case.
;;;  -- Default unsupplied stack values. This needs to be done whenever
;;;     there are stack values.
;;;  -- Reset SP. This must be done whenever other than 1 value is
;;;     returned, regardless of the number of values desired.
(defun default-unknown-values (vop values nvals)
  (declare (type (or tn-ref null) values)
           (type unsigned-byte nvals)
           (ignore vop values nvals))
  (avm2 :comment "fixme: default-unknown-values")
  (values))

;;;; unknown values receiving

;;; Emit code needed at the return point for an unknown-values call
;;; for an arbitrary number of values.
;;;
;;; We do the single and non-single cases with no shared code: there
;;; doesn't seem to be any potential overlap, and receiving a single
;;; value is more important efficiency-wise.
;;;
;;; When there is a single value, we just push it on the stack,
;;; returning the old SP and 1.
;;;
;;; When there is a variable number of values, we move all of the
;;; argument registers onto the stack, and return ARGS and NARGS.
;;;
;;; ARGS and NARGS are TNs wired to the named locations. We must
;;; explicitly allocate these TNs, since their lifetimes overlap with
;;; the results start and count. (Also, it's nice to be able to target
;;; them.)
(defun receive-unknown-values (args nargs start count)
  (declare (type tn args nargs start count)
           (ignore args nargs start count))
  (avm2 :comment "fixme: receive-unknown-values")
  (values))

;;; VOP that can be inherited by unknown values receivers. The main thing this
;;; handles is allocation of the result temporaries.
(define-vop (unknown-values-receiver)
  (:temporary (:sc descriptor-reg :offset ebx-offset
                   :from :eval :to (:result 0))
              values-start)
  (:temporary (:sc any-reg :offset ecx-offset
               :from :eval :to (:result 1))
              nvals)
  (:results (start :scs (any-reg control-stack))
            (count :scs (any-reg control-stack))))

;;;; local call with unknown values convention return

;;; Non-TR local call for a fixed number of values passed according to
;;; the unknown values convention.
;;;
;;; FP is the frame pointer in install before doing the call.
;;;
;;; NFP would be the number-stack frame pointer if we had a separate
;;; number stack.
;;;
;;; Args are the argument passing locations, which are specified only
;;; to terminate their lifetimes in the caller.
;;;
;;; VALUES are the return value locations (wired to the standard
;;; passing locations). NVALS is the number of values received.
;;;
;;; Save is the save info, which we can ignore since saving has been
;;; done.
;;;
;;; TARGET is a continuation pointing to the start of the called
;;; function.
(define-vop (call-local)
  (:args (fp)
         (nfp)
         (args :more t))
  (:results (values :more t))
  (:save-p t)
  (:move-args :local-call)
  (:info arg-locs callee target nvals)
  (:vop-var vop)
  (:ignore nfp arg-locs args #+nil callee
           callee target fp)
  (:generator 5
    (trace-table-entry trace-table-call-site)
    (note-this-location vop :call-site)

    (avm2 :get-local/object 0)                  ;this, i.e. the function we're in
    (avm2 :push-int 42)                 ;fixme: need the xep index here
    ;; fixme: arguments go here
    (%call-property "invoke" 1)
    ;; fixme: store first return value somewhere

    (default-unknown-values vop values nvals)
    (trace-table-entry trace-table-normal)))

;;; Non-TR local call for a variable number of return values passed according
;;; to the unknown values convention. The results are the start of the values
;;; glob and the number of values received.
(define-vop (multiple-call-local unknown-values-receiver)
  (:args (fp)
         (nfp)
         (args :more t))
  (:save-p t)
  (:move-args :local-call)
  (:info save callee target)
  (:ignore args save nfp #+nil callee
           callee fp target)
  (:vop-var vop)
  (:generator 20
    (trace-table-entry trace-table-call-site)
    (note-this-location vop :call-site)

    (avm2 :get-local/object 0)                  ;this, i.e. the function we're in
    (avm2 :push-int 42)                 ;fixme: need the xep index here
    ;; fixme: arguments go here
    (%call-property "invoke" 1)
    ;; fixme: store first return value somewhere

    (note-this-location vop :unknown-return)
    (receive-unknown-values values-start nvals start count)
    (trace-table-entry trace-table-normal)))

;;;; local call with known values return

;;; Non-TR local call with known return locations. Known-value return
;;; works just like argument passing in local call.
;;;
;;; Note: we can't use normal load-tn allocation for the fixed args,
;;; since all registers may be tied up by the more operand. Instead,
;;; we use MAYBE-LOAD-STACK-TN.
(define-vop (known-call-local)
  (:args (fp)
         (nfp)
         (args :more t))
  (:results (res :more t))
  (:move-args :local-call)
  (:save-p t)
  (:info save callee target)
  (:ignore args res save nfp #+nil callee
           fp nfp args callee target )
  (:vop-var vop)
  (:generator 5
    (trace-table-entry trace-table-call-site)
    (note-this-location vop :call-site)

    (avm2 :get-local/object 0)                  ;this, i.e. the function we're in
    (avm2 :push-int 42)                 ;fixme: need the xep index here
    ;; fixme: arguments go here
    (%call-property "invoke" 1)
    ;; fixme: store first return value somewhere

    (note-this-location vop :known-return)
    (trace-table-entry trace-table-normal)))


;;; Return from known values call. We receive the return locations as
;;; arguments to terminate their lifetimes in the returning function. We
;;; restore FP and CSP and jump to the Return-PC.
;;;
;;; The old-fp may be either in a register or on the stack in its
;;; standard save locations - slot 0.
;;;
;;; The return-pc may be in a register or on the stack in any slot.
(define-vop (known-return)
  (:args (old-fp)
         (return-pc)
         (vals :more t))
  (:move-args :known-return)
  (:info val-locs)
  (:ignore val-locs vals old-fp return-pc vals)
  (:vop-var vop)
  (:generator 6
    (trace-table-entry trace-table-fun-epilogue)

    ;; fixme:
    (avm2 :push-string "<replace me with the primary return value>")
    (avm2 :return-value)

    (trace-table-entry trace-table-normal)))

;;;; full call
;;;
;;; There is something of a cross-product effect with full calls.
;;; Different versions are used depending on whether we know the
;;; number of arguments or the name of the called function, and
;;; whether we want fixed values, unknown values, or a tail call.
;;;
;;; In full call, the arguments are passed creating a partial frame on
;;; the stack top and storing stack arguments into that frame. On
;;; entry to the callee, this partial frame is pointed to by FP.

;;; This macro helps in the definition of full call VOPs by avoiding
;;; code replication in defining the cross-product VOPs.
;;;
;;; NAME is the name of the VOP to define.
;;;
;;; NAMED is true if the first argument is an fdefinition object whose
;;; definition is to be called.
;;;
;;; RETURN is either :FIXED, :UNKNOWN or :TAIL:
;;; -- If :FIXED, then the call is for a fixed number of values, returned in
;;;    the standard passing locations (passed as result operands).
;;; -- If :UNKNOWN, then the result values are pushed on the stack, and the
;;;    result values are specified by the Start and Count as in the
;;;    unknown-values continuation representation.
;;; -- If :TAIL, then do a tail-recursive call. No values are returned.
;;;    The Old-Fp and Return-PC are passed as the second and third arguments.
;;;
;;; In non-tail calls, the pointer to the stack arguments is passed as
;;; the last fixed argument. If Variable is false, then the passing
;;; locations are passed as a more arg. Variable is true if there are
;;; a variable number of arguments passed on the stack. Variable
;;; cannot be specified with :TAIL return. TR variable argument call
;;; is implemented separately.
;;;
;;; In tail call with fixed arguments, the passing locations are
;;; passed as a more arg, but there is no new-FP, since the arguments
;;; have been set up in the current frame.
(macrolet ((define-full-call (name named return variable)
            ;; (aver (not (and variable (eq return :tail))))
            `(define-vop (,name
                          ,@(when (eq return :unknown)
                              '(unknown-values-receiver)))
               (:args
               ,@(unless (eq return :tail)
                   '((new-fp :scs (any-reg))))

               (fun :scs (descriptor-reg control-stack))

               ,@(when (eq return :tail)
                   '((old-fp)
                     (return-pc)))

               ,@(unless variable '((args :more t :scs (descriptor-reg)))))

               ,@(when (eq return :fixed)
               '((:results (values :more t))))

               (:save-p ,(if (eq return :tail) :compute-only t))

               ,@(unless (or (eq return :tail) variable)
               '((:move-args :full-call)))

               (:vop-var vop)
               (:info
               ,@(unless (or variable (eq return :tail)) '(arg-locs))
               ,@(unless variable '(nargs))
               ,@(when (eq return :fixed) '(nvals))
               step-instrumenting)

               (:ignore
                ;; kludge
               ,@(unless (eq return :tail) '(new-fp))
               ,@(when (eq return :tail) '(old-fp return-pc))
                ;; end kludge
               step-instrumenting

               ,@(unless (or variable (eq return :tail)) '(arg-locs))
               ,@(unless variable '(args)))

               ;; With variable call, we have to load the
               ;; register-args out of the (new) stack frame before
               ;; doing the call. Therefore, we have to tell the
               ;; lifetime stuff that we need to use them.
;;                ,@(when variable
;;                    (mapcar (lambda (name offset)
;;                              `(:temporary (:sc descriptor-reg
;;                                                :offset ,offset
;;                                                :from (:argument 0)
;;                                                :to :eval)
;;                                           ,name))
;;                            *register-arg-names* *register-arg-offsets*))

               (:generator ,(+ (if named 5 0)
                               (if variable 19 1)
                               (if (eq return :tail) 0 10)
                               15
                               (if (eq return :unknown) 25 0))
               (trace-table-entry trace-table-call-site)

               ,@(if variable
                     (progn
                       ;; For variable call, compute the number of
                       ;; arguments and move some of the arguments to
                       ;; registers.

                       ;; ?
                       ;; (avm2 :push-int 42)
                       ;; (avm2 :set-local <number-of-args>)
                       )
                   '(;; (avm2 :push-int nargs)
                     ;; (avm2 :set-local <number-of-args>)
                     ))

               (note-this-location vop :call-site)

               (avm2 :get-local/object fun)
               ,@ (unless variable      ;fixme!
                    `((avm2 :push-int nargs)))
               ;; fixme: arguments go here
               (%call-property "invoke" 1)
               ;; fixme: store first return value somewhere

               ,@(ecase return
                   (:fixed
                    '((default-unknown-values vop values nvals)))
                   (:unknown
                    '((note-this-location vop :unknown-return)
                      (receive-unknown-values values-start nvals start count)))
                   (:tail))
               (trace-table-entry trace-table-normal)))))

  (define-full-call call nil :fixed nil)
  (define-full-call call-named t :fixed nil)
  (define-full-call multiple-call nil :unknown nil)
  (define-full-call multiple-call-named t :unknown nil)
  (define-full-call tail-call nil :tail nil)
  (define-full-call tail-call-named t :tail nil)

  (define-full-call call-variable nil :fixed t)
  (define-full-call multiple-call-variable nil :unknown t)
  (define-full-call tail-call-variable nil :tail t))


;;;; unknown values return

;;; Return a single-value using the Unknown-Values convention. Specifically,
;;; we jump to clear the stack and jump to return-pc+2.
;;;
;;; We require old-fp to be in a register, because we want to reset ESP before
;;; restoring EBP. If old-fp were still on the stack, it could get clobbered
;;; by a signal.
;;;
;;; pfw--get wired-tn conflicts sometimes if register sc specd for args
;;; having problems targeting args to regs -- using temps instead.
;;;
;;; First off, modifying the return-pc defeats the branch-prediction
;;; optimizations on modern CPUs quite handily. Second, we can do all
;;; this without needing a temp register. Fixed the latter, at least.
;;; -- AB 2006/Feb/04
(define-vop (return-single)
  (:args (old-fp)
         (return-pc)
         (value))
  (:ignore old-fp return-pc)
  (:generator 6
    (trace-table-entry trace-table-fun-epilogue)

    ;; fixme: set number of args somewhere
    (avm2 :get-local/object value)
    (avm2 :return-value)))

;;; Do unknown-values return of a fixed (other than 1) number of
;;; values. The VALUES are required to be set up in the standard
;;; passing locations. NVALS is the number of values returned.
;;;
;;; Basically, we just load ECX with the number of values returned and
;;; EBX with a pointer to the values, set ESP to point to the end of
;;; the values, and jump directly to return-pc.
(define-vop (return)
  (:args (old-fp)
         (return-pc :to (:eval 1))
         (values :more t))
  (:ignore old-fp return-pc)
  (:info nvals)

  (:generator 6
    (trace-table-entry trace-table-fun-epilogue)

    ;; fixme: store NVALS somewhere
    (trace-table-entry trace-table-normal)
    (cond
      ((zerop nvals)
        (avm2 :return-void))
      (t
       ;; only a single return value for now
       (avm2 :get-local/object (tn-ref-tn values))
       (avm2 :return-value)))))

;;; Do unknown-values return of an arbitrary number of values (passed
;;; on the stack.) We check for the common case of a single return
;;; value, and do that inline using the normal single value return
;;; convention. Otherwise, we branch off to code that calls an
;;; assembly-routine.
;;;
;;; The assembly routine takes the following args:
;;;  EAX -- the return-pc to finally jump to.
;;;  EBX -- pointer to where to put the values.
;;;  ECX -- number of values to find there.
;;;  ESI -- pointer to where to find the values.
(define-vop (return-multiple)
  (:args (old-fp :to (:eval 1))
         (return-pc)
         (vals :scs (any-reg))
         (nvals :scs (any-reg)))
  (:ignore old-fp return-pc nvals)
  (:generator 13
    (trace-table-entry trace-table-fun-epilogue)

    (avm2 :get-local/object vals)
    ;; fixme
    (avm2 :return-value)

    (trace-table-entry trace-table-normal)))

;;;; XEP hackery

;;; We don't need to do anything special for regular functions.
(define-vop (setup-environment)
  (:info label)
  (:ignore label)
  (:generator 0
    ;; Don't bother doing anything.
    nil))

;;; Get the lexical environment from its passing location.
(define-vop (setup-closure-environment)
  (:results (closure :scs (descriptor-reg)))
  (:info label)
  (:ignore label)
  (:generator 6
    ;; fixme
    (avm2 :push-string "<when I grow up, I'll be a closure environment>")
    (avm2 :set-local closure)))

;;; Copy a &MORE arg from the argument area to the end of the current
;;; frame. FIXED is the number of non-&MORE arguments.
;;;
;;; The tricky part is doing this without trashing any of the calling
;;; convention registers that are still needed. This vop is emitted
;;; directly after the xep-allocate frame. That means the registers
;;; are in use as follows:
;;;
;;;  EAX -- The lexenv.
;;;  EBX -- Available.
;;;  ECX -- The total number of arguments.
;;;  EDX -- The first arg.
;;;  EDI -- The second arg.
;;;  ESI -- The third arg.
;;;
;;; So basically, we have one register available for our use: EBX.
;;;
;;; What we can do is push the other regs onto the stack, and then
;;; restore their values by looking directly below where we put the
;;; more-args.
(define-vop (copy-more-arg)
  (:info fixed)
  (:vop-var vop)
  (:generator 20
    (%not-implemented vop fixed)))

#+(or)
(define-vop (more-kw-arg)
  (:translate sb!c::%more-kw-arg)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg) :to (:result 1))
         (index :scs (any-reg immediate) :to (:result 1) :target keyword))
  (:arg-types * tagged-num)
  (:results (value :scs (descriptor-reg any-reg))
            (keyword :scs (descriptor-reg any-reg)))
  (:result-types * *)
  (:vop-var vop)
  (:generator 4
    (%not-implemented vop)))

(define-vop (more-arg)
    (:translate sb!c::%more-arg)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg) :to (:result 1))
         (index :scs (any-reg) :to (:result 1) :target value))
  (:arg-types * tagged-num)
  (:results (value :scs (descriptor-reg any-reg)))
  (:result-types *)
  (:vop-var vop)
  (:generator 4
    (%not-implemented vop object index value)))

;;; Turn more arg (context, count) into a list.
(define-vop (listify-rest-args)
  (:translate %listify-rest-args)
  (:policy :safe)
  (:args (context :scs (descriptor-reg))
         (count :scs (any-reg)))
  (:arg-types * tagged-num)
  (:results (result :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 20
    (%not-implemented vop context count result)))

;;; Return the location and size of the &MORE arg glob created by
;;; COPY-MORE-ARG. SUPPLIED is the total number of arguments supplied
;;; (originally passed in ECX). FIXED is the number of non-rest
;;; arguments.
;;;
;;; We must duplicate some of the work done by COPY-MORE-ARG, since at
;;; that time the environment is in a pretty brain-damaged state,
;;; preventing this info from being returned as values. What we do is
;;; compute supplied - fixed, and return a pointer that many words
;;; below the current stack top.
(define-vop (more-arg-context)
  (:policy :fast-safe)
  (:translate sb!c::%more-arg-context)
  (:args (supplied :scs (any-reg) :target count))
  (:arg-types positive-fixnum (:constant fixnum))
  (:info fixed)
  (:results (context :scs (descriptor-reg))
            (count :scs (any-reg)))
  (:result-types t tagged-num)
  (:note "more-arg-context")
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop supplied context count fixed)))

;;; Signal wrong argument count error if NARGS isn't equal to COUNT.
(define-vop (verify-arg-count)
  (:policy :fast-safe)
  (:translate sb!c::%verify-arg-count)
  (:args (nargs :scs (any-reg)))
  (:arg-types positive-fixnum (:constant t))
  (:info count)
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 3
    (avm2 :push-int count)
    (avm2 :get-local/unsigned nargs)
    (let ((ok (gen-label)))
      (avm2 :if-eq ok)
      (avm2 :push-string "invalid-arg-count-error")
      (avm2 :throw)
      (avm2 :%label ok))))

;;; Various other error signallers.
(macrolet ((def (name error translate &rest args)
             `(define-vop (,name)
                ,@(when translate
                    `((:policy :fast-safe)
                      (:translate ,translate)))
                (:args ,@(mapcar (lambda (arg)
                                   `(,arg :scs (any-reg descriptor-reg)))
                                 args))
                (:ignore ,@args)
                (:vop-var vop)
                (:save-p :compute-only)
                (:generator 1000
                  (avm2 :push-string ,(princ-to-string error))
                  (avm2 :throw)))))
  (def arg-count-error invalid-arg-count-error
    sb!c::%arg-count-error nargs)
  (def type-check-error object-not-type-error sb!c::%type-check-error
    object type)
  (def layout-invalid-error layout-invalid-error sb!c::%layout-invalid-error
    object layout)
  (def odd-key-args-error odd-key-args-error
    sb!c::%odd-key-args-error)
  (def unknown-key-arg-error unknown-key-arg-error
    sb!c::%unknown-key-arg-error key)
  (def nil-fun-returned-error nil-fun-returned-error nil fun))

;;; Single-stepping

(defun emit-single-step-test ()
  ;; We use different ways of representing whether stepping is on on
  ;; +SB-THREAD / -SB-THREAD: on +SB-THREAD, we use a slot in the
  ;; thread structure. On -SB-THREAD we use the value of a static
  ;; symbol. Things are done this way, since reading a thread-local
  ;; slot from a symbol would require an extra register on +SB-THREAD,
  ;; and reading a slot from a thread structure would require an extra
  ;; register on -SB-THREAD.
  (avm2 :comment "single step test"))

(define-vop (step-instrument-before-vop)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 3
     nil))
