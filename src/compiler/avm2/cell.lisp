;;;; various primitive memory access VOPs for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; data object ref/set stuff

(define-vop (slot)
  (:args (object :scs (descriptor-reg)))
  ;; KLUDGE: using slot-name rather than offset for primitive object slots;
  ;; still use the offset for instances.
  (:info name offset-or-slot-name lowtag)
  (:ignore name)
  (:results (result :scs (descriptor-reg any-reg)))
  (:generator 1
    (cond
      ((eql lowtag instance-pointer-lowtag)
       (check-type offset-or-slot-name integer)
       (%find-property "getData")
       (avm2 :get-local/object object)
       (%call-property "getData" 1)
       (avm2 :push-int (- offset-or-slot-name sb!vm:instance-slots-offset))
       (avm2 :get-property (multiname-l "" ""))
       (avm2 :set-local result))
      (t
       (check-type offset-or-slot-name (or symbol string))
       (avm2 :get-local/object object)
       (%get-property offset-or-slot-name)
       (avm2 :set-local result)))))

(define-vop (set-slot)
  (:args (object :scs (descriptor-reg))
         (value :scs (descriptor-reg any-reg)))
  ;; KLUDGE: using slot-name rather than offset for primitive object slots;
  ;; still use the offset for instances.
  (:info name offset-or-slot-name lowtag)
  (:ignore name)
  (:results)
  (:generator 1
    (cond
      ((eql lowtag instance-pointer-lowtag)
       (check-type offset-or-slot-name integer)
       (%find-property "getData")
       (avm2 :get-local/object object)
       (%call-property "getData" 1)
       (avm2 :push-int (- offset-or-slot-name instance-slots-offset))
       (avm2 :get-local/object value)
       (avm2 :set-property (multiname-l "" "")))
      (t
       (check-type offset-or-slot-name (or symbol string))
       (avm2 :get-local/object object)
       (avm2 :get-local/object value)
       (%set-property offset-or-slot-name)))))


;;;; symbol hacking VOPs

(define-vop (%compare-and-swap-symbol-value)
  (:translate %compare-and-swap-symbol-value)
  (:args (symbol :scs (descriptor-reg) :to (:result 1))
         (old :scs (descriptor-reg any-reg))
         (new :scs (descriptor-reg any-reg)))
  (:results (result :scs (descriptor-reg any-reg)))
  (:policy :fast-safe)
  (:vop-var vop)
  (:ignore symbol old new result)
  (:generator 15
    (avm2 :push-string "compare and swap not implemented yet")
    (avm2 :throw)))

;; unithreaded it's a lot simpler ...
(define-vop (set cell-set)
  (:variant "_value" #+nil symbol-value-slot other-pointer-lowtag))

(define-vop (symbol-value)
  (:translate symbol-value)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg) :to (:result 1)))
  (:results (value :scs (descriptor-reg any-reg)))
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 9
    (avm2 :get-local/object object)
    (%get-property "symbolValue")
    (avm2 :dup)
    (avm2 :set-local value)

    (%get-property "widetag")
    (let ((ok-label (gen-label)))
      (avm2 :push-int unbound-marker-widetag)
      (avm2 :if-ne ok-label)
      (avm2 :push-string "unbound symbol")
      (avm2 :throw)

      (avm2 :%label ok-label))))

(define-vop (fast-symbol-value cell-ref)
  (:variant "_value" #+nil symbol-value-slot other-pointer-lowtag)
  (:policy :fast)
  (:translate symbol-value))

(defknown locked-symbol-global-value-add (symbol fixnum) fixnum ())

(define-vop (boundp)
  (:translate boundp)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg)))
  (:conditional)
  (:info target not-p)
  (:generator 9
    (avm2 :get-local/object object)
    (%get-property "symbolValue")
    (%get-property "widetag")
    (avm2 :push-int unbound-marker-widetag)
    (avm2 (if not-p :if-eq :if-ne) target)))

(define-vop (symbol-hash)
  (:policy :fast-safe)
  (:translate symbol-hash)
  (:args (symbol :scs (descriptor-reg)))
  (:results (res :scs (any-reg)))
  (:result-types positive-fixnum)
  (:generator 2
    (avm2 :get-local/object symbol)
    (%get-property "symbolHash")
    (avm2 :dup)
    (%find-property "getNil")
    (%call-property "getNil" 0)
    (let ((normal (gen-label)))
      (avm2 :if-ne normal)
      (avm2 :pop)
      (avm2 :push-uint 0)               ;arbitrary hash code for NIL

      (avm2 :%label normal)
      (avm2 :set-local res))))

;;;; fdefinition (FDEFN) objects

(define-vop (fdefn-fun cell-ref)        ; /pfw - alpha
  (:variant "_fun" #+nil fdefn-fun-slot other-pointer-lowtag))

(define-vop (safe-fdefn-fun)
  (:args (object :scs (descriptor-reg) :to (:result 1)))
  (:results (value :scs (descriptor-reg any-reg)))
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 10
    (%not-implemented vop object value)))

(define-vop (set-fdefn-fun)
  (:policy :fast-safe)
  (:translate (setf fdefn-fun))
  (:args (function :scs (descriptor-reg) :target result)
         (fdefn :scs (descriptor-reg)))
  (:results (result :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 38
    (%not-implemented vop function fdefn result)))

(define-vop (fdefn-makunbound)
  (:policy :fast-safe)
  (:translate fdefn-makunbound)
  (:args (fdefn :scs (descriptor-reg) :target result))
  (:results (result :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 38
    (%not-implemented vop fdefn result)))

;;;; binding and unbinding

;;; BIND -- Establish VAL as a binding for SYMBOL. Save the old value and
;;; the symbol on the binding stack and stuff the new value into the
;;; symbol.

(define-vop (bind)
  (:args (val :scs (any-reg descriptor-reg))
         (symbol :scs (descriptor-reg)))
  (:generator 5
    (%find-property "bind")
    (avm2 :get-local/object symbol)
    (avm2 :get-local/object val)
    (%call-property "bind" 2)))

(define-vop (unbind)
  (:generator 0
    (%find-property "unbind")
    (%call-property "unbind" 0)))

(define-vop (unbind-to-here)
  (:args (where :scs (descriptor-reg any-reg)))
  (:generator 0
    (%find-property "unbindToHere")
    (avm2 :get-local/object where)
    (%call-property "unbindToHere" 1)))



;;;; closure indexing

(define-datavector-reffer closure-index-ref *
  (any-reg descriptor-reg) * %closure-index-ref)

(define-datavector-setter set-funcallable-instance-info *
  (any-reg descriptor-reg) * %set-funcallable-instance-info)

(define-datavector-reffer funcallable-instance-info *
  (descriptor-reg any-reg) * %funcallable-instance-info)

(define-vop (closure-ref slot-ref)
  (:variant closure-info-offset fun-pointer-lowtag))

(define-vop (closure-init slot-set)
  (:variant closure-info-offset fun-pointer-lowtag))

;;;; value cell hackery

(define-vop (value-cell-ref cell-ref)
  (:variant "_value" #+nil value-cell-value-slot other-pointer-lowtag))

(define-vop (value-cell-set cell-set)
  (:variant "_value" #+nil value-cell-value-slot other-pointer-lowtag))

;;;; structure hackery

;; instance-length see system.lisp

(define-datavector-reffer instance-index-ref *
  (any-reg descriptor-reg) *
  %instance-ref)

(define-datavector-setter instance-index-set *
  (any-reg descriptor-reg) *
  %instance-set)

;; (define-full-compare-and-swap %compare-and-swap-instance-ref instance
;;   instance-slots-offset instance-pointer-lowtag
;;   (any-reg descriptor-reg) *
;;   %compare-and-swap-instance-ref)

;;;; code object frobbing

(define-datavector-reffer code-header-ref *
  (any-reg descriptor-reg) * code-header-ref)

(define-datavector-setter code-header-set *
  (any-reg descriptor-reg) * code-header-set)

