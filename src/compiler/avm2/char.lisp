;;;; x86 definition of character operations

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; moves and coercions

#!-sb-unicode
(error "can't build for AVM2 without unicode")

;;; Move a tagged char to an untagged representation.
(define-vop (move-to-character)
  (:args (x :scs (any-reg descriptor-reg) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (character-reg)
               :load-if (not (location= x y))))
  (:note "character untagging")
  (:generator 1
    (%unbox-character x)
    (avm2 :set-local y)))

(define-move-vop move-to-character :move
  (any-reg)
  (character-reg))

;;; Move an untagged char to a tagged representation.
(define-vop (move-from-character)
  (:args (x :scs (character-reg) :target y))
  (:results (y :scs (any-reg descriptor-reg)))
  (:note "character tagging")
  (:generator 1
    (%box-character x)
    (avm2 :set-local y)))

(define-move-vop move-from-character :move
  (character-reg)
  (any-reg descriptor-reg))

;;; Move untagged character values.
(define-vop (character-move)
  (:args (x :target y
            :scs (character-reg)
            :load-if (not (location= x y))))
  (:results (y :scs (character-reg character-stack)
               :load-if (not (location= x y))))
  (:note "character move")
  (:effects)
  (:affected)
  (:generator 0
    (avm2 :get-local/unsigned x)
    (avm2 :set-local y)))
(define-move-vop character-move :move
  (character-reg) (character-reg character-stack))

;;; Move untagged character arguments/return-values.
(define-vop (move-character-arg)
  (:args (x :target y
            :scs (character-reg))
         (fp :scs (any-reg)
             :load-if (not (sc-is y character-reg))))
  (:results (y))
  (:note "character arg move")
  (:generator 0
    (sc-case y
      (character-reg
       (avm2 :get-local/unsigned x)
       (avm2 :set-local y))
      (character-stack
       (avm2 :push-string "fixme: move-character-arg")
       (avm2 :throw)))))
(define-move-vop move-character-arg :move-arg
  (any-reg character-reg) (character-reg))

;;; Use standard MOVE-ARG + coercion to move an untagged character
;;; to a descriptor passing location.
(define-move-vop move-arg :move-arg
  (character-reg) (any-reg descriptor-reg))

;;;; other operations

(define-vop (char-code)
  (:translate char-code)
  (:policy :fast-safe)
  (:args (ch :scs (character-reg character-stack) :target res))
  (:arg-types character)
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 1
    (avm2 :get-local/unsigned ch)
    (avm2 :set-local res)))

(define-vop (code-char)
  (:translate code-char)
  (:policy :fast-safe)
  (:args (code :scs (unsigned-reg unsigned-stack) :target res))
  (:arg-types positive-fixnum)
  (:results (res :scs (character-reg)))
  (:result-types character)
  (:generator 1
    (avm2 :get-local/unsigned code)
    (avm2 :set-local res)))

;;; comparison of CHARACTERs
(define-vop (character-compare)
  (:args (x :scs (character-reg character-stack))
         (y :scs (character-reg)
            :load-if (not (and (sc-is x character-reg)
                               (sc-is y character-stack)))))
  (:arg-types character character)
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:note "inline comparison")
  (:variant-vars condition not-condition)
  (:generator 3
    (avm2 :get-local/unsigned x)
    (avm2 :get-local/unsigned y)
    (avm2 (if not-p not-condition condition) target)))

(define-vop (fast-char=/character character-compare)
  (:translate char=)
  (:variant :if-eq :if-ne))

(define-vop (fast-char</character character-compare)
  (:translate char<)
  (:variant :if-lt :if-ge))

(define-vop (fast-char>/character character-compare)
  (:translate char>)
  (:variant :if-gt :if-le))

(define-vop (character-compare/c)
  (:args (x :scs (character-reg character-stack)))
  (:arg-types character (:constant character))
  (:conditional)
  (:info target not-p y)
  (:policy :fast-safe)
  (:note "inline constant comparison")
  (:variant-vars condition not-condition)
  (:generator 2
    (avm2 :get-local/unsigned x)
    (avm2 :push-int y)
    (avm2 (if not-p not-condition condition) target)))

(define-vop (fast-char=/character/c character-compare/c)
  (:translate char=)
  (:variant :if-eq :if-ne))

(define-vop (fast-char</character/c character-compare/c)
  (:translate char<)
  (:variant :if-lt :if-ge))

(define-vop (fast-char>/character/c character-compare/c)
  (:translate char>)
  (:variant :if-gt :if-le))
