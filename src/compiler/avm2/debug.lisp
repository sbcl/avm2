;;;; x86 support for the debugger

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

(defun %not-implemented (x &rest args-to-ignore)
  (declare (ignore args-to-ignore))
  (avm2 :push-string (princ-to-string x))
  (avm2 :throw))

(define-vop (debug-cur-sp)
  (:translate current-sp)
  (:policy :fast-safe)
  (:results (res :scs (sap-reg sap-stack)))
  (:result-types system-area-pointer)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop res)))

(define-vop (debug-cur-fp)
  (:translate current-fp)
  (:policy :fast-safe)
  (:results (res :scs (sap-reg sap-stack)))
  (:result-types system-area-pointer)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop res)))

;;; Stack-ref and %set-stack-ref can be used to read and store
;;; descriptor objects on the control stack. Use the sap-ref
;;; functions to access other data types.
(define-vop (read-control-stack)
  (:translate stack-ref)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to :eval)
         (offset :scs (any-reg)))
  (:arg-types system-area-pointer positive-fixnum)
  (:results (result :scs (descriptor-reg)))
  (:result-types *)
  (:vop-var vop)
  (:generator 9
    (%not-implemented vop sap offset result)))

(define-vop (read-control-stack-c)
  (:translate stack-ref)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg)))
  (:info index)
  (:arg-types system-area-pointer (:constant (signed-byte 30)))
  (:results (result :scs (descriptor-reg)))
  (:result-types *)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop sap index result)))

(define-vop (write-control-stack)
  (:translate %set-stack-ref)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to :eval)
         (offset :scs (any-reg))
         (value :scs (descriptor-reg) :to :result :target result))
  (:arg-types system-area-pointer positive-fixnum *)
  (:results (result :scs (descriptor-reg)))
  (:result-types *)
  (:vop-var vop)
  (:generator 9
    (%not-implemented vop sap offset value result)))

(define-vop (write-control-stack-c)
  (:translate %set-stack-ref)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg))
         (value :scs (descriptor-reg) :target result))
  (:info index)
  (:arg-types system-area-pointer (:constant (signed-byte 30)) *)
  (:results (result :scs (descriptor-reg)))
  (:result-types *)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop sap value index result)))

(define-vop (code-from-mumble)
  (:policy :fast-safe)
  (:args (thing :scs (descriptor-reg)))
  (:results (code :scs (descriptor-reg)))
  (:variant-vars lowtag)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop thing code lowtag)))

(define-vop (code-from-lra code-from-mumble)
  (:translate sb!di::lra-code-header)
  (:variant other-pointer-lowtag))

(define-vop (code-from-function code-from-mumble)
  (:translate sb!di::fun-code-header)
  (:variant fun-pointer-lowtag))

(define-vop (%make-lisp-obj)
  (:policy :fast-safe)
  (:translate %make-lisp-obj)
  (:args (value :scs (unsigned-reg unsigned-stack) :target result))
  (:arg-types unsigned-num)
  (:results (result :scs (descriptor-reg)
                    :load-if (not (sc-is value unsigned-reg))
                    ))
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop value result)))

(define-vop (get-lisp-obj-address)
  (:policy :fast-safe)
  (:translate sb!di::get-lisp-obj-address)
  (:args (thing :scs (descriptor-reg control-stack) :target result))
  (:results (result :scs (unsigned-reg)
                    :load-if (not (and (sc-is thing descriptor-reg)
                                       (sc-is result unsigned-stack)))))
  (:result-types unsigned-num)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop thing result)))


(define-vop (fun-word-offset)
  (:policy :fast-safe)
  (:translate sb!di::fun-word-offset)
  (:args (fun :scs (descriptor-reg)))
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop fun res)))
