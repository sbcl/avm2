;;;; floating point support for the x86

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

#!+long-float
(error "no long floats on AVM2")

;;;; move functions

;;; X is source, Y is destination.
(define-move-fun (load-single 2) (vop x y)
  ((single-stack) (single-reg))
  (avm2 :get-local/object x)
  (avm2 :set-local y))

(define-move-fun (store-single 2) (vop x y)
  ((single-reg) (single-stack))
  (avm2 :get-local/object x)
  (avm2 :set-local y))

(define-move-fun (load-double 2) (vop x y)
  ((double-stack) (double-reg))
  (avm2 :get-local/object x)
  (avm2 :set-local y))

(define-move-fun (store-double 2) (vop x y)
  ((double-reg) (double-stack))
  (avm2 :get-local/object x)
  (avm2 :set-local y))

(define-move-fun (load-fp-constant 2) (vop x y)
  ((fp-constant) (single-reg double-reg))
  (avm2 :push-double (sb!c::constant-value (sb!c::tn-leaf x)))
  (avm2 :set-local y))

;;;; complex float move functions

(defun complex-single-reg-real-tn (x)
  (make-random-tn :kind :normal :sc (sc-or-lose 'single-reg)
                  :offset (tn-offset x)))
(defun complex-single-reg-imag-tn (x)
  (make-random-tn :kind :normal :sc (sc-or-lose 'single-reg)
                  :offset (1+ (tn-offset x))))

(defun complex-double-reg-real-tn (x)
  (make-random-tn :kind :normal :sc (sc-or-lose 'double-reg)
                  :offset (tn-offset x)))
(defun complex-double-reg-imag-tn (x)
  (make-random-tn :kind :normal :sc (sc-or-lose 'double-reg)
                  :offset (1+ (tn-offset x))))

;;; X is source, Y is destination.
(define-move-fun (load-complex-single 2) (vop x y)
  ((complex-single-stack) (complex-single-reg))
  (avm2 :get-local/object (complex-single-reg-real-tn y))
  (avm2 :set-local        (complex-single-reg-real-tn x))
  (avm2 :get-local/object (complex-single-reg-imag-tn y))
  (avm2 :set-local        (complex-single-reg-imag-tn x)))

(define-move-fun (store-complex-single 2) (vop x y)
  ((complex-single-reg) (complex-single-stack))
  (avm2 :get-local/object (complex-single-reg-real-tn y))
  (avm2 :set-local        (complex-single-reg-real-tn x))
  (avm2 :get-local/object (complex-single-reg-imag-tn y))
  (avm2 :set-local        (complex-single-reg-imag-tn x)))

(define-move-fun (load-complex-double 2) (vop x y)
  ((complex-double-stack) (complex-double-reg))
  (avm2 :get-local/object (complex-double-reg-real-tn y))
  (avm2 :set-local        (complex-double-reg-real-tn x))
  (avm2 :get-local/object (complex-double-reg-imag-tn y))
  (avm2 :set-local        (complex-double-reg-imag-tn x)))

(define-move-fun (store-complex-double 2) (vop x y)
  ((complex-double-reg) (complex-double-stack))
  (avm2 :get-local/object (complex-double-reg-real-tn y))
  (avm2 :set-local        (complex-double-reg-real-tn x))
  (avm2 :get-local/object (complex-double-reg-imag-tn y))
  (avm2 :set-local        (complex-double-reg-imag-tn x)))


;;;; move VOPs

;;; float register to register moves
(define-vop (float-move)
  (:args (x))
  (:results (y))
  (:note "float move")
  (:generator 0
    (avm2 :get-local/object x)
    (avm2 :set-local y)))

(define-vop (single-move float-move)
  (:args (x :scs (single-reg) :target y :load-if (not (location= x y))))
  (:results (y :scs (single-reg) :load-if (not (location= x y)))))
(define-move-vop single-move :move (single-reg) (single-reg))

(define-vop (double-move float-move)
  (:args (x :scs (double-reg) :target y :load-if (not (location= x y))))
  (:results (y :scs (double-reg) :load-if (not (location= x y)))))
(define-move-vop double-move :move (double-reg) (double-reg))

;;; complex float register to register moves
(define-vop (complex-float-move)
  (:args (x :target y :load-if (not (location= x y))))
  (:results (y :load-if (not (location= x y))))
  (:note "complex float move")
  (:generator 0
    (avm2 :get-local/object (complex-double-reg-real-tn x))
    (avm2 :set-local        (complex-double-reg-real-tn y))
    (avm2 :get-local/object (complex-double-reg-imag-tn x))
    (avm2 :set-local        (complex-double-reg-imag-tn y))))

(define-vop (complex-single-move complex-float-move)
  (:args (x :scs (complex-single-reg) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (complex-single-reg) :load-if (not (location= x y)))))
(define-move-vop complex-single-move :move
  (complex-single-reg) (complex-single-reg))

(define-vop (complex-double-move complex-float-move)
  (:args (x :scs (complex-double-reg)
            :target y :load-if (not (location= x y))))
  (:results (y :scs (complex-double-reg) :load-if (not (location= x y)))))
(define-move-vop complex-double-move :move
  (complex-double-reg) (complex-double-reg))


;;; Move from float to a descriptor reg. allocating a new float
;;; object in the process.
(define-vop (move-from-single)
  (:args (x :scs (single-reg) :to :save))
  (:results (y :scs (descriptor-reg)))
  (:note "float to pointer coercion")
  (:generator 13
    (%box-float x)
    (avm2 :set-local y)))
(define-move-vop move-from-single :move
  (single-reg) (descriptor-reg))

(define-vop (move-from-double)
  (:args (x :scs (double-reg) :to :save))
  (:results (y :scs (descriptor-reg)))
  (:note "float to pointer coercion")
  (:generator 13
    (%box-float x)
    (avm2 :set-local y)))
(define-move-vop move-from-double :move
  (double-reg) (descriptor-reg))

(define-vop (move-from-fp-constant)
  (:args (x :scs (fp-constant)))
  (:results (y :scs (descriptor-reg)))
  (:generator 2
    (avm2 :push-double x)
    (%box-float)
    (avm2 :set-local y)))
(define-move-vop move-from-fp-constant :move
  (fp-constant) (descriptor-reg))

;;; Move from a descriptor to a float register.
(define-vop (move-to-single)
  (:args (x :scs (descriptor-reg)))
  (:results (y :scs (single-reg)))
  (:note "pointer to float coercion")
  (:generator 2
    (%unbox-float x)
    (avm2 :set-local y)))
(define-move-vop move-to-single :move (descriptor-reg) (single-reg))

(define-vop (move-to-double)
  (:args (x :scs (descriptor-reg)))
  (:results (y :scs (double-reg)))
  (:note "pointer to float coercion")
  (:generator 2
    (%unbox-float x)
    (avm2 :set-local y)))
(define-move-vop move-to-double :move (descriptor-reg) (double-reg))


;;; Move from complex float to a descriptor reg. allocating a new
;;; complex float object in the process.
(define-vop (move-from-complex-single)
  (:args (x :scs (complex-single-reg) :to :save))
  (:results (y :scs (descriptor-reg)))
  (:note "complex float to pointer coercion")
  (:generator 13
    (%box-complex (complex-single-reg-real-tn x)
                  (complex-single-reg-imag-tn x))
    (avm2 :set-local y)))
(define-move-vop move-from-complex-single :move
  (complex-single-reg) (descriptor-reg))

(define-vop (move-from-complex-double)
  (:args (x :scs (complex-double-reg) :to :save))
  (:results (y :scs (descriptor-reg)))
  (:note "complex float to pointer coercion")
  (:generator 13
    (%box-complex (complex-double-reg-real-tn x)
                  (complex-double-reg-imag-tn x))
    (avm2 :set-local y)))
(define-move-vop move-from-complex-double :move
  (complex-double-reg) (descriptor-reg))

;;; Move from a descriptor to a complex float register.
(macrolet ((frob (name sc format)
             `(progn
                (define-vop (,name)
                  (:args (x :scs (descriptor-reg)))
                  (:results (y :scs (,sc)))
                  (:note "pointer to complex float coercion")
                  (:generator 2
                    (%unbox-complex x)
                    (avm2 :set-local (complex-double-reg-imag-tn y))
                    (avm2 :set-local (complex-double-reg-real-tn y))))
                (define-move-vop ,name :move (descriptor-reg) (,sc)))))
          (frob move-to-complex-single complex-single-reg :single)
          (frob move-to-complex-double complex-double-reg :double))

;;;; the move argument vops
;;;;
;;;; Note these are also used to stuff fp numbers onto the c-call
;;;; stack so the order is different than the lisp-stack.

;;; the general MOVE-ARG VOP
(macrolet ((frob (name sc stack-sc format)
             `(progn
                (define-vop (,name)
                  (:args (x :scs (,sc) :target y)
                         (fp :scs (any-reg)
                             :load-if (not (sc-is y ,sc))))
                  (:results (y))
                  (:vop-var vop)
                  (:note "float argument move")
                  (:generator ,(case format (:single 2) (:double 3) (:long 4))
                    (sc-case y
                      (,sc
                       (avm2 :get-local/object x)
                       (avm2 :set-local y))
                      (,stack-sc
                       (%not-implemented vop)))))
                (define-move-vop ,name :move-arg
                  (,sc descriptor-reg) (,sc)))))
  (frob move-single-float-arg single-reg single-stack :single)
  (frob move-double-float-arg double-reg double-stack :double))

;;;; complex float MOVE-ARG VOP
(macrolet ((frob (name sc stack-sc format)
             `(progn
                (define-vop (,name)
                  (:args (x :scs (,sc) :target y)
                         (fp :scs (any-reg)))
                  (:ignore fp)
                  (:results (y))
                  (:vop-var vop)
                  (:note "complex float argument move")
                  (:generator ,(ecase format (:single 2) (:double 3) (:long 4))
                    (sc-case y
                      (,sc
                       (avm2 :get-local/object (complex-double-reg-real-tn x))
                       (avm2 :set-local (complex-double-reg-real-tn y))
                       (avm2 :get-local/object (complex-double-reg-imag-tn x))
                       (avm2 :set-local (complex-double-reg-imag-tn y)))
                      (,stack-sc
                       (%not-implemented vop)))))
                (define-move-vop ,name :move-arg
                  (,sc descriptor-reg) (,sc)))))
  (frob move-complex-single-float-arg
        complex-single-reg complex-single-stack :single)
  (frob move-complex-double-float-arg
        complex-double-reg complex-double-stack :double))

(define-move-vop move-arg :move-arg
  (single-reg double-reg
   complex-single-reg complex-double-reg)
  (descriptor-reg))


;;;; arithmetic VOPs

(macrolet
    ((frob (translate opcode
            sname scost
            dname dcost)
       `(progn
         (define-vop (,sname)
           (:translate ,translate)
           (:args (x :scs (single-reg single-stack)
                     :to :eval)
                  (y :scs (single-reg single-stack)
                     :to :eval))
           (:results (r :scs (single-reg single-stack)))
           (:arg-types single-float single-float)
           (:result-types single-float)
           (:policy :fast-safe)
           (:note "inline float arithmetic")
           (:vop-var vop)
           (:save-p :compute-only)
           (:generator ,scost
             (avm2 :get-local/object x)
             (avm2 :get-local/object y)
             (avm2 ,opcode)
             (avm2 :set-local r)))

         (define-vop (,dname)
           (:translate ,translate)
           (:args (x :scs (double-reg double-stack #+nil descriptor-reg)
                     :to :eval)
                  (y :scs (double-reg double-stack #+nil descriptor-reg)
                     :to :eval))
           (:results (r :scs (double-reg double-stack)))
           (:arg-types double-float double-float)
           (:result-types double-float)
           (:policy :fast-safe)
           (:note "inline float arithmetic")
           (:vop-var vop)
           (:save-p :compute-only)
           (:generator ,dcost
             (avm2 :get-local/object x)
             (avm2 :get-local/object y)
             (avm2 ,opcode)
             (avm2 :set-local r))))))

    (frob + :add
          +/single-float 2
          +/double-float 2)
    (frob - :subtract
          -/single-float 2
          -/double-float 2)
    (frob * :multiply
          */single-float 3
          */double-float 3)
    (frob / :divide
          //single-float 12
          //double-float 12))

(defun %Math (name &rest args)
  (%find-property "Math")
  (%get-property "Math")
  (dolist (arg args)
    (avm2 :get-local/object arg))
  (%call-property name (length args)))

(macrolet ((frob (name inst translate sc type)
             `(define-vop (,name)
               (:args (x :scs (,sc)))
               (:results (y :scs (,sc)))
               (:translate ,translate)
               (:policy :fast-safe)
               (:arg-types ,type)
               (:result-types ,type)
               (:note "inline float arithmetic")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 1
                (%Math "abs" x)
                (avm2 :set-local y)))))

  (frob abs/single-float fabs abs single-reg single-float)
  (frob abs/double-float fabs abs double-reg double-float))

(macrolet ((frob (name inst translate sc type)
             `(define-vop (,name)
               (:args (x :scs (,sc)))
               (:results (y :scs (,sc)))
               (:translate ,translate)
               (:policy :fast-safe)
               (:arg-types ,type)
               (:result-types ,type)
               (:note "inline float arithmetic")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 1
                (avm2 :get-local/object x)
                (avm2 ,inst)
                (avm2 :set-local y)))))

  (frob %negate/single-float :negate %negate single-reg single-float)
  (frob %negate/double-float :negate %negate double-reg double-float))

;;;; comparison

(define-vop (=/float)
  (:args (x) (y))
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:vop-var vop)
  (:save-p :compute-only)
  (:note "inline float comparison")
  (:ignore temp)
  (:generator 3
    (avm2 :get-local/object x)
    (avm2 :get-local/object y)
    (avm2 (if not-p :if-ne :if-eq) target)))

(define-vop (=/single-float =/float)
  (:translate =)
  (:args (x :scs (single-reg))
         (y :scs (single-reg)))
  (:arg-types single-float single-float))

(define-vop (=/double-float =/float)
  (:translate =)
  (:args (x :scs (double-reg))
         (y :scs (double-reg)))
  (:arg-types double-float double-float))

(define-vop (<single-float)
  (:translate <)
  (:args (x :scs (single-reg single-stack descriptor-reg))
         (y :scs (single-reg single-stack descriptor-reg)))
  (:arg-types single-float single-float)
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:note "inline float comparison")
  (:ignore temp)
  (:generator 3
    (avm2 :get-local/object x)
    (avm2 :get-local/object y)
    (avm2 (if not-p :if-ge :if-lt) target)))

(define-vop (<double-float)
  (:translate <)
  (:args (x :scs (double-reg double-stack descriptor-reg))
         (y :scs (double-reg double-stack descriptor-reg)))
  (:arg-types double-float double-float)
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:note "inline float comparison")
  (:ignore temp)
  (:generator 3
    (avm2 :get-local/object x)
    (avm2 :get-local/object y)
    (avm2 (if not-p :if-ge :if-lt) target)))

(define-vop (>single-float)
  (:translate >)
  (:args (x :scs (single-reg single-stack descriptor-reg))
         (y :scs (single-reg single-stack descriptor-reg)))
  (:arg-types single-float single-float)
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:note "inline float comparison")
  (:ignore temp)
  (:generator 3
    (avm2 :get-local/object x)
    (avm2 :get-local/object y)
    (avm2 (if not-p :if-le :if-gt) target)))

(define-vop (>double-float)
  (:translate >)
  (:args (x :scs (double-reg double-stack descriptor-reg))
         (y :scs (double-reg double-stack descriptor-reg)))
  (:arg-types double-float double-float)
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:note "inline float comparison")
  (:ignore temp)
  (:generator 3
    (avm2 :get-local/object x)
    (avm2 :get-local/object y)
    (avm2 (if not-p :if-le :if-gt) target)))

(define-vop (float-test)
  (:args (x))
  (:conditional)
  (:info target not-p y)
  (:variant-vars code)
  (:policy :fast-safe)
  (:vop-var vop)
  (:save-p :compute-only)
  (:note "inline float comparison")
  (:ignore temp code)
  (:generator 2
    (avm2 :get-local/object x)
    (avm2 :push-double y)
    (avm2 (if not-p :if-ne :if-eq) target)))

(define-vop (=0/single-float float-test)
  (:translate =)
  (:args (x :scs (single-reg)))
  (:arg-types single-float (:constant (single-float 0f0 0f0))))
(define-vop (=0/double-float float-test)
  (:translate =)
  (:args (x :scs (double-reg)))
  (:arg-types double-float (:constant (double-float 0d0 0d0))))

(define-vop (<0/single-float float-test)
  (:translate <)
  (:args (x :scs (single-reg)))
  (:arg-types single-float (:constant (single-float 0f0 0f0))))
(define-vop (<0/double-float float-test)
  (:translate <)
  (:args (x :scs (double-reg)))
  (:arg-types double-float (:constant (double-float 0d0 0d0))))

(define-vop (>0/single-float float-test)
  (:translate >)
  (:args (x :scs (single-reg)))
  (:arg-types single-float (:constant (single-float 0f0 0f0))))
(define-vop (>0/double-float float-test)
  (:translate >)
  (:args (x :scs (double-reg)))
  (:arg-types double-float (:constant (double-float 0d0 0d0))))

;;;; conversion

(macrolet ((frob (name translate to-sc to-type)
             `(define-vop (,name)
                (:args (x :scs (signed-stack signed-reg)))
                (:results (y :scs (,to-sc)))
                (:arg-types signed-num)
                (:result-types ,to-type)
                (:policy :fast-safe)
                (:note "inline float coercion")
                (:translate ,translate)
                (:vop-var vop)
                (:save-p :compute-only)
                (:generator 5
                  (avm2 :get-local/signed x)
                  (avm2 :set-local y)))))
  (frob %single-float/signed %single-float single-reg single-float)
  (frob %double-float/signed %double-float double-reg double-float))

(macrolet ((frob (name translate to-sc to-type)
             `(define-vop (,name)
                (:args (x :scs (unsigned-reg)))
                (:results (y :scs (,to-sc)))
                (:arg-types unsigned-num)
                (:result-types ,to-type)
                (:policy :fast-safe)
                (:note "inline float coercion")
                (:translate ,translate)
                (:vop-var vop)
                (:save-p :compute-only)
                (:generator 6
                 (avm2 :get-local/unsigned x)
                 (avm2 :set-local y)))))
  (frob %single-float/unsigned %single-float single-reg single-float)
  (frob %double-float/unsigned %double-float double-reg double-float))

;;; These should be no-ops but the compiler might want to move some
;;; things around.
(macrolet ((frob (name translate from-sc from-type to-sc to-type)
             `(define-vop (,name)
               (:args (x :scs (,from-sc) :target y))
               (:results (y :scs (,to-sc)))
               (:arg-types ,from-type)
               (:result-types ,to-type)
               (:policy :fast-safe)
               (:note "inline float coercion")
               (:translate ,translate)
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 2
                (avm2 :get-local/object x)
                (avm2 :set-local y)))))

  (frob %single-float/double-float %single-float double-reg
        double-float single-reg single-float)
  (frob %double-float/single-float %double-float single-reg single-float
        double-reg double-float))

;; fixme: isn't there an easier way to do this?
(defun %sign (&optional x)
  (when x
    (avm2 :get-local/object x))
  (let ((plus-label (gen-label))
        (done-label (gen-label)))
    (avm2 :push-double 0.0d0)
    (avm2 :if-ge plus-label)

    (avm2 :push-double -1.0d0)
    (avm2 :jump done-label)

    (avm2 :%label plus-label)
    (avm2 :push-double 1.0d0)
    (avm2 :%label done-label)))

(macrolet ((frob (trans from-sc from-type)
             `(define-vop (,(symbolicate trans "/" from-type))
               (:args (x :scs (,from-sc)))
               (:temporary (:sc signed-stack) tmp)
               (:results (y :scs (signed-reg)))
               (:arg-types ,from-type)
               (:result-types signed-num)
               (:translate ,trans)
               (:policy :fast-safe)
               (:note "inline float truncate")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 5
                (%Math "abs" x)
                (avm2 :set-local tmp)
                (%Math "floor" tmp)
                (%sign x)
                (avm2 :multiply)
                (avm2 :set-local y)))))
  (frob %unary-truncate single-reg single-float)
  (frob %unary-truncate double-reg double-float))

(macrolet ((frob (trans from-sc from-type)
             `(define-vop (,(symbolicate trans "/" from-type))
               (:args (x :scs (,from-sc)))
               (:results (y :scs (signed-reg)))
               (:arg-types ,from-type)
               (:result-types signed-num)
               (:translate ,trans)
               (:policy :fast-safe)
               (:note "inline float truncate")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 5
                (%Math "round" x)
                (avm2 :set-local y)))))
  (frob %unary-round single-reg single-float)
  (frob %unary-round double-reg double-float))

(macrolet ((frob (trans from-sc from-type round-p)
             `(define-vop (,(symbolicate trans "/" from-type "=>UNSIGNED"))
               (:args (x :scs (,from-sc)))
               (:results (y :scs (unsigned-reg)))
               (:arg-types ,from-type)
               (:result-types unsigned-num)
               (:translate ,trans)
               (:policy :fast-safe)
               (:note "inline float truncate")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 5
                (avm2 :get-local/object x)
                (avm2 :convert-unsigned)
                (avm2 :set-local y)))))
  (frob %unary-truncate single-reg single-float nil)
  (frob %unary-truncate double-reg double-float nil))

(macrolet ((frob (trans from-sc from-type)
             `(define-vop (,(symbolicate trans "/" from-type "=>UNSIGNED"))
               (:args (x :scs (,from-sc)))
               (:results (y :scs (unsigned-reg)))
               (:arg-types ,from-type)
               (:result-types unsigned-num)
               (:translate ,trans)
               (:policy :fast-safe)
               (:note "inline float truncate")
               (:vop-var vop)
               (:save-p :compute-only)
               (:generator 5
                (%Math "round" x)
                (avm2 :convert-unsigned)
                (avm2 :set-local y)))))
  (frob %unary-round single-reg single-float)
  (frob %unary-round double-reg double-float))

(define-vop (make-single-float)
  (:args (bits :scs (signed-reg) :target res
               :load-if (not (or (and (sc-is bits signed-stack)
                                      (sc-is res single-reg))
                                 (and (sc-is bits signed-stack)
                                      (sc-is res single-stack)
                                      (location= bits res))))))
  (:results (res :scs (single-reg single-stack)))
  (:arg-types signed-num)
  (:result-types single-float)
  (:translate make-single-float)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 4
    ;; fixme: can the new undocumented instructions be used for this?
    ;; http://ncannasse.fr/blog/adobe_alchemy
    ;; 0x3e lf64 - load a 64 bit (IEEE 754) float from global memory
    ;; ...
    (%not-implemented vop bits res)))

(define-vop (make-double-float)
  (:args (hi-bits :scs (signed-reg))
         (lo-bits :scs (unsigned-reg)))
  (:results (res :scs (double-reg)))
  (:arg-types signed-num unsigned-num)
  (:result-types double-float)
  (:translate make-double-float)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 2
    ;; fixme: can the new undocumented instructions be used for this?
    ;; http://ncannasse.fr/blog/adobe_alchemy
    ;; 0x3e lf64 - load a 64 bit (IEEE 754) float from global memory
    ;; ...
    (%not-implemented vop hi-bits lo-bits res)))

(define-vop (single-float-bits)
  (:args (float :scs (single-reg descriptor-reg)
                :load-if (not (sc-is float single-stack))))
  (:results (bits :scs (signed-reg)))
  (:arg-types single-float)
  (:result-types signed-num)
  (:translate single-float-bits)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 4
    ;; fixme: can the new undocumented instructions be used for this?
    ;; http://ncannasse.fr/blog/adobe_alchemy
    ;; 0x3e lf64 - load a 64 bit (IEEE 754) float from global memory
    ;; ...
    (%not-implemented vop float bits)))

(define-vop (double-float-high-bits)
  (:args (float :scs (double-reg descriptor-reg)
                :load-if (not (sc-is float double-stack))))
  (:results (hi-bits :scs (signed-reg)))
  (:arg-types double-float)
  (:result-types signed-num)
  (:translate double-float-high-bits)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 5
    ;; fixme: can the new undocumented instructions be used for this?
    ;; http://ncannasse.fr/blog/adobe_alchemy
    ;; 0x3e lf64 - load a 64 bit (IEEE 754) float from global memory
    ;; ...
    (%not-implemented vop float hi-bits)))

(define-vop (double-float-low-bits)
  (:args (float :scs (double-reg descriptor-reg)
                :load-if (not (sc-is float double-stack))))
  (:results (lo-bits :scs (unsigned-reg)))
  (:arg-types double-float)
  (:result-types unsigned-num)
  (:translate double-float-low-bits)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 5
    ;; fixme: can the new undocumented instructions be used for this?
    ;; http://ncannasse.fr/blog/adobe_alchemy
    ;; 0x3e lf64 - load a 64 bit (IEEE 754) float from global memory
    ;; ...
    (%not-implemented vop float lo-bits)))


;;;; no float modes on AVM2

(sb!xc:deftype float-modes () '(unsigned-byte 32)) ; really only 16
(defknown floating-point-modes () float-modes (flushable))
(defknown ((setf floating-point-modes)) (float-modes)
  float-modes)

(def!constant npx-env-size (* 7 n-word-bytes))
(def!constant npx-cw-offset 0)
(def!constant npx-sw-offset 4)

(define-vop (floating-point-modes)
  (:results (res :scs (unsigned-reg)))
  (:result-types unsigned-num)
  (:translate floating-point-modes)
  (:policy :fast-safe)
  (:generator 8
   (avm2 :push-uint #xdeadbeef)
   (avm2 :set-local res)))

(define-vop (set-floating-point-modes)
  (:args (new :scs (unsigned-reg) :to :result :target res))
  (:results (res :scs (unsigned-reg)))
  (:arg-types unsigned-num)
  (:result-types unsigned-num)
  (:translate (setf floating-point-modes))
  (:policy :fast-safe)
  (:generator 3
   (avm2 :get-local/nosign new)
   (avm2 :set-local res)))


(define-vop (flog)
  (:translate %log)
  (:args (x :scs (double-reg double-stack descriptor-reg)))
  (:results (y :scs (double-reg)))
  (:arg-types double-float)
  (:result-types double-float)
  (:policy :fast-safe)
  (:note "inline log function")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 5
    (%Math "log" x)
    (avm2 :set-local y)))

#!+we-do-not-really-need-these-x86-vops-do-we?
(progn
  (macrolet ((frob (func trans op)
               `(define-vop (,func)
                  (:args (x :scs (double-reg)))
                  (:results (y :scs (double-reg)))
                  (:arg-types double-float)
                  (:result-types double-float)
                  (:translate ,trans)
                  (:policy :fast-safe)
                  (:note "inline NPX function")
                  (:vop-var vop)
                  (:save-p :compute-only)
                  (:generator 5
                              (%Math ,op x)
                              (avm2 :set-local y)))))
    (frob fsin-quick %sin-quick "sin")
    (frob fcos-quick %cos-quick "cos")
    (frob fsqrt %sqrt "sqrt"))

  (define-vop (ftan-quick)
    (:translate %tan-quick)
    (:args (x :scs (double-reg)))
    (:results (y :scs (double-reg)))
    (:arg-types double-float)
    (:result-types double-float)
    (:policy :fast-safe)
    (:note "inline tan function")
    (:vop-var vop)
    (:save-p :compute-only)
    (:generator 5
                (%Math "tan" x)
                (avm2 :set-local y)))

  (macrolet ((frob (func trans op)
               `(define-vop (,func)
                  (:translate ,trans)
                  (:args (x :scs (double-reg)))
                  (:results (y :scs (double-reg)))
                  (:arg-types double-float)
                  (:result-types double-float)
                  (:policy :fast-safe)
                  (:note "inline sin/cos function")
                  (:vop-var vop)
                  (:save-p :compute-only)
                  (:generator 5
                              (%Math ,op x)
                              (avm2 :set-local y)))))
    (frob fsin  %sin "sin")
    (frob fcos  %cos "cos"))

  (define-vop (ftan)
    (:translate %tan)
    (:args (x :scs (double-reg)))
    (:results (y :scs (double-reg)))
    (:arg-types double-float)
    (:result-types double-float)
    (:policy :fast-safe)
    (:note "inline tan function")
    (:vop-var vop)
    (:save-p :compute-only)
    (:generator 5
                (%Math "tan" x)
                (avm2 :set-local y)))

  (define-vop (fexp)
    (:translate %exp)
    (:args (x :scs (double-reg)))
    (:results (y :scs (double-reg)))
    (:arg-types double-float)
    (:result-types double-float)
    (:policy :fast-safe)
    (:note "inline exp function")
    (:vop-var vop)
    (:save-p :compute-only)
    (:ignore temp)
    (:generator 5
                (%Math "exp" x)
                (avm2 :set-local y)))

  (define-vop (flog10)
    (:translate %log10)
    (:args (x :scs (double-reg double-stack descriptor-reg)))
    (:results (y :scs (double-reg)))
    (:arg-types double-float)
    (:result-types double-float)
    (:policy :fast-safe)
    (:note "inline log10 function")
    (:vop-var vop)
    (:save-p :compute-only)
    (:generator 5
                (%Math "log" x)
                (avm2 :push-double (log 10.0d0 (exp 1.0d0)))
                (avm2 :divide)
                (avm2 :set-local y)))

  (define-vop (fpow)
    (:translate %pow)
    (:args (x :scs (double-reg double-stack descriptor-reg))
           (y :scs (double-reg double-stack descriptor-reg)))
    (:results (r :scs (double-reg)))
    (:arg-types double-float double-float)
    (:result-types double-float)
    (:policy :fast-safe)
    (:note "inline pow function")
    (:vop-var vop)
    (:save-p :compute-only)
    (:generator 5
                (%Math "pow" x y)
                (avm2 :set-local r))))

(define-vop (flogb)
  (:translate %logb)
  (:args (x :scs (double-reg double-stack descriptor-reg)))
  (:results (y :scs (double-reg)))
  (:arg-types double-float)
  (:result-types double-float)
  (:policy :fast-safe)
  (:note "inline logb function")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 5
     (%not-implemented vop x y)))

(define-vop (fatan)
  (:translate %atan)
  (:args (x :scs (double-reg double-stack descriptor-reg)))
  (:results (r :scs (double-reg)))
  (:arg-types double-float)
  (:result-types double-float)
  (:policy :fast-safe)
  (:note "inline atan function")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 5
    (%Math "atan" x)
    (avm2 :set-local r)))

(define-vop (fatan2)
  (:translate %atan2)
  (:args (x :scs (double-reg double-stack descriptor-reg))
         (y :scs (double-reg double-stack descriptor-reg)))
  (:results (r :scs (double-reg)))
  (:arg-types double-float double-float)
  (:result-types double-float)
  (:policy :fast-safe)
  (:note "inline atan2 function")
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 5
    (%Math "atan2" x y)
    (avm2 :set-local r)))


;;;; complex float VOPs

(define-vop (make-complex-single-float)
  (:translate complex)
  (:args (real :scs (single-reg) :to :result :target r
               :load-if (not (location= real r)))
         (imag :scs (single-reg) :to :save))
  (:arg-types single-float single-float)
  (:results (r :scs (complex-single-reg) :from (:argument 0)
               :load-if (not (sc-is r complex-single-stack))))
  (:result-types complex-single-float)
  (:note "inline complex single-float creation")
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop real imag r)))

(define-vop (make-complex-double-float)
  (:translate complex)
  (:args (real :scs (double-reg) :target r
               :load-if (not (location= real r)))
         (imag :scs (double-reg) :to :save))
  (:arg-types double-float double-float)
  (:results (r :scs (complex-double-reg) :from (:argument 0)
               :load-if (not (sc-is r complex-double-stack))))
  (:result-types complex-double-float)
  (:note "inline complex double-float creation")
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 5
    (%not-implemented vop real imag r)))

(define-vop (complex-float-value)
  (:args (x :target r))
  (:results (r))
  (:variant-vars offset)
  (:policy :fast-safe)
  (:generator 3
    (avm2 :get-local/object
          (make-random-tn :kind :normal
                          :sc (sc-or-lose 'double-reg)
                          :offset (+ offset (tn-offset x))))
    (avm2 :set-local r)))

(define-vop (realpart/complex-single-float complex-float-value)
  (:translate realpart)
  (:args (x :scs (complex-single-reg complex-single-stack descriptor-reg)
            :target r))
  (:arg-types complex-single-float)
  (:results (r :scs (single-reg)))
  (:result-types single-float)
  (:note "complex float realpart")
  (:variant 0))

(define-vop (realpart/complex-double-float complex-float-value)
  (:translate realpart)
  (:args (x :scs (complex-double-reg complex-double-stack descriptor-reg)
            :target r))
  (:arg-types complex-double-float)
  (:results (r :scs (double-reg)))
  (:result-types double-float)
  (:note "complex float realpart")
  (:variant 0))

(define-vop (imagpart/complex-single-float complex-float-value)
  (:translate imagpart)
  (:args (x :scs (complex-single-reg complex-single-stack descriptor-reg)
            :target r))
  (:arg-types complex-single-float)
  (:results (r :scs (single-reg)))
  (:result-types single-float)
  (:note "complex float imagpart")
  (:variant 1))

(define-vop (imagpart/complex-double-float complex-float-value)
  (:translate imagpart)
  (:args (x :scs (complex-double-reg complex-double-stack descriptor-reg)
            :target r))
  (:arg-types complex-double-float)
  (:results (r :scs (double-reg)))
  (:result-types double-float)
  (:note "complex float imagpart")
  (:variant 1))


;;; hack dummy VOPs to bias the representation selection of their
;;; arguments towards a FP register, which can help avoid consing at
;;; inappropriate locations
(defknown double-float-reg-bias (double-float) (values))
(define-vop (double-float-reg-bias)
  (:translate double-float-reg-bias)
  (:args (x :scs (double-reg double-stack) :load-if nil))
  (:arg-types double-float)
  (:policy :fast-safe)
  (:note "inline dummy FP register bias")
  (:ignore x)
  (:generator 0))
(defknown single-float-reg-bias (single-float) (values))
(define-vop (single-float-reg-bias)
  (:translate single-float-reg-bias)
  (:args (x :scs (single-reg single-stack) :load-if nil))
  (:arg-types single-float)
  (:policy :fast-safe)
  (:note "inline dummy FP register bias")
  (:ignore x)
  (:generator 0))
