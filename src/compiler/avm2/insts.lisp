;;;; that part of the description of the x86 instruction set (for
;;;; 80386 and above) which can live on the cross-compilation host

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")
;;; FIXME: SB!DISASSEM: prefixes are used so widely in this file that
;;; I wonder whether the separation of the disassembler from the
;;; virtual machine is valid or adds value.

;;; Note: In CMU CL, this used to be a call to SET-DISASSEM-PARAMS.
(setf sb!disassem:*disassem-inst-alignment-bytes* 1)

(deftype reg () '(unsigned-byte 3))

(def!constant +default-operand-size+ :dword)

(defun offset-next (value dstate)
  (declare (type integer value)
           (type sb!disassem:disassem-state dstate))
  (+ (sb!disassem:dstate-next-addr dstate) value))

(defparameter *default-address-size*
  ;; Actually, :DWORD is the only one really supported.
  :dword)

(defparameter *byte-reg-names*
  #(al cl dl bl ah ch dh bh))
(defparameter *word-reg-names*
  #(ax cx dx bx sp bp si di))
(defparameter *dword-reg-names*
  #(eax ecx edx ebx esp ebp esi edi))

(declaim (special sb!c::*abc-buffer*))
(declaim (special sb!c::*abc-elsewhere-buffer*))

(defun tn-to-avm2 (tn)
  (sc-case tn
    ((immediate fp-constant)
     ;; immediate value.  Very annoying: Unlike normal ports, we can't treat
     ;; static symbols as integer-like immediate values, but NIL is used as
     ;; an immediate all over the place.  Let's be careful.
     (let ((val (tn-value tn)))
       (etypecase val
         (number val)
         (symbol (error "static symbol ~A used as instruction argument" val))
         (character (char-code val)))))
    (t
     (if (eq (sb-name (sc-sb (tn-sc tn))) 'registers)
         ;; this is a register.
         ;;
         ;; Now permute things so that the argument passing
         ;; registers EDX EDI ESI are mapped to local
         ;; variables 2 3 4 instead.  (local 1 is the xep hack, 0 is `this'.)
         ;;
         ;; Only "double word" registers should matter, the
         ;; rest need to go away at some point anyway.
         (ecase (tn-offset tn)
           ((4 5) 2)                    ;EDX
           ((14 15) 3)                  ;EDI
           ((12 13) 4)                  ;ESI
           ((0 1) 5)                    ;the rest:
           ((2 3) 6)
           ((6 7) 7)
           ((8 9) 8)
           ((10 11) 9))
         ;; this is a "stack location", which we map to a local variable, too.
         ;;
         ;; add 9 to skip the registers.
         (+ 9 (tn-offset tn))))))

;; debugging-helper
(defun pprint-avm2 (instructions)
  (fresh-line)
  (dolist (instruction instructions)
    (destructuring-bind (opcode &rest args) instruction
      (if (eq opcode :%LABEL)
          (format t "~5T~A~%" (car args))
          (format t "~10T~A~20T~{ ~A~}~%" opcode args)))))

(defun avm2 (opcode &rest args)
  (multiple-value-bind (opcode extra)
      (case opcode
        (:get-local/unsigned
         (values :get-local :convert-unsigned))
        (:get-local/signed
         (values :get-local :convert-integer))
        ((:get-local/nosign :get-local/object)
         (values :get-local nil))
        (:get-local
         (error ":get-local without sign hint"))
        (:push-immediate
         (destructuring-bind (tn) args
           (aver (sc-is tn immediate))
           (let ((value (tn-value tn)))
             (etypecase value
               ((or number character)
                (values :push-int nil))
               (null
                (%find-property "getNil")
                (%call-property "getNil" 0)
                (return-from avm2))
               (symbol
                (%find-property "getStaticSymbol")
                (avm2 :push-int (static-symbol-offset value))
                (%call-property "getStaticSymbol" 1)
                (return-from avm2))))))
        (t
         (values opcode nil)))
    (setf args
          (mapcar (lambda (arg)
                    (etypecase arg
                      ((or list symbol string number)
                       arg)
                      (character
                       (char-code arg))
                      (sb!assem::annotation
                       (sb!assem::annotation-avm2name arg))
                      (tn
                       (when (sc-is arg immediate)
                         (aver (not (eq opcode :get-local))))
                       (tn-to-avm2 arg))))
                  args))
;;     (format t "~&~30T AVM2: ~A ~A~%" opcode args)
    (case opcode
      (:push-int
       (check-type (car args) integer)))
    (when sb!c::*abc-buffer*
      (vector-push-extend (cons opcode args)
                          (if (eq sb!assem::**current-segment**
                                  sb!c:*elsewhere*)
                              sb!c::*abc-elsewhere-buffer*
                              sb!c::*abc-buffer*)))
    (when extra
      (avm2 extra))))
