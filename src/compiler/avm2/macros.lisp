;;;; a bunch of handy macros for the x86

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

(defmacro define-datavector-reffer (name type scs el-type &optional translate)
  `(define-vop (,name)
     ,@(when translate
             `((:translate ,translate)))
     (:policy :fast-safe)
     (:args (object :scs (descriptor-reg))
            (index :scs (any-reg immediate unsigned-reg)))
     (:arg-types ,type tagged-num)
     (:results (value :scs ,scs))
     (:result-types ,el-type)
     (:generator 3                      ; pw was 5
       (%find-property "getData")
       (avm2 :get-local/object object)
       (%call-property "getData" 1)
       (sc-case index
         (immediate
          (avm2 :push-int (tn-value index)))
         (unsigned-reg
          (avm2 :get-local/unsigned index))
         (t
          (avm2 :push-int index)))
       (avm2 :get-property (multiname-l "" ""))
       (avm2 :set-local value))))

(defmacro define-avm2slot-reffer
    (name type slotname scs el-type &optional translate)
  `(define-vop (,name)
     ,@(when translate
             `((:translate ,translate)))
     (:policy :fast-safe)
     (:args (object :scs (descriptor-reg)))
     (:arg-types ,type)
     (:results (value :scs ,scs))
     (:result-types ,el-type)
     (:generator 3                      ; pw was 5
;;       (%find-property ,slotname)
       (avm2 :get-local/object object)
       (%get-property ,slotname)
       (avm2 :set-local value))))

(defmacro define-avm2slot-setter
    (name type slotname scs el-type &optional translate)
  `(define-vop (,name)
     ,@(when translate
             `((:translate ,translate)))
     (:policy :fast-safe)
     (:args (object :scs (descriptor-reg))
            (value :scs ,scs))
     (:arg-types ,type ,el-type)
     (:results (result :scs ,scs))
     (:result-types ,el-type)
     (:generator 3                      ; pw was 5
;;       (%find-property ,slotname)
       (avm2 :get-local/object object)
       (avm2 :get-local/object value)
       (%set-property ,slotname)
       (avm2 :get-local/object value)
       (avm2 :set-local result))))

(defmacro define-datavector-reffer+offset (name type scs el-type &optional translate)
  `(define-vop (,name)
       ,@(when translate
           `((:translate ,translate)))
       (:policy :fast-safe)
       (:args (object :scs (descriptor-reg))
              (index :scs (any-reg immediate unsigned-reg)))
       (:arg-types ,type tagged-num (:constant t))
       (:info offset)
       (:results (value :scs ,scs))
       (:result-types ,el-type)
     (:generator 3                      ; pw was 5
       (%find-property "getData")
       (avm2 :get-local/object object)
       (%call-property "getData" 1)
       (sc-case index
         (immediate
          (avm2 :push-int (tn-value index)))
         (unsigned-reg
          (avm2 :get-local/unsigned index))
         (t
          (%unbox-fixnum index)))
       (avm2 :push-int offset)
       (avm2 :add-i)
       (avm2 :get-property (multiname-l "" ""))
       (avm2 :set-local value))))

(defmacro define-datavector-setter (name type scs el-type &optional translate)
  `(progn
     (define-vop (,name)
       ,@(when translate
           `((:translate ,translate)))
       (:policy :fast-safe)
       (:args (object :scs (descriptor-reg))
              (index :scs (any-reg immediate))
              (value :scs ,scs :target result))
       (:arg-types ,type tagged-num ,el-type)
       (:results (result :scs ,scs))
       (:result-types ,el-type)
       (:generator 4                    ; was 5
         (%find-property "getData")
         (avm2 :get-local/object object)
         (%call-property "getData" 1)
         (sc-case index
           (immediate
            (avm2 :push-int (tn-value index)))
           (unsigned-reg
            (avm2 :get-local/object index))
           (t
            (%unbox-fixnum index)))
         (avm2 :get-local/object value)
         (avm2 :set-property (multiname-l "" ""))
         (avm2 :get-local/object value)
         (avm2 :set-local result)))))

(defmacro define-datavector-setter+offset
    (name type scs el-type &optional translate)
  `(progn
     (define-vop (,name)
       ,@(when translate
           `((:translate ,translate)))
       (:policy :fast-safe)
       (:args (object :scs (descriptor-reg))
              (index :scs (any-reg immediate))
              (value :scs ,scs :target result))
       (:info offset)
       (:arg-types ,type tagged-num (:constant t) ,el-type)
       (:results (result :scs ,scs))
       (:result-types ,el-type)
       (:generator 4                    ; was 5
         (%find-property "getData")
         (avm2 :get-local/object object)
         (%call-property "getData" 1)
         (sc-case index
           (immediate
            (avm2 :push-int (tn-value index)))
           (unsigned-reg
            (avm2 :get-local/object index))
           (t
            (%unbox-fixnum index)))
         (avm2 :push-int offset)
         (avm2 :add-i)
         (avm2 :get-local/object value)
         (avm2 :set-property (multiname-l "" ""))
         (avm2 :get-local/object value)
         (avm2 :set-local result)))))

(defun slot-name-to-string (name)
  (etypecase name
    (string name)
    (symbol (format nil "_~(~A~)" (substitute #\_ #\- (string name))))))

(defun %find-property (local-name)
  (avm2 :find-property-strict (qname "avm2-compiler" local-name)))

(defun %get-property (local-name)
  (avm2 :get-property
        (qname "avm2-compiler" (slot-name-to-string local-name))))

(defun %set-property (local-name)
  (avm2 :set-property
        (qname "avm2-compiler" (slot-name-to-string local-name))))

(defun %call-property (local-name nargs)
  (avm2 :call-property
        (qname "avm2-compiler" (slot-name-to-string local-name))
        nargs))

(defun error-call (vop error-code &rest values)
  #!+sb-doc
  "Cause an error. ERROR-CODE is the error to cause."
  (avm2 :push-string (format nil "~A ~A ~A" vop error-code values))
  (avm2 :throw))

;;; helper for alien stuff.

(def!macro with-pinned-objects ((&rest objects) &body body)
  "Arrange with the garbage collector that the pages occupied by
OBJECTS will not be moved in memory for the duration of BODY.
Useful for e.g. foreign calls where another thread may trigger
collection."
  (if objects
      (let ((pins (make-gensym-list (length objects)))
            (wpo (block-gensym "WPO")))
        ;; BODY is stuffed in a function to preserve the lexical
        ;; environment.
        `(flet ((,wpo () (progn ,@body)))
           ;; PINS are dx-allocated in case the compiler for some
           ;; unfathomable reason decides to allocate value-cells
           ;; for them -- since we have DX value-cells on x86oid
           ;; platforms this still forces them on the stack.
           (dx-let ,(mapcar #'list pins objects)
             (multiple-value-prog1 (,wpo)
               ;; TOUCH-OBJECT has a VOP with an empty body: compiler
               ;; thinks we're using the argument and doesn't flush
               ;; the variable, but we don't have to pay any extra
               ;; beyond that -- and MULTIPLE-VALUE-PROG1 keeps them
               ;; live till the body has finished. *whew*
               ,@(mapcar (lambda (pin)
                           `(touch-object ,pin))
                         pins)))))
      `(progn ,@body)))
