;;;; the x86 definitions of some general purpose memory reference VOPs
;;;; inherited by basic memory reference operations

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;; CELL-REF and CELL-SET are used to define VOPs like CAR, where the
;;; property to be read or written is a property of the VOP used.
;;; CELL-SETF is similar to CELL-SET, but delivers the new value as
;;; the result. CELL-SETF-FUN takes its arguments as if it were a
;;; SETF function (new value first, as apposed to a SETF macro, which
;;; takes the new value last).
;;;
;;; Unlike ordinary backends, we use property names here, not offsets.
;;; Only for use with fixed-length objects, others need to go through
;;; the data slot indirection.
;;;
;;; (FIXME: can we switch back to offsets at some point, using optimized
;;; property accessors?  Might need type annotations.)
(define-vop (cell-ref)
  (:args (object :scs (descriptor-reg)))
  (:results (value :scs (descriptor-reg any-reg)))
  (:variant-vars name lowtag)
  (:ignore lowtag)
  (:policy :fast-safe)
  (:generator 4
    (avm2 :get-local/object object)
    (%get-property name)
    (avm2 :set-local value)))
(define-vop (cell-set)
  (:args (object :scs (descriptor-reg))
         (value :scs (descriptor-reg any-reg)))
  (:variant-vars name lowtag)
  (:ignore lowtag)
  (:policy :fast-safe)
  (:generator 4
    (avm2 :get-local/object object)
    (avm2 :get-local/object value)
    (%set-property name)))
(define-vop (cell-setf)
  (:args (object :scs (descriptor-reg))
         (value :scs (descriptor-reg any-reg) :target result))
  (:results (result :scs (descriptor-reg any-reg)))
  (:variant-vars name lowtag)
  (:ignore lowtag)
  (:policy :fast-safe)
  (:generator 4
    (avm2 :get-local/object object)
    (avm2 :get-local/object value)
    (%set-property name)
    (avm2 :get-local/object value)
    (avm2 :set-local result)))
(define-vop (cell-setf-fun)
  (:args (value :scs (descriptor-reg any-reg) :target result)
         (object :scs (descriptor-reg)))
  (:results (result :scs (descriptor-reg any-reg)))
  (:variant-vars name lowtag)
  (:ignore lowtag)
  (:policy :fast-safe)
  (:generator 4
    (avm2 :get-local/object object)
    (avm2 :get-local/object value)
    (%set-property name)
    (avm2 :get-local/object value)
    (avm2 :set-local result)))

;; is this being used?
;; ;;; Define accessor VOPs for some cells in an object. If the operation
;; ;;; name is NIL, then that operation isn't defined. If the translate
;; ;;; function is null, then we don't define a translation.
;; (defmacro define-cell-accessors (offset lowtag
;;                                         ref-op ref-trans set-op set-trans)
;;   `(progn
;;      ,@(when ref-op
;;          `((define-vop (,ref-op cell-ref)
;;              (:variant ,offset ,lowtag)
;;              ,@(when ref-trans
;;                  `((:translate ,ref-trans))))))
;;      ,@(when set-op
;;          `((define-vop (,set-op cell-setf)
;;              (:variant ,offset ,lowtag)
;;              ,@(when set-trans
;;                  `((:translate ,set-trans))))))))

;;; SLOT-REF and SLOT-SET are used to define VOPs like CLOSURE-REF,
;;; where the offset is constant at compile time, but varies for
;;; different uses.
(define-vop (slot-ref)
  (:args (object :scs (descriptor-reg)))
  (:results (value :scs (descriptor-reg any-reg)))
  (:variant-vars base lowtag)
  (:ignore base lowtag)
  (:info offset)
  (:generator 4
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :push-int offset)
    (avm2 :get-property (multiname-l "" ""))
    (avm2 :set-local value)))
(define-vop (slot-set)
  (:args (object :scs (descriptor-reg))
         (value :scs (descriptor-reg any-reg immediate)))
  (:variant-vars base lowtag)
  (:ignore base lowtag)
  (:info offset)
  (:generator 4
    (%find-property "getData")
    (avm2 :get-local/object object)
    (%call-property "getData" 1)
    (avm2 :push-int offset)
    (avm2 :get-local/object value)
    (avm2 :set-property (multiname-l "" ""))))

;; (define-vop (slot-set-conditional)
;;   (:args (object :scs (descriptor-reg) :to :eval)
;;          (old-value :scs (descriptor-reg any-reg) :target eax)
;;          (new-value :scs (descriptor-reg any-reg) :target temp))
;;   (:temporary (:sc descriptor-reg :offset eax-offset
;;                    :from (:argument 1) :to :result :target result)  eax)
;;   (:temporary (:sc descriptor-reg :from (:argument 2) :to :result) temp)
;;   (:variant-vars base lowtag)
;;   (:results (result :scs (descriptor-reg)))
;;   (:info offset)
;;   (:generator 4
;;     (move eax old-value)
;;     (move temp new-value)
;;     (x86inst cmpxchg (make-ea-for-object-slot object (+ base offset) lowtag)
;;           temp)
;;     (move result eax)))
