;;;; the x86 VM definition of operand loading/saving and the MOVE vop

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

(define-move-fun (load-immediate 1) (vop x y)
  ((immediate)
   (any-reg descriptor-reg fp/bp))
  (unless (sc-is y fp/bp)
    (typecase (tn-value x)
      (null
       (%find-property "getNil")
       (%call-property "getNil" 0)
       (avm2 :set-local y))
      (symbol
       (%find-property "getStaticSymbol")
       (avm2 :push-int (static-symbol-offset (tn-value x)))
       (%call-property "getStaticSymbol" 1)
       (avm2 :set-local y))
      (integer
       (avm2 :push-int x)
       (avm2 :set-local y)))))

(define-move-fun (load-number 1) (vop x y)
  ((immediate) (signed-reg unsigned-reg))
  (let ((val (tn-value x)))
    (avm2 :push-int val)
    (avm2 :set-local y)))

(define-move-fun (load-character 1) (vop x y)
  ((immediate) (character-reg))
  (avm2 :push-int (char-code (tn-value x)))
  (avm2 :set-local y))

(define-move-fun (load-system-area-pointer 1) (vop x y)
  ((immediate) (sap-reg))
  (avm2 :get-local/object x)
  (avm2 :pop)
  (avm2 :get-local/object y)
  (avm2 :pop)
  (avm2 :comment "hoppla, SAP-Nutzung"))

(define-move-fun (load-constant 5) (vop x y)
  ((constant) (descriptor-reg any-reg))
  (avm2 :get-local/object x)
  (avm2 :set-local y))

(define-move-fun (load-stack 5) (vop x y)
  ((control-stack) (any-reg descriptor-reg)
   (character-stack) (character-reg)
   (sap-stack) (sap-reg)
   (signed-stack) (signed-reg)
   (unsigned-stack) (unsigned-reg)
   (fp/bp-stack) (fp/bp))
  (unless (sc-is x fp/bp-stack)
    (avm2 :get-local/object x)
    (avm2 :set-local y)))

(define-move-fun (store-stack 5) (vop x y)
  ((any-reg descriptor-reg) (control-stack)
   (character-reg) (character-stack)
   (sap-reg) (sap-stack)
   (signed-reg) (signed-stack)
   (unsigned-reg) (unsigned-stack)
   (fp/bp) (fp/bp-stack))
  (unless (sc-is x fp/bp)
    (avm2 :get-local/object x)
    (avm2 :set-local y)))

;;;; the MOVE VOP
(define-vop (move)
  (:args (x :scs (any-reg descriptor-reg immediate fp/bp) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (any-reg descriptor-reg fp/bp)
               :load-if
               (not (or (location= x y)
                        (sc-is x fp/bp)
                        (sc-is y fp/bp)
                        (and (sc-is x any-reg descriptor-reg immediate)
                             (sc-is y control-stack))))))
  (:effects)
  (:affected)
  (:generator 0
    (cond
      ((or (sc-is x fp/bp) (sc-is y fp/bp)))
      ((and (sc-is x immediate)
            (sc-is y any-reg descriptor-reg control-stack))
       (avm2 :push-immediate x)
       (avm2 :set-local y))
      (t
       (avm2 :get-local/object x)
       (avm2 :set-local y)))))

(define-move-vop move :move
  (any-reg descriptor-reg immediate fp/bp)
  (any-reg descriptor-reg fp/bp))

;;; Make MOVE the check VOP for T so that type check generation
;;; doesn't think it is a hairy type. This also allows checking of a
;;; few of the values in a continuation to fall out.
(primitive-type-vop move (:check) t)

;;; The MOVE-ARG VOP is used for moving descriptor values into
;;; another frame for argument or known value passing.
;;;
;;; Note: It is not going to be possible to move a constant directly
;;; to another frame, except if the destination is a register and in
;;; this case the loading works out.
(define-vop (move-arg)
  (:args (x :scs (any-reg descriptor-reg immediate) :target y
            :load-if (not (and (sc-is y any-reg descriptor-reg)
                               (sc-is x control-stack))))
         (fp :scs (any-reg fp/bp)
             :load-if (not (sc-is y any-reg descriptor-reg))))
  (:results (y))
  (:generator 0
    (sc-case y
      (fp/bp)
      ((any-reg descriptor-reg)
       (sc-case x
         (fp/bp)
         (immediate
          (avm2 :push-immediate x)
          (avm2 :push-string (format nil "move arg/imm ~A <- ~A" y x))
          (avm2 :throw))
         (t
          (avm2 :push-string (format nil "move arg ~A <- ~A" y x))
          (avm2 :throw))))
      ((control-stack)
       (let ((frame-offset (if (= (tn-offset fp) esp-offset)
                               ;; C-call
                               (tn-offset y)
                               ;; Lisp stack
                               (frame-word-offset (tn-offset y)))))
         (avm2 :push-string (format nil "move arg ~A, ~A, ~A"
                                    x
                                    fp
                                    frame-offset))
         (avm2 :throw))))))

(define-move-vop move-arg :move-arg
  (any-reg descriptor-reg fp/bp)
  (any-reg descriptor-reg fp/bp))

;;;; ILLEGAL-MOVE

;;; This VOP exists just to begin the lifetime of a TN that couldn't
;;; be written legally due to a type error. An error is signalled
;;; before this VOP is so we don't need to do anything (not that there
;;; would be anything sensible to do anyway.)
(define-vop (illegal-move)
  (:args (x) (type))
  (:results (y))
  (:ignore y)
  (:vop-var vop)
  (:save-p :compute-only)
  (:generator 666
    (error-call vop 'object-not-type-error x type)
    (avm2 :push-string "object-not-type-error")
    (avm2 :throw)))

;;;; moves and coercions

;;; These MOVE-TO-WORD VOPs move a tagged integer to a raw full-word
;;; representation. Similarly, the MOVE-FROM-WORD VOPs converts a raw
;;; integer to a tagged bignum or fixnum.

;;; Arg is a fixnum, so just shift it. We need a type restriction
;;; because some possible arg SCs (control-stack) overlap with
;;; possible bignum arg SCs.
(define-vop (move-to-word/fixnum)
  (:args (x :scs (any-reg descriptor-reg) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (signed-reg unsigned-reg)
               :load-if (not (location= x y))))
  (:arg-types tagged-num)
  (:note "fixnum untagging")
  (:generator 1
    (%unbox-fixnum x)
    (avm2 :set-local y)))
(define-move-vop move-to-word/fixnum :move
  (any-reg descriptor-reg) (signed-reg unsigned-reg))

;;; Arg is a non-immediate constant, load it.
(define-vop (move-to-word-c)
  (:args (x :scs (constant)))
  (:results (y :scs (signed-reg unsigned-reg)))
  (:note "constant load")
  (:generator 1
    (avm2 :push-int (tn-value x))
    (avm2 :set-local y)))
(define-move-vop move-to-word-c :move
  (constant) (signed-reg unsigned-reg))


;;; Arg is a fixnum or bignum, figure out which and load if necessary.
(define-vop (move-to-word/integer)
  (:args (x :scs (descriptor-reg)))
  (:results (y :scs (signed-reg unsigned-reg)))
  (:note "integer to untagged word coercion")
  (:generator 4
    (%find-property "moveToWord")
    (avm2 :get-local/object x)
    (%call-property "moveToWord" 1)
    (avm2 :set-local y)))
(define-move-vop move-to-word/integer :move
  (descriptor-reg) (signed-reg unsigned-reg))


;;; Result is a fixnum, so we can just shift. We need the result type
;;; restriction because of the control-stack ambiguity noted above.
(define-vop (move-from-word/fixnum)
  (:args (x :scs (signed-reg unsigned-reg) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (any-reg descriptor-reg)
               :load-if (not (location= x y))))
  (:result-types tagged-num)
  (:note "fixnum tagging")
  (:generator 1
    (%box-fixnum x)
    (avm2 :set-local y)))
(define-move-vop move-from-word/fixnum :move
  (signed-reg unsigned-reg) (any-reg descriptor-reg))

;;; Convert an untagged signed word to a lispobj -- fixnum or bignum
;;; as the case may be. Fixnum case inline, bignum case in an assembly
;;; routine.
(define-vop (move-from-signed)
  (:args (x :scs (signed-reg unsigned-reg) :to :result))
  (:results (y :scs (any-reg descriptor-reg) :from :argument))
  (:note "signed word to integer coercion")
  ;; Worst case cost to make sure people know they may be number consing.
  (:generator 20
     (aver (not (location= x y)))
     (%find-property "moveFromSigned")
     (avm2 :get-local/signed x)
     (%call-property "moveFromSigned" 1)
     (avm2 :set-local y)))
(define-move-vop move-from-signed :move
  (signed-reg) (descriptor-reg))

;;; Convert an untagged unsigned word to a lispobj -- fixnum or bignum
;;; as the case may be. Fixnum case inline, bignum case in an assembly
;;; routine.
(define-vop (move-from-unsigned)
  (:args (x :scs (signed-reg unsigned-reg) :to :result))
  (:results (y :scs (any-reg descriptor-reg) :from :argument))
  (:note "unsigned word to integer coercion")
  ;; Worst case cost to make sure people know they may be number consing.
  (:generator 20
    (aver (not (location= x y)))
    (%find-property "moveFromUnsigned")
    (avm2 :get-local/object x)
    (%call-property "moveFromUnsigned" 1)
    (avm2 :set-local y)))
(define-move-vop move-from-unsigned :move
  (unsigned-reg) (descriptor-reg))

;;; Move untagged numbers.
(define-vop (word-move)
  (:args (x :scs (signed-reg unsigned-reg) :target y
            :load-if (not (location= x y))))
  (:results (y :scs (signed-reg unsigned-reg)
               :load-if
               (not (or (location= x y)
                        (and (sc-is x signed-reg unsigned-reg)
                             (sc-is y signed-stack unsigned-stack))))))
  (:effects)
  (:affected)
  (:note "word integer move")
  (:generator 0
    (avm2 :get-local/object x)
    (avm2 :set-local y)))
(define-move-vop word-move :move
  (signed-reg unsigned-reg) (signed-reg unsigned-reg))

;;; Move untagged number arguments/return-values.
(define-vop (move-word-arg)
  (:args (x :scs (signed-reg unsigned-reg) :target y)
         (fp :scs (any-reg) :load-if (not (sc-is y sap-reg))))
  (:results (y))
  (:note "word integer argument move")
  (:generator 0
    (sc-case y
      ((signed-reg unsigned-reg)
       (avm2 :comment (format nil "move word arg (reg) ~A <- ~A" y x))
       )
      ((signed-stack unsigned-stack)
       (avm2 :comment (format nil "move word arg (stack) ~A ~A ~A"
                              x fp y))
       ;; (storew x fp (frame-word-offset (tn-offset y)))
       ))))
(define-move-vop move-word-arg :move-arg
  (descriptor-reg any-reg signed-reg unsigned-reg) (signed-reg unsigned-reg))

;;; Use standard MOVE-ARG and coercion to move an untagged number
;;; to a descriptor passing location.
(define-move-vop move-arg :move-arg
  (signed-reg unsigned-reg) (any-reg descriptor-reg))
