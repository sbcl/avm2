;;;; the definition of non-local exit for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;; Make an environment-live stack TN for saving the SP for NLX entry.
(!def-vm-support-routine make-nlx-sp-tn (env)
  (physenv-live-tn
   (make-representation-tn *fixnum-primitive-type* any-reg-sc-number)
   env))

;;; Make a TN for the argument count passing location for a non-local entry.
(!def-vm-support-routine make-nlx-entry-arg-start-location ()
  (make-wired-tn *fixnum-primitive-type* any-reg-sc-number ebx-offset))


;;;; Save and restore dynamic environment.
;;;;
;;;; These VOPs are used in the reentered function to restore the
;;;; appropriate dynamic environment. Currently we only save the
;;;; Current-Catch and the alien stack pointer. (Before sbcl-0.7.0,
;;;; when there were IR1 and byte interpreters, we had to save
;;;; the interpreter "eval stack" too.)
;;;;
;;;; We don't need to save/restore the current UNWIND-PROTECT, since
;;;; UNWIND-PROTECTs are implicitly processed during unwinding.
;;;;
;;;; We don't need to save the BSP, because that is handled automatically.

(define-vop (save-dynamic-state)
  (:results (catch :scs (descriptor-reg))
            (alien-stack :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 13
    (%not-implemented vop catch alien-stack)))

(define-vop (restore-dynamic-state)
  (:args (catch :scs (descriptor-reg))
         (alien-stack :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 10
    (%not-implemented vop catch alien-stack)))

(define-vop (current-stack-pointer)
  (:results (res :scs (any-reg control-stack)))
  (:ignore res)
  (:generator 1
    nil))

(define-vop (current-binding-pointer)
  (:results (res :scs (any-reg descriptor-reg)))
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop res)))

;;;; unwind block hackery

;;; Compute the address of the catch block from its TN, then store into the
;;; block the current Fp, Env, Unwind-Protect, and the entry PC.
(define-vop (make-unwind-block)
  (:args (tn))
  (:info entry-label)
  (:results (block :scs (any-reg)))
  (:vop-var vop)
  (:generator 22
    (%not-implemented vop tn entry-label block)))

;;; like MAKE-UNWIND-BLOCK, except that we also store in the specified
;;; tag, and link the block into the CURRENT-CATCH list
(define-vop (make-catch-block)
  (:args (tn)
         (tag :scs (any-reg descriptor-reg) :to (:result 1)))
  (:info entry-label)
  (:results (block :scs (any-reg)))
  (:vop-var vop)
  (:generator 44
    (%not-implemented vop tn tag entry-label block)))

;;; Just set the current unwind-protect to TN's address. This instantiates an
;;; unwind block as an unwind-protect.
(define-vop (set-unwind-protect)
  (:args (tn))
  (:vop-var vop)
  (:generator 7
    (%not-implemented vop tn)))

(define-vop (unlink-catch-block)
  (:policy :fast-safe)
  (:translate %catch-breakup)
  (:vop-var vop)
  (:generator 17
    (%not-implemented vop)))

(define-vop (unlink-unwind-protect)
  (:policy :fast-safe)
  (:translate %unwind-protect-breakup)
  (:vop-var vop)
  (:generator 17
    (%not-implemented vop)))

;;;; NLX entry VOPs
(define-vop (nlx-entry)
  ;; Note: we can't list an sc-restriction, 'cause any load vops would
  ;; be inserted before the return-pc label.
  (:args (sp)
         (start)
         (count))
  (:results (values :more t))
  (:info label nvals)
  (:save-p :force-to-stack)
  (:vop-var vop)
  (:generator 30
    (emit-label label)
    (note-this-location vop :non-local-entry)
    (%not-implemented vop sp start count nvals values)))

(define-vop (nlx-entry-multiple)
  (:args (top)
         (source)
         (count))
  ;; Again, no SC restrictions for the args, 'cause the loading would
  ;; happen before the entry label.
  (:info label)
  (:results (result :scs (any-reg) :from (:argument 0))
            (num :scs (any-reg control-stack)))
  (:save-p :force-to-stack)
  (:vop-var vop)
  (:generator 30
    (emit-label label)
    (note-this-location vop :non-local-entry)
    (%not-implemented vop top source count result num)))


;;; This VOP is just to force the TNs used in the cleanup onto the stack.
(define-vop (uwp-entry)
  (:info label)
  (:save-p :force-to-stack)
  (:results (block) (start) (count))
  (:ignore block start count)
  (:vop-var vop)
  (:generator 0
    (emit-label label)
    (note-this-location vop :non-local-entry)))

(define-vop (unwind-to-frame-and-call)
    (:args (ofp :scs (descriptor-reg))
           (uwp :scs (descriptor-reg))
           (function :scs (descriptor-reg)))
  (:arg-types system-area-pointer system-area-pointer t)
  (:vop-var vop)
  (:generator 22
    (%not-implemented vop ofp uwp function)))

(define-vop (throw)
  (:args (target :scs (descriptor-reg any-reg))
         (start :scs (any-reg))
         (count :scs (any-reg)))
  (:vop-var vop)
  (:generator 22
    (%not-implemented vop target start count)))

(define-vop (unwind)
  (:args (block :scs (descriptor-reg any-reg))
         (start :scs (any-reg))
         (count :scs (any-reg)))
  (:vop-var vop)
  (:generator 22
    (%not-implemented vop block start count)))
