;;;; predicate VOPs for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; the branch VOP

;;; The unconditional branch, emitted when we can't drop through to the desired
;;; destination. Dest is the continuation we transfer control to.
(define-vop (branch)
  (:info dest)
  (:generator 5
    (avm2 :jump (sb!assem::annotation-avm2name dest))))


;;;; conditional VOPs

;;; Note: a constant-tn is allowed in CMP; it uses an EA displacement,
;;; not immediate data.
(define-vop (if-eq)
  (:args (x :scs (any-reg descriptor-reg control-stack constant))
         (y :scs (any-reg descriptor-reg #+(or) immediate)))
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe)
  (:translate eq)
  (:generator 3
    (let ()
      (cond
        ;; An encoded value (literal integer) has to be the second argument.
        ((sc-is x immediate)
         (avm2 :push-int x)
         (avm2 :push-int y)
         (avm2 (if not-p :if-ne :if-eq) (sb!assem::annotation-avm2name target)))
;;         ((sc-is y immediate)
;;       (aver (null (tn-value y)))
;;       (%find-property "eq")
;;       (avm2 :get-local/object x)
;;       (%find-property "getNil")
;;       (%call-property "getNil" 0)
;;       (avm2 (if not-p :if-ne :if-eq)
;;             (sb!assem::annotation-avm2name target)))
        (t
         (%find-property "eq")
         (avm2 :get-local/object x)
         (avm2 :get-local/object y)
         (%call-property "eq" 2)
         (avm2 (if not-p :if-false :if-true)
               (sb!assem::annotation-avm2name target)))))))
