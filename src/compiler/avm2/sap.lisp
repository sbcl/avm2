;;;; SAP operations for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")


;;; (all of this is pointless, since AVM2 doesn't have FFI)

(defun %not-implemented/sap (x)
  (avm2 :push-string (format nil "SAPs aren't implemented on AVM2 (~A)" x))
  (avm2 :throw))

;;; Move a tagged SAP to an untagged representation.
(define-vop (move-to-sap)
  (:args (x :scs (descriptor-reg)))
  (:results (y :scs (sap-reg)))
  (:note "pointer to SAP coercion")
  (:vop-var vop)
  (:ignore x y)
  (:generator 1
    (%not-implemented/sap vop)))
(define-move-vop move-to-sap :move
  (descriptor-reg) (sap-reg))

;;; Move an untagged SAP to a tagged representation.
(define-vop (move-from-sap)
  (:args (sap :scs (sap-reg) :to :result))
  (:results (res :scs (descriptor-reg) :from :argument))
  (:note "SAP to pointer coercion")
  (:vop-var vop)
  (:ignore sap res)
  (:generator 20
    (%not-implemented/sap vop)))
(define-move-vop move-from-sap :move
  (sap-reg) (descriptor-reg))

;;; Move untagged sap values.
(define-vop (sap-move)
  (:args (x :scs (sap-reg)))
  (:results (y :scs (sap-reg)))
  (:note "SAP move")
  (:ignore x y)
  (:effects)
  (:affected)
  (:vop-var vop)
  (:generator 0
    (%not-implemented/sap vop)))
(define-move-vop sap-move :move
  (sap-reg) (sap-reg))

;;; Move untagged sap arguments/return-values.
(define-vop (move-sap-arg)
  (:args (x :scs (sap-reg))
         (fp :scs (any-reg)))
  (:results (y))
  (:ignore x fp y)
  (:note "SAP argument move")
  (:vop-var vop)
  (:generator 0
    (%not-implemented/sap vop)))
(define-move-vop move-sap-arg :move-arg
  (descriptor-reg sap-reg) (sap-reg))

;;; Use standard MOVE-ARG + coercion to move an untagged sap to a
;;; descriptor passing location.
(define-move-vop move-arg :move-arg
  (sap-reg) (descriptor-reg))

;;;; SAP-INT and INT-SAP

;;; The function SAP-INT is used to generate an integer corresponding
;;; to the system area pointer, suitable for passing to the kernel
;;; interfaces (which want all addresses specified as integers). The
;;; function INT-SAP is used to do the opposite conversion. The
;;; integer representation of a SAP is the byte offset of the SAP from
;;; the start of the address space.
(define-vop (sap-int)
  (:args (sap :scs (sap-reg) :target int))
  (:ignore sap int)
  (:arg-types system-area-pointer)
  (:results (int :scs (unsigned-reg)))
  (:result-types unsigned-num)
  (:translate sap-int)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented/sap vop)))
(define-vop (int-sap)
  (:args (int :scs (unsigned-reg) :target sap))
  (:arg-types unsigned-num)
  (:results (sap :scs (sap-reg)))
  (:ignore int sap)
  (:result-types system-area-pointer)
  (:translate int-sap)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented/sap vop)))

;;;; POINTER+ and POINTER-

(define-vop (pointer+)
  (:translate sap+)
  (:args (ptr :scs (sap-reg))
         (offset :scs (signed-reg immediate)))
  (:arg-types system-area-pointer signed-num)
  (:results (res :scs (sap-reg) :from (:argument 0)))
  (:ignore ptr offset res)
  (:result-types system-area-pointer)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented/sap vop)))

(define-vop (pointer-)
  (:translate sap-)
  (:args (ptr1 :scs (sap-reg))
         (ptr2 :scs (sap-reg)))
  (:ignore ptr1 ptr2 res)
  (:arg-types system-area-pointer system-area-pointer)
  (:policy :fast-safe)
  (:results (res :scs (signed-reg) :from (:argument 0)))
  (:result-types signed-num)
  (:vop-var vop)
  (:generator 1
    (%not-implemented/sap vop)))

;;;; mumble-SYSTEM-REF and mumble-SYSTEM-SET

;;;; SAP-REF-DOUBLE

#+(or)
(define-vop (sap-ref-double-with-offset)
  (:translate sb!c::sap-ref-double-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg))
         (offset :scs (signed-reg immediate)))
  (:info disp)
  (:arg-types system-area-pointer signed-num
              (:constant (constant-displacement 0 ; lowtag
                                                8 ; double-float size
                                                0)))
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:vop-var vop)
  (:generator 5
    (%not-implemented/sap vop)))

#+(or)
(define-vop (%set-sap-ref-double-with-offset)
  (:translate sb!c::%set-sap-ref-double-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to (:eval 0))
         (offset :scs (signed-reg) :to (:eval 0))
         (value :scs (double-reg)))
  (:info disp)
  (:arg-types system-area-pointer signed-num
              (:constant (constant-displacement 0 ; lowtag
                                                8 ; double-float size
                                                0))
              double-float)
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:vop-var vop)
  (:generator 5
    (%not-implemented/sap vop)))

#+(or)
(define-vop (%set-sap-ref-double-with-offset-c)
  (:translate sb!c::%set-sap-ref-double-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to (:eval 0))
         (value :scs (double-reg)))
  (:arg-types system-area-pointer (:constant (signed-byte 32))
              (:constant (constant-displacement 0 ; lowtag
                                                8 ; double-float size
                                                0))
              double-float)
  (:info offset disp)
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:vop-var vop)
  (:generator 4
    (%not-implemented/sap vop)))

;;;; SAP-REF-SINGLE

#+(or)
(define-vop (sap-ref-single-with-offset)
  (:translate sb!c::sap-ref-single-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg))
         (offset :scs (signed-reg immediate)))
  (:info disp)
  (:arg-types system-area-pointer signed-num
              (:constant (constant-displacement 0 ; lowtag
                                                4 ; single-float size
                                                0)))
  (:results (result :scs (single-reg)))
  (:result-types single-float)
  (:vop-var vop)
  (:generator 5
    (%not-implemented/sap vop)))

#+(or)
(define-vop (%set-sap-ref-single-with-offset)
  (:translate sb!c::%set-sap-ref-single-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to (:eval 0))
         (offset :scs (signed-reg) :to (:eval 0))
         (value :scs (single-reg)))
  (:info disp)
  (:arg-types system-area-pointer signed-num
              (:constant (constant-displacement 0 ; lowtag
                                                4 ; single-float size
                                                0))
              single-float)
  (:results (result :scs (single-reg)))
  (:result-types single-float)
  (:vop-var vop)
  (:generator 5
    (%not-implemented/sap vop)))

#+(or)
(define-vop (%set-sap-ref-single-with-offset-c)
  (:translate sb!c::%set-sap-ref-single-with-offset)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg) :to (:eval 0))
         (value :scs (single-reg)))
  (:arg-types system-area-pointer (:constant (signed-byte 32))
              (:constant (constant-displacement 0 ; lowtag
                                                4 ; single-float size
                                                0))
              single-float)
  (:info offset disp)
  (:results (result :scs (single-reg)))
  (:result-types single-float)
  (:vop-var vop)
  (:generator 4
    (%not-implemented/sap vop)))

;;;; SAP-REF-LONG

(define-vop (sap-ref-long)
  (:translate sap-ref-long)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg))
         (offset :scs (signed-reg)))
  (:ignore sap offset result)
  (:arg-types system-area-pointer signed-num)
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:vop-var vop)
  (:generator 5
    (%not-implemented/sap vop)))

(define-vop (sap-ref-long-c)
  (:translate sap-ref-long)
  (:policy :fast-safe)
  (:args (sap :scs (sap-reg)))
  (:arg-types system-area-pointer (:constant (signed-byte 32)))
  (:info offset)
  (:results (result :scs (double-reg)))
  (:result-types double-float)
  (:vop-var vop)
  (:ignore sap offset result)
  (:generator 4
    (%not-implemented/sap vop)))

;;; noise to convert normal lisp data objects into SAPs

(define-vop (vector-sap)
  (:translate vector-sap)
  (:policy :fast-safe)
  (:args (vector :scs (descriptor-reg) :target sap))
  (:results (sap :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:vop-var vop)
  (:ignore vector sap)
  (:generator 2
    (%not-implemented/sap vop)))

;;; Transforms for 64-bit SAP accessors.

(deftransform sap-ref-64 ((sap offset) (* *))
  '(logior (sap-ref-32 sap offset)
           (ash (sap-ref-32 sap (+ offset 4)) 32)))

(deftransform signed-sap-ref-64 ((sap offset) (* *))
  '(logior (sap-ref-32 sap offset)
           (ash (signed-sap-ref-32 sap (+ offset 4)) 32)))

(deftransform %set-sap-ref-64 ((sap offset value) (* * *))
  '(progn
     (%set-sap-ref-32 sap offset (logand value #xffffffff))
     (%set-sap-ref-32 sap (+ offset 4) (ash value -32))))

(deftransform %set-signed-sap-ref-64 ((sap offset value) (* * *))
  '(progn
     (%set-sap-ref-32 sap offset (logand value #xffffffff))
     (%set-signed-sap-ref-32 sap (+ offset 4) (ash value -32))))
