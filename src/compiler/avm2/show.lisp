;;;; VOPs which are useful for following the progress of the system
;;;; early in boot

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;; FIXME: should probably become conditional on #!+SB-SHOW
;;; FIXME: should be called DEBUG-PRINT or COLD-PRINT
(define-vop (print)
  (:args (object :scs (descriptor-reg any-reg)))
  (:results (result :scs (descriptor-reg)))
  (:save-p t)
  (:generator 100
    (avm2 :find-property-strict "trace")
    (avm2 :get-local/object object)
    (avm2 :coerce-any)
    (avm2 :call-property "trace" 1)

    (avm2 :get-local/object object)
    (avm2 :set-local result)))
