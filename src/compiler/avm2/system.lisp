;;;; x86 VM definitions of various system hacking operations

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; type frobbing VOPs

(define-vop (count-me)
  (:args (count-vector :scs (descriptor-reg)))
  (:info index)
  (:vop-var vop)
  (:generator 0
    (%not-implemented vop count-vector index)))

(define-vop (widetag-of)
  (:translate widetag-of)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg)))
  (:results (result :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 6
    (avm2 :get-local/object object)
    (%get-property "widetag")
    (avm2 :set-local result)))

(define-vop (fun-subtype)
  (:translate fun-subtype)
  (:policy :fast-safe)
  (:args (function :scs (descriptor-reg)))
  (:results (result :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 6
    ;; probably incorrect
    (avm2 :get-local/object function)
    (%get-property "widetag")
    (avm2 :set-local result)))

(define-vop (set-fun-subtype)
  (:translate (setf fun-subtype))
  (:policy :fast-safe)
  (:args (type :scs (unsigned-reg))
         (function :scs (descriptor-reg)))
  (:arg-types positive-fixnum *)
  (:results (result :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 6
    ;; probably incorrect
    (avm2 :get-local/object function)
    (avm2 :get-local/unsigned type)
    (%set-property "widetag")

    (avm2 :get-local/unsigned type)
    (avm2 :set-local result)))

(define-vop (get-header-data)
  (:translate get-header-data)
  (:policy :fast-safe)
  (:args (x :scs (descriptor-reg)))
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:vop-var vop)
  (:generator 6
    (%not-implemented vop x res)))

(define-vop (instance-length)
  (:policy :fast-safe)
  (:translate %instance-length)
  (:args (struct :scs (descriptor-reg)))
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 4
    ;; fixme: getData and fetch length
    (%find-property "getInstanceLength")
    (avm2 :get-local/object struct)
    (%call-property "getInstanceLength" 1)
    (avm2 :set-local res)))

(define-vop (get-closure-length
             instance-length)
  (:translate get-closure-length)
  (:policy :fast-safe)
  (:args (x :scs (descriptor-reg)))
  (:results (res :scs (unsigned-reg)))
  (:result-types positive-fixnum)
  (:generator 6
    ;; fixme: getData and fetch length
    (%find-property "getClosureLength")
    (avm2 :get-local/object x)
    (%call-property "getClosureLength" 1)
    (avm2 :set-local res)))

(define-vop (set-header-data)
  (:translate set-header-data)
  (:policy :fast-safe)
  (:args (x :scs (descriptor-reg) :target res :to (:result 0))
         (data :scs (any-reg)))
  (:arg-types * positive-fixnum)
  (:results (res :scs (descriptor-reg)))
  (:vop-var vop)
  (:generator 6
    (%not-implemented vop x data res)))

(define-vop (pointer-hash)
  (:translate pointer-hash)
  (:args (ptr :scs (any-reg descriptor-reg) :target res))
  (:results (res :scs (any-reg descriptor-reg)))
  (:policy :fast-safe)
  (:generator 1
    (avm2 :get-local/object ptr)
    (%get-property "hashCode")
    (avm2 :set-local res)))

(define-vop (make-other-immediate-type)
  (:args (val :scs (any-reg descriptor-reg) :target res)
         (type :scs (unsigned-reg immediate)))
  (:results (res :scs (any-reg descriptor-reg) :from (:argument 0)))
  (:vop-var vop)
  (:generator 2
    (%not-implemented vop val type res)))

;;;; allocation

(define-vop (dynamic-space-free-pointer)
  (:results (int :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:translate dynamic-space-free-pointer)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop int)))

(define-vop (binding-stack-pointer-sap)
  (:results (int :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:translate binding-stack-pointer-sap)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop int)))

(defknown (setf binding-stack-pointer-sap)
    (system-area-pointer) system-area-pointer ())

(define-vop (set-binding-stack-pointer-sap)
  (:args (new-value :scs (sap-reg) :target int))
  (:arg-types system-area-pointer)
  (:results (int :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:translate (setf binding-stack-pointer-sap))
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop new-value int)))

(define-vop (control-stack-pointer-sap)
  (:results (int :scs (sap-reg)))
  (:result-types system-area-pointer)
  (:translate control-stack-pointer-sap)
  (:policy :fast-safe)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop int)))

;;;; code object frobbing

(define-vop (code-instructions)
  (:translate code-instructions)
  (:policy :fast-safe)
  (:args (code :scs (descriptor-reg) :to (:result 0)))
  (:results (sap :scs (sap-reg) :from (:argument 0)))
  (:result-types system-area-pointer)
  (:vop-var vop)
  (:generator 10
    (%not-implemented vop code sap)))

(define-vop (compute-fun)
  (:args (code :scs (descriptor-reg) :to (:result 0))
         (offset :scs (signed-reg unsigned-reg) :to (:result 0)))
  (:arg-types * positive-fixnum)
  (:results (func :scs (descriptor-reg) :from (:argument 0)))
  (:vop-var vop)
  (:generator 10
    (%not-implemented vop code offset func)))

;; this is an x86 invention, right?
;; (define-vop (%simple-fun-self)
;;   (:policy :fast-safe)
;;   (:translate %simple-fun-self)
;;   (:args (function :scs (descriptor-reg)))
;;   (:results (result :scs (descriptor-reg)))
;;   (:generator 3
;;     (%not-implemented vop)))

;;; The closure function slot is a pointer to raw code on X86 instead
;;; of a pointer to the code function object itself. This VOP is used
;;; to reference the function object given the closure object.

;; fixme?
;; (define-source-transform %closure-fun (closure)
;;   `(%simple-fun-self ,closure))

;; this is an x86 invention, right?
;; (define-vop (%set-fun-self)
;;   (:policy :fast-safe)
;;   (:translate (setf %simple-fun-self))
;;   (:args (new-self :scs (descriptor-reg) :target result :to :result)
;;          (function :scs (descriptor-reg) :to :result))
;;   (:temporary (:sc any-reg :from (:argument 0) :to :result) temp)
;;   (:results (result :scs (descriptor-reg)))
;;   (:generator 3
;;     (%not-implemented vop)))

;;;; other miscellaneous VOPs

(defknown sb!unix::receive-pending-interrupt () (values))
(define-vop (sb!unix::receive-pending-interrupt)
  (:policy :fast-safe)
  (:translate sb!unix::receive-pending-interrupt)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop)))

(define-vop (halt)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop)))


;;;; Miscellany
