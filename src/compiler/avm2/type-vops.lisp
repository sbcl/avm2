;;;; type testing and checking VOPs for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

;;;; test generation utilities

(defun generate-fixnum-test (value)
  (avm2 :get-local/object value)
  (avm2 :is-type "Fixnum"))

(defun %test-fixnum (value target not-p)
  (generate-fixnum-test value)
  (avm2 (if not-p :if-false :if-true) target))

(defun %test-fixnum-and-headers (value target not-p headers)
  (let ((drop-through (gen-label)))
    (generate-fixnum-test value)
    (avm2 :if-false (if not-p drop-through target))
    (%test-headers value target not-p nil headers drop-through)))

;; we don't have lowtags, so take their values here and turn them back
;; into class names

(defun %test-immediate (value target not-p immediate)
  (avm2 :get-local/object value)
  (avm2 :is-type (ecase immediate
                  ((#.even-fixnum-lowtag #.odd-fixnum-lowtag)
                   "Fixnum")
                  (#.character-widetag
                   "Character")
                  ;; ...
                  ))
  (avm2 (if not-p :if-false :if-true) target))

(defun %test-lowtag (value target not-p lowtag)
  (avm2 :get-local/object value)
  (avm2 :is-type (ecase lowtag
                  ((#.even-fixnum-lowtag #.odd-fixnum-lowtag)
                   "Fixnum")
                  ((#.fun-pointer-lowtag)
                   "Function")
                  ((#.instance-pointer-lowtag)
                   "Instance")
                  ((#.other-pointer-lowtag)
                   "OtherPointer")
                  ((#.list-pointer-lowtag)
                   ;; fixme: nil is a cons, right?
                   "Cons")
                  ;; ...
                  ))
  (avm2 (if not-p :if-false :if-true) target))

(defun uncanonicalize-headers (headers)
  (let ((delta (- other-immediate-1-lowtag other-immediate-0-lowtag)))
    (collect ((results))
      (dolist (header headers)
        (if (atom header)
            (results header)
            (loop
               for tag from (car header)
                 to (cdr header)
                 by delta
               do (results tag))))
      (results))))

(defun %test-headers (value target not-p function-p headers
                            &optional (drop-through (gen-label)))
  (let ((lowtag (if function-p fun-pointer-lowtag other-pointer-lowtag)))
    (multiple-value-bind (equal less-or-equal greater-or-equal when-true when-false)
        ;; EQUAL, LESS-OR-EQUAL and GREATER-OR-EQUAL are the conditions for
        ;; branching to TARGET.  WHEN-TRUE and WHEN-FALSE are the
        ;; labels to branch to when we know it's true and when we know
        ;; it's false respectively.
        (if not-p
            (values :if-ne :if-ge :if-lt drop-through target)
            (values :if-eq :if-le :if-gt target drop-through))
      (declare (ignore less-or-equal greater-or-equal))
      (%test-lowtag value when-false t lowtag)
      (dolist (header (uncanonicalize-headers headers))
        (avm2 :get-local/object value)
        (%get-property "widetag")
        (avm2 :push-int header)
        (avm2 equal when-true))
      (avm2 :jump when-false)))
   (avm2 :%label drop-through))

;;;; type checking and testing

(define-vop (check-type)
  (:args (value :target result :scs (any-reg descriptor-reg)))
  (:results (result :scs (any-reg descriptor-reg)))
  (:vop-var vop)
  (:save-p :compute-only))

(define-vop (type-predicate)
  (:args (value :scs (any-reg descriptor-reg)))
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe))

;;; simpler VOP that don't need a temporary register
(define-vop (simple-check-type)
  (:args (value :target result :scs (any-reg descriptor-reg)))
  (:results (result :scs (any-reg descriptor-reg)
                    :load-if (not (and (sc-is value any-reg descriptor-reg)
                                       (sc-is result control-stack)))))
  (:vop-var vop)
  (:save-p :compute-only))

(define-vop (simple-type-predicate)
  (:args (value :scs (any-reg descriptor-reg control-stack)))
  (:conditional)
  (:info target not-p)
  (:policy :fast-safe))

(defun cost-to-test-types (type-codes)
  (+ (* 2 (length type-codes))
     (if (> (apply #'max type-codes) lowtag-limit) 7 2)))

(defmacro !define-type-vops (pred-name check-name ptype error-code
                             (&rest type-codes)
                             &key (variant nil variant-p) &allow-other-keys)
  (declare (ignore error-code))
  ;; KLUDGE: UGH. Why do we need this eval? Can't we put this in the
  ;; expansion?
  (let* ((cost (cost-to-test-types (mapcar #'eval type-codes)))
         (prefix (if variant-p
                     (concatenate 'string (string variant) "-")
                     "")))
    `(progn
       ,@(when pred-name
           `((define-vop (,pred-name ,(intern (concatenate 'string prefix "TYPE-PREDICATE")))
               (:translate ,pred-name)
               (:generator ,cost
                 (test-type value target not-p (,@type-codes))))))
       ,@(when check-name
           `((define-vop (,check-name ,(intern (concatenate 'string prefix "CHECK-TYPE")))
               (:generator ,cost
                 (let ((error-label (gen-label))
                       (ok-label (gen-label)))
                   (test-type value error-label t (,@type-codes))
                   (avm2 :jump ok-label)

                   (avm2 :%label error-label)
                   (avm2 :push-string "type error")
                   (avm2 :throw)

                   (avm2 :%label ok-label)
                   (avm2 :get-local/object value)
                   (avm2 :set-local result))))))
       ,@(when ptype
           `((primitive-type-vop ,check-name (:check) ,ptype))))))

;;;; other integer ranges

(define-vop (fixnump/unsigned-byte-32 simple-type-predicate)
  (:args (value :scs (unsigned-reg)))
  (:arg-types unsigned-num)
  (:translate fixnump)
  (:generator 5
    (avm2 :get-local/signed value)
    (avm2 :push-int #.sb!xc:most-positive-fixnum)
    (avm2 (if not-p :if-gt :if-le) target)))

;;; A (SIGNED-BYTE 32) can be represented with either fixnum or a bignum with
;;; exactly one digit.

(define-vop (signed-byte-32-p type-predicate)
  (:translate signed-byte-32-p)
  (:generator 45
    (let ((not-target (gen-label)))
      (multiple-value-bind (target not-target)
          (if not-p
              (values not-target target)
              (values target not-target))
        (avm2 :get-local/object value)
        (avm2 :is-type "Fixnum")
        (avm2 :if-true target)

        (avm2 :get-local/object value)
        (avm2 :is-type "Bignum")
        (avm2 :if-false not-target)

        (avm2 :get-local/object value)
        (%get-property "bignumLength")
        (avm2 :push-int 1)
        (avm2 :if-eq target))
      (avm2 :%label not-target))))

(define-vop (check-signed-byte-32 check-type)
  (:generator 45
    (let ((target (gen-label))
          (not-target (gen-label)))
      (avm2 :get-local/object value)
      (avm2 :is-type "Fixnum")
      (avm2 :if-true target)

      (avm2 :get-local/object value)
      (avm2 :is-type "Bignum")
      (avm2 :if-false not-target)

      (avm2 :get-local/object value)
      (%get-property "bignumLength")
      (avm2 :push-int 1)
      (avm2 :if-eq target)

      (avm2 :%label not-target)
      (avm2 :push-string "type-error")
      (avm2 :throw)

      (avm2 :%label target)
      (avm2 :get-local/object value)
      (avm2 :set-local result))))

;;; An (unsigned-byte 32) can be represented with either a positive
;;; fixnum, a bignum with exactly one positive digit, or a bignum with
;;; exactly two digits and the second digit all zeros.
(define-vop (unsigned-byte-32-p type-predicate)
  (:translate unsigned-byte-32-p)
  (:generator 45
    ;; as evident below, this is annoyingly long for an inline expansion.
    ;; let's do it in a method.
    (%find-property "unsignedbyte32p")
    (avm2 :get-local/object value)
    (%call-property "unsignedbyte32p" 1)
    (avm2 (if not-p :if-false :if-true) target)

;;     (let ((not-target (gen-label))
;;        (word-target (gen-label))
;;        (fixnum-target (gen-label))
;;        (bignum-oneword-target (gen-label)))
;;       (multiple-value-bind (target not-target)
;;        (if not-p
;;            (values not-target target)
;;            (values target not-target))
;;      (avm2 :get-local/object value)
;;      (avm2 :is-type "Fixnum")
;;      (avm2 :if-true fixnum-target)

;;      (avm2 :get-local/object value)
;;      (avm2 :is-type "Bignum")
;;      (avm2 :if-false not-target)

;;      (avm2 :get-local/object value)
;;      (%get-property "bignumLength")
;;      (avm2 :push-int 1)
;;      (avm2 :if-eq target)
;;      (avm2 :jump not-target)

;;      (avm2 :%label bignum-oneword-target)
;;      (avm2 :get-local/object value)
;;      (%get-property "bignumLength" 1)
;;      (avm2 :dup)
;;      (avm2 :push-int 1)
;;      (avm2 :if-eq bignum-oneword-target)
;;      (avm2 :push-int 2)
;;      (avm2 :if-ne not-target)
;;      (%find-property "getSecondWord")
;;      (avm2 :get-local/object value)
;;      (%call-property "getSecondWord" 1)
;;      (avm2 :push-int 0)
;;      (avm2 :if-eq target)
;;      (avm2 :jump not-target)

;;      (avm2 :%label bignum-oneword-target)
;;      (avm2 :pop)
;;      (%find-property "moveToWord")
;;      (avm2 :get-local/object value)
;;      (%call-property "moveToWord" 1)
;;      ;; word on stack
;;      (avm2 :jump word-target)

;;      (avm2 :%label fixnum-target)
;;      (%unbox-fixnum value)
;;      ;; word on stack; fall through

;;      (avm2 :%label word-target)
;;      ;; word on stack
;;      (avm2 :push-int 0)
;;      (avm2 :if-lt not-target)
;;      (avm2 :jump target))
;;       (avm2 :%label not-target))
    ))

(define-vop (check-unsigned-byte-32 check-type)
  (:generator 45
    ;; c.f. unsigned-byte-32-p
    (%find-property "unsignedbyte32p")
    (avm2 :get-local/object value)
    (%call-property "unsignedbyte32p" 1)
    (let ((ok-label (gen-label)))
      (avm2 :if-true ok-label)
      (avm2 :push-string "type error")
      (avm2 :throw)
      (avm2 :%label ok-label)
      (avm2 :get-local/object value)
      (avm2 :set-local result))))

;;;; list/symbol types
;;;
;;; symbolp (or symbol (eq nil))
;;; consp (and list (not (eq nil)))

(define-vop (symbolp type-predicate)
  (:translate symbolp)
  (:generator 12
    (let* ((drop-thru (gen-label))
           (is-symbol-label (if not-p drop-thru target)))
      (%find-property "getNil")
      (%call-property "getNil" 0)
      (avm2 :get-local/object value)
      (avm2 :if-eq is-symbol-label)
      (test-type value target not-p (symbol-header-widetag))
      (avm2 :%label drop-thru))))

(define-vop (check-symbol check-type)
  (:generator 12
    (let ((error (gen-label))
          (ok (gen-label)))
      (%find-property "getNil")
      (%call-property "getNil" 0)
      (avm2 :get-local/object value)
      (avm2 :if-eq ok)
      (test-type value error nil (symbol-header-widetag))
      (avm2 :jump ok)

      (avm2 :%label error)
      (avm2 :push-string "type error")
      (avm2 :throw)

      (avm2 :%label ok)
      (avm2 :get-local/object value)
      (avm2 :set-local result))))

(define-vop (consp type-predicate)
  (:translate consp)
  (:generator 8
    (avm2 :get-local/object value)
    (avm2 :is-type "Cons")
    (avm2 (if not-p :if-false :if-true) target)))

(define-vop (check-cons check-type)
  (:generator 8
    (let ((ok (gen-label))
          (error (gen-label)))
      (avm2 :get-local/object value)
      (avm2 :is-type "Cons")
      (avm2 :if-true ok)

      (avm2 :%label error)
      (avm2 :push-string "type-error")
      (avm2 :throw)

      (avm2 :%label ok)
      (avm2 :get-local/object value)
      (avm2 :set-local result))))
