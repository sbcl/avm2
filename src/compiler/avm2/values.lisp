;;;; unknown-values VOPs for the x86 VM

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!VM")

(define-vop (reset-stack-pointer)
  (:args (ptr :scs (any-reg)))
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop ptr)))

(define-vop (%%nip-values)
  (:args (last-nipped-ptr :scs (any-reg))
         (last-preserved-ptr :scs (any-reg))
         (moved-ptrs :scs (any-reg) :more t))
  (:results (r-moved-ptrs :scs (any-reg) :more t)
            ;; same as MOVED-PTRS
            )
  (:ignore r-moved-ptrs)
  (:vop-var vop)
  (:generator 1
    (%not-implemented vop last-nipped-ptr last-preserved-ptr moved-ptrs)))

;;; Push some values onto the stack, returning the start and number of values
;;; pushed as results. It is assumed that the Vals are wired to the standard
;;; argument locations. Nvals is the number of values to push.
;;;
;;; The generator cost is pseudo-random. We could get it right by defining a
;;; bogus SC that reflects the costs of the memory-to-memory moves for each
;;; operand, but this seems unworthwhile.
(define-vop (push-values)
  (:args (vals :more t))
  (:results (start) (count))
  (:info nvals)
  (:vop-var vop)
  (:generator 20
    (%not-implemented vop vals start count nvals)))

;;; Push a list of values on the stack, returning Start and Count as used in
;;; unknown values continuations.
(define-vop (values-list)
  (:args (arg :scs (descriptor-reg)))
  (:arg-types list)
  (:policy :fast-safe)
  (:results (start :scs (any-reg))
            (count :scs (any-reg)))
  (:vop-var vop)
  (:save-p :compute-only)
  (:vop-var vop)
  (:generator 0
    (%not-implemented vop start count arg)))

;;; Copy the more arg block to the top of the stack so we can use them
;;; as function arguments.
;;;
;;; Accepts a context as produced by more-arg-context; points to the first
;;; value on the stack, not 4 bytes above as in other contexts.
;;;
;;; Return a context that is 4 bytes above the first value, suitable for
;;; defining a new stack frame.
(define-vop (%more-arg-values)
  (:args (context :scs (descriptor-reg any-reg))
         (skip :scs (any-reg immediate))
         (num :scs (any-reg)))
  (:arg-types * positive-fixnum positive-fixnum)
  (:results (start :scs (any-reg))
            (count :scs (any-reg)))
  (:vop-var vop)
  (:generator 20
    (%not-implemented vop context skip num start count)))
