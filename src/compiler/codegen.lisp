;;;; the implementation-independent parts of the code generator. We use
;;;; functions and information provided by the VM definition to convert
;;;; IR2 into assembly code. After emitting code, we finish the
;;;; assembly and then do the post-assembly phase.

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!C")

;;;; utilities used during code generation

;;; the number of bytes used by the code object header
(defun component-header-length (&optional
                                (component *component-being-compiled*))
  (let* ((2comp (component-info component))
         (constants (ir2-component-constants 2comp))
         (num-consts (length constants)))
    (ash (logandc2 (1+ num-consts) 1) sb!vm:word-shift)))

;;; the size of the NAME'd SB in the currently compiled component.
;;; This is useful mainly for finding the size for allocating stack
;;; frames.
(defun sb-allocated-size (name)
  (finite-sb-current-size (sb-or-lose name)))

;;; the TN that is used to hold the number stack frame-pointer in
;;; VOP's function, or NIL if no number stack frame was allocated
(defun current-nfp-tn (vop)
  (unless (zerop (sb-allocated-size 'non-descriptor-stack))
    (let ((block (ir2-block-block (vop-block vop))))
    (when (ir2-physenv-number-stack-p
           (physenv-info
            (block-physenv block)))
      (ir2-component-nfp (component-info (block-component block)))))))

;;; the TN that is used to hold the number stack frame-pointer in the
;;; function designated by 2ENV, or NIL if no number stack frame was
;;; allocated
(defun callee-nfp-tn (2env)
  (unless (zerop (sb-allocated-size 'non-descriptor-stack))
    (when (ir2-physenv-number-stack-p 2env)
      (ir2-component-nfp (component-info *component-being-compiled*)))))

;;; the TN used for passing the return PC in a local call to the function
;;; designated by 2ENV
(defun callee-return-pc-tn (2env)
  (ir2-physenv-return-pc-pass 2env))

;;;; specials used during code generation

(defvar *trace-table-info*)
(defvar *code-segment* nil)
(defvar *elsewhere* nil)
(defvar *elsewhere-label* nil)

;;;; noise to emit an instruction trace

(defvar *prev-segment*)
(defvar *prev-vop*)

(defun trace-instruction (segment vop inst args)
  (let ((*standard-output* *compiler-trace-output*))
    (unless (eq *prev-segment* segment)
      (format t "in the ~A segment:~%" (sb!assem:segment-type segment))
      (setf *prev-segment* segment))
    (unless (eq *prev-vop* vop)
      (when vop
        (format t "~%VOP ")
        (if (vop-p vop)
            (print-vop vop)
            (format *compiler-trace-output* "~S~%" vop)))
      (terpri)
      (setf *prev-vop* vop))
    (case inst
      (:label
       (format t "~A:~%" args))
      (:align
       (format t "~0,8T.align~0,8T~A~%" args))
      (t
       (format t "~0,8T~A~@[~0,8T~{~A~^, ~}~]~%" inst args))))
  (values))

;;;; GENERATE-CODE and support routines

;;; standard defaults for slots of SEGMENT objects
(defun default-segment-run-scheduler ()
  (and *assembly-optimize*
        (policy (lambda-bind
                 (block-home-lambda
                  (block-next (component-head *component-being-compiled*))))
                (or (> speed compilation-speed) (> space compilation-speed)))))
(defun default-segment-inst-hook ()
  (and *compiler-trace-output*
       #'trace-instruction))

(defun init-assembler ()
  (setf *code-segment*
        (sb!assem:make-segment :type :regular
                               :run-scheduler (default-segment-run-scheduler)
                               :inst-hook (default-segment-inst-hook)))
  #!+sb-dyncount
  (setf (sb!assem:segment-collect-dynamic-statistics *code-segment*)
        *collect-dynamic-statistics*)
  (setf *elsewhere*
        (sb!assem:make-segment :type :elsewhere
                               :run-scheduler (default-segment-run-scheduler)
                               :inst-hook (default-segment-inst-hook)))
  (values))

(defvar *abc-buffer* nil)
(defvar *abc-elsewhere-buffer* nil)

;;; big kludge
(defun build-xep-assembly (component)
  (let ((labels (mapcar (lambda (entry)
                          ;; (print :entry)
                          ;; (describe (sb!c::entry-info-offset entry))
                          (sb!assem::annotation-avm2name
                           (sb!c::entry-info-offset entry)))
                        (ir2-component-entries (component-info component)))))
    `((:get-local-1)
      (:coerce-i)
      (:lookup-switch invalid-xep-code ,labels)
      (:%label invalid-xep-code)
      (:push-string "invalid XEP code")
      (:throw))))

(defun generate-code (component)
  (when *compiler-trace-output*
    (format *compiler-trace-output*
            "~|~%assembly code for ~S~2%"
            component))
  (let ((prev-env nil)
        (*trace-table-info* nil)
        (*prev-segment* nil)
        (*prev-vop* nil)
        (*fixup-notes* nil)
        #!+avm2
        (*abc-buffer* (make-array 1 :fill-pointer 0 :adjustable t))
        #!+avm2
        (*abc-elsewhere-buffer* (make-array 1 :fill-pointer 0 :adjustable t)))
    (let ((label (sb!assem:gen-label)))
      (setf *elsewhere-label* label)
      (sb!assem:assemble (*elsewhere*)
        (sb!assem:emit-label label))
      #!+avm2
      (vector-push-extend (list :comment "elsewhere")
                          *abc-elsewhere-buffer*)
      #!+avm2
      (vector-push-extend (list :%label
                                (sb!assem::annotation-avm2name label))
                          *abc-elsewhere-buffer*))
    (do-ir2-blocks (block component)
      (let ((1block (ir2-block-block block)))
        (when (and (eq (block-info 1block) block)
                   (block-start 1block))
          (sb!assem:assemble (*code-segment*)
            ;; Align first emitted block of each loop: x86 and x86-64 both
            ;; like 16 byte alignment, however, since x86 aligns code objects
            ;; on 8 byte boundaries we cannot guarantee proper loop alignment
            ;; there (yet.)
            #!+x86-64
            (let ((cloop (sb!c::block-loop 1block)))
              (when (and cloop
                         (sb!c::loop-tail cloop)
                         (not (sb!c::loop-info cloop)))
                (sb!assem:emit-alignment sb!vm:n-lowtag-bits #x90)
                ;; Mark the loop as aligned by saving the IR1 block aligned.
                (setf (sb!c::loop-info cloop) 1block)))
            #!+avm2
            (vector-push-extend (list :%label
                                      (sb!assem::annotation-avm2name
                                       (block-label 1block)))
                                *abc-buffer*)
            (sb!assem:emit-label (block-label 1block)))
          (let ((env (block-physenv 1block)))
            (unless (eq env prev-env)
              (let ((lab (gen-label)))
                (setf (ir2-physenv-elsewhere-start (physenv-info env))
                      lab)
                #!+avm2
                (vector-push-extend (list :%label
                                          (sb!assem::annotation-avm2name
                                           (block-label 1block)))
                                    *abc-elsewhere-buffer*)
                (emit-label-elsewhere lab))
              (setq prev-env env)))))
      (do ((vop (ir2-block-start-vop block) (vop-next vop)))
          ((null vop))
        (let ((gen (vop-info-generator-function (vop-info vop))))
          (if gen
            (funcall gen vop)
            (format t
                    "missing generator for ~S~%"
                    (template-name (vop-info vop)))))))
    (sb!assem:append-segment *code-segment* *elsewhere*)
    (setf *elsewhere* nil)
    #!+avm2
    (with-simple-restart (ignore "ignore this error recklessly")
      (let ((method-info
             #+sb-xc-host
              (avm2-asm:assemble-method-body
               (concatenate 'list
                            (build-xep-assembly component)
                            *abc-buffer*
                            *abc-elsewhere-buffer*))
              #-sb-xc-host
              (error "fixme")))
        (setf (sb!assem::segment-avm2-method-info *code-segment*) method-info)
        (values (length (avm2-asm::code method-info))
                (nreverse *trace-table-info*)
                *fixup-notes*)))
    #!-avm2
    (values (sb!assem:finalize-segment *code-segment*)
            (nreverse *trace-table-info*)
            *fixup-notes*)))

(defun emit-label-elsewhere (label)
  (sb!assem:assemble (*elsewhere*)
    (sb!assem:emit-label label)))

(defun label-elsewhere-p (label-or-posn)
  (<= (label-position *elsewhere-label*)
      (etypecase label-or-posn
        (label
         (label-position label-or-posn))
        (index
         label-or-posn))))
