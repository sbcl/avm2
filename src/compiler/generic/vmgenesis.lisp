;;;; "cold" core image builder: This is how we create a target Lisp
;;;; system from scratch, by converting from fasl files to an image
;;;; file in the cross-compilation host, without the help of the
;;;; target Lisp system.
;;;;
;;;; As explained by Rob MacLachlan on the CMU CL mailing list Wed, 06
;;;; Jan 1999 11:05:02 -0500, this cold load generator more or less
;;;; fakes up static function linking. I.e. it makes sure that all the
;;;; DEFUN-defined functions in the fasl files it reads are bound to the
;;;; corresponding symbols before execution starts. It doesn't do
;;;; anything to initialize variable values; instead it just arranges
;;;; for !COLD-INIT to be called at cold load time. !COLD-INIT is
;;;; responsible for explicitly initializing anything which has to be
;;;; initialized early before it transfers control to the ordinary
;;;; top level forms.
;;;;
;;;; (In CMU CL, and in SBCL as of 0.6.9 anyway, functions not defined
;;;; by DEFUN aren't set up specially by GENESIS. In particular,
;;;; structure slot accessors are not set up. Slot accessors are
;;;; available at cold init time because they're usually compiled
;;;; inline. They're not available as out-of-line functions until the
;;;; toplevel forms installing them have run.)

;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!FASL")

;;; a magic number used to identify our core files
(defconstant core-magic
  (logior (ash (sb!xc:char-code #\S) 24)
          (ash (sb!xc:char-code #\B) 16)
          (ash (sb!xc:char-code #\C) 8)
          (sb!xc:char-code #\L)))

;;; the current version of SBCL core files
;;;
;;; FIXME: This is left over from CMU CL, and not well thought out.
;;; It's good to make sure that the runtime doesn't try to run core
;;; files from the wrong version, but a single number is not the ideal
;;; way to do this in high level data like this (as opposed to e.g. in
;;; IP packets), and in fact the CMU CL version number never ended up
;;; being incremented past 0. A better approach might be to use a
;;; string which is set from CVS data. (Though now as of sbcl-0.7.8 or
;;; so, we have another problem that the core incompatibility
;;; detection mechanisms are on such a hair trigger -- with even
;;; different builds from the same sources being considered
;;; incompatible -- that any coarser-grained versioning mechanisms
;;; like this are largely irrelevant as long as the hair-triggering
;;; persists.)
;;;
;;; 0: inherited from CMU CL
;;; 1: rearranged static symbols for sbcl-0.6.8
;;; 2: eliminated non-ANSI %DEFCONSTANT/%%DEFCONSTANT support,
;;;    deleted a slot from DEBUG-SOURCE structure
;;; 3: added build ID to cores to discourage sbcl/.core mismatch
;;; 4: added gc page table data
(defconstant sbcl-core-version-integer 4)

;;;; implementing the concept of "vector" in (almost) portable
;;;; Common Lisp
;;;;
;;;; "If you only need to do such simple things, it doesn't really
;;;; matter which language you use." -- _ANSI Common Lisp_, p. 1, Paul
;;;; Graham (evidently not considering the abstraction "vector" to be
;;;; such a simple thing:-)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +smallvec-length+
    (expt 2 16)))

;;; an element of a BIGVEC -- a vector small enough that we have
;;; a good chance of it being portable to other Common Lisps
(deftype smallvec ()
  `(simple-array (unsigned-byte 8) (,+smallvec-length+)))

(defun make-smallvec ()
  (make-array +smallvec-length+ :element-type '(unsigned-byte 8)))

;;; a big vector, implemented as a vector of SMALLVECs
;;;
;;; KLUDGE: This implementation seems portable enough for our
;;; purposes, since realistically every modern implementation is
;;; likely to support vectors of at least 2^16 elements. But if you're
;;; masochistic enough to read this far into the contortions imposed
;;; on us by ANSI and the Lisp community, for daring to use the
;;; abstraction of a large linearly addressable memory space, which is
;;; after all only directly supported by the underlying hardware of at
;;; least 99% of the general-purpose computers in use today, then you
;;; may be titillated to hear that in fact this code isn't really
;;; portable, because as of sbcl-0.7.4 we need somewhat more than
;;; 16Mbytes to represent a core, and ANSI only guarantees that
;;; ARRAY-DIMENSION-LIMIT is not less than 1024. -- WHN 2002-06-13
(defstruct bigvec
  (outer-vector (vector (make-smallvec)) :type (vector smallvec)))

;;; analogous to SVREF, but into a BIGVEC
(defun bvref (bigvec index)
  (multiple-value-bind (outer-index inner-index)
      (floor index +smallvec-length+)
    (aref (the smallvec
            (svref (bigvec-outer-vector bigvec) outer-index))
          inner-index)))
(defun (setf bvref) (new-value bigvec index)
  (multiple-value-bind (outer-index inner-index)
      (floor index +smallvec-length+)
    (setf (aref (the smallvec
                  (svref (bigvec-outer-vector bigvec) outer-index))
                inner-index)
          new-value)))

;;; analogous to LENGTH, but for a BIGVEC
;;;
;;; the length of BIGVEC, measured in the number of BVREFable bytes it
;;; can hold
(defun bvlength (bigvec)
  (* (length (bigvec-outer-vector bigvec))
     +smallvec-length+))

;;; analogous to WRITE-SEQUENCE, but for a BIGVEC
(defun write-bigvec-as-sequence (bigvec stream &key (start 0) end)
  (loop for i of-type index from start below (or end (bvlength bigvec)) do
        (write-byte (bvref bigvec i)
                    stream)))

;;; analogous to READ-SEQUENCE-OR-DIE, but for a BIGVEC
(defun read-bigvec-as-sequence-or-die (bigvec stream &key (start 0) end)
  (loop for i of-type index from start below (or end (bvlength bigvec)) do
        (setf (bvref bigvec i)
              (read-byte stream))))

;;; Grow BIGVEC (exponentially, so that large increases in size have
;;; asymptotic logarithmic cost per byte).
(defun expand-bigvec (bigvec)
  (let* ((old-outer-vector (bigvec-outer-vector bigvec))
         (length-old-outer-vector (length old-outer-vector))
         (new-outer-vector (make-array (* 2 length-old-outer-vector))))
    (dotimes (i length-old-outer-vector)
      (setf (svref new-outer-vector i)
            (svref old-outer-vector i)))
    (loop for i from length-old-outer-vector below (length new-outer-vector) do
          (setf (svref new-outer-vector i)
                (make-smallvec)))
    (setf (bigvec-outer-vector bigvec)
          new-outer-vector))
  bigvec)

;;;; looking up bytes and multi-byte values in a BIGVEC (considering
;;;; it as an image of machine memory on the cross-compilation target)

;;; BVREF-32 and friends. These are like SAP-REF-n, except that
;;; instead of a SAP we use a BIGVEC.
(macrolet ((make-bvref-n
            (n)
            (let* ((name (intern (format nil "BVREF-~A" n)))
                   (number-octets (/ n 8))
                   (ash-list-le
                    (loop for i from 0 to (1- number-octets)
                          collect `(ash (bvref bigvec (+ byte-index ,i))
                                        ,(* i 8))))
                   (ash-list-be
                    (loop for i from 0 to (1- number-octets)
                          collect `(ash (bvref bigvec
                                               (+ byte-index
                                                  ,(- number-octets 1 i)))
                                        ,(* i 8))))
                   (setf-list-le
                    (loop for i from 0 to (1- number-octets)
                          append
                          `((bvref bigvec (+ byte-index ,i))
                            (ldb (byte 8 ,(* i 8)) new-value))))
                   (setf-list-be
                    (loop for i from 0 to (1- number-octets)
                          append
                          `((bvref bigvec (+ byte-index ,i))
                            (ldb (byte 8 ,(- n 8 (* i 8))) new-value)))))
              `(progn
                 (defun ,name (bigvec byte-index)
                   (logior ,@(ecase sb!c:*backend-byte-order*
                               (:little-endian ash-list-le)
                               (:big-endian ash-list-be))))
                 (defun (setf ,name) (new-value bigvec byte-index)
                   (setf ,@(ecase sb!c:*backend-byte-order*
                             (:little-endian setf-list-le)
                             (:big-endian setf-list-be))))))))
  (make-bvref-n 8)
  (make-bvref-n 16)
  (make-bvref-n 32)
  (make-bvref-n 64))

;; lispobj-sized word, whatever that may be
;; hopefully nobody ever wants a 128-bit SBCL...
#!+#.(cl:if (cl:= 64 sb!vm:n-word-bits) '(and) '(or))
(progn
(defun bvref-word (bytes index)
  (bvref-64 bytes index))
(defun (setf bvref-word) (new-val bytes index)
  (setf (bvref-64 bytes index) new-val)))

#!+#.(cl:if (cl:= 32 sb!vm:n-word-bits) '(and) '(or))
(progn
(defun bvref-word (bytes index)
  (bvref-32 bytes index))
(defun (setf bvref-word) (new-val bytes index)
  (setf (bvref-32 bytes index) new-val)))


;;;; representation of spaces in the core

(defun descriptor-fixnum (des)
  (sb!vm::cold-fixnum-value des))

(defun descriptor-word-sized-integer (des)
  (etypecase des
    (sb!vm::cold-fixnum
     (sb!vm::cold-fixnum-value des))
    (sb!vm::cold-bignum
     (elt (sb!vm::cold-bignum-digits des) 0))))

(defun make-fixnum-descriptor (num)
  (when (>= (integer-length num)
            (1+ (- sb!vm:n-word-bits sb!vm:n-lowtag-bits)))
    (error "~W is too big for a fixnum." num))
  (sb!vm::make-cold-fixnum :value num))

(defun make-character-descriptor (data)
  (sb!vm::make-cold-character :code data))


;;;; miscellaneous variables and other noise

;;; a numeric value to be returned for undefined foreign symbols, or NIL if
;;; undefined foreign symbols are to be treated as an error.
;;; (In the first pass of GENESIS, needed to create a header file before
;;; the C runtime can be built, various foreign symbols will necessarily
;;; be undefined, but we don't need actual values for them anyway, and
;;; we can just use 0 or some other placeholder. In the second pass of
;;; GENESIS, all foreign symbols should be defined, so any undefined
;;; foreign symbol is a problem.)
;;;
;;; KLUDGE: It would probably be cleaner to rewrite GENESIS so that it
;;; never tries to look up foreign symbols in the first place unless
;;; it's actually creating a core file (as in the second pass) instead
;;; of using this hack to allow it to go through the motions without
;;; causing an error. -- WHN 20000825
(defvar *foreign-symbol-placeholder-value*)

;;; a handle on the trap object
(defvar *unbound-marker*)

;;; a handle on the NIL object
(defvar *nil-descriptor*)

;;; the head of a list of TOPLEVEL-THINGs describing stuff to be done
;;; when the target Lisp starts up
;;;
;;; Each TOPLEVEL-THING can be a function to be executed or a fixup or
;;; loadtime value, represented by (CONS KEYWORD ..). The FILENAME
;;; tells which fasl file each list element came from, for debugging
;;; purposes.
(defvar *current-reversed-cold-toplevels*)

;;; the name of the object file currently being cold loaded (as a string, not a
;;; pathname), or NIL if we're not currently cold loading any object file
(defvar *cold-load-filename* nil)
(declaim (type (or string null) *cold-load-filename*))

;;;; miscellaneous stuff to read and write the core memory

;;; FIXME: should be DEFINE-MODIFY-MACRO
(defmacro cold-push (thing list)
  #!+sb-doc
  "Push THING onto the given cold-load LIST."
  `(setq ,list (cold-cons ,thing ,list)))

;; (declaim (ftype (function (sb!vm:word descriptor) (values))
;;                 note-load-time-value-reference))
;; (defun note-load-time-value-reference (address marker)
;;   (cold-push (cold-cons
;;               (cold-intern :load-time-value-fixup)
;;               (cold-cons (sap-int-to-core address)
;;                          (cold-cons
;;                           (number-to-core (descriptor-word-offset marker))
;;                           *nil-descriptor*)))
;;              *current-reversed-cold-toplevels*)
;;   (values))

;;;; copying simple objects into the cold core

(defun make-cold-vector (widetag data)
  (sb!vm::make-cold-vector
   :widetag widetag
   :length (make-fixnum-descriptor (length data))
   :data data))

(defun base-string-to-core (string)
  #!+sb-doc
  "Copy STRING (which must only contain STANDARD-CHARs) into the cold
core and return a descriptor to it."
  ;; (No null byte at the end, in contrast to #!-avm2 conventions.)
  (make-cold-vector sb!vm:simple-base-string-widetag
                    (map 'vector #'char-code string)))

(defun bignum-to-core (n)
  #!+sb-doc
  "Copy a bignum to the cold core."
  (let* ((words (ceiling (1+ (integer-length n)) sb!vm:n-word-bits))
         (data (make-array words))
         (handle (sb!vm::make-cold-bignum :digits data)))
    (declare (fixnum words))
    (do ((index 0 (1+ index))
         (remainder n (ash remainder (- sb!vm:n-word-bits))))
        ((>= index words)
         (unless (zerop (integer-length remainder))
           ;; FIXME: Shouldn't this be a fatal error?
           (warn "~W words of ~W were written, but ~W bits were left over."
                 words n remainder)))
      (let ((word (ldb (byte sb!vm:n-word-bits 0) remainder)))
        (setf (elt (sb!vm::cold-bignum-digits handle) index)
              word)))
    handle))

(defun float-to-core (x)
  (etypecase x
    (single-float
     (sb!vm::make-cold-single-float :value (float x 1.0d0)))
    (double-float
     (sb!vm::make-cold-double-float :value x))))

(defun complex-single-float-to-core (num)
  (declare (type (complex single-float) num))
  (sb!vm::make-cold-complex-single-float
   :real (float-to-core (realpart num))
   :imag (float-to-core (imagpart num))))

(defun complex-double-float-to-core (num)
  (declare (type (complex double-float) num))
  (sb!vm::make-cold-complex-double-float
   :real (float-to-core (realpart num))
   :imag (float-to-core (imagpart num))))

;;; Copy the given number to the core.
(defun number-to-core (number)
  (typecase number
    (integer (if (< (integer-length number)
                    (- (1+ sb!vm:n-word-bits) sb!vm:n-lowtag-bits))
                 (make-fixnum-descriptor number)
                 (bignum-to-core number)))
    (ratio (sb!vm::make-cold-ratio
            :numerator (number-to-core (numerator number))
            :denominator (number-to-core (denominator number))))
    ((complex single-float) (complex-single-float-to-core number))
    ((complex double-float) (complex-double-float-to-core number))
    #!+long-float
    ((complex long-float)
     (error "~S isn't a cold-loadable number at all!" number))
    (complex
     (sb!vm::make-cold-complex
      :real (number-to-core (realpart number))
      :imag (number-to-core (imagpart number))))
    (float (float-to-core number))
    (t (error "~S isn't a cold-loadable number at all!" number))))

(declaim (ftype (function (sb!vm:word) sb!vm::cold-object) sap-int-to-core))
(defun sap-int-to-core (sap-int)
  ;; we don't actually have SAPs, but let's play along for now
  (sb!vm::make-cold-sap :pointer sap-int))

;;; Allocate a cons cell in GSPACE and fill it in with CAR and CDR.
(defun cold-cons (car cdr)
  (sb!vm::make-cold-cons :car car :cdr cdr))

;;; Make a simple-vector on the target that holds the specified
;;; OBJECTS, and return its descriptor.
(defun vector-in-core (&rest objects)
  (make-cold-vector sb!vm:simple-vector-widetag (coerce objects 'vector)))

;;;; symbol magic

;;; Allocate (and initialize) a symbol.
(defun allocate-symbol (name)
  (declare (simple-string name))
  (sb!vm::make-cold-symbol :value *unbound-marker*
                           :hash (make-fixnum-descriptor 0)
                           :plist *nil-descriptor*
                           :name (base-string-to-core name)
                           :package *nil-descriptor*))

;;; Set the cold symbol value of COLD-OR-WARM-SYMBOL, which can be either a
;;; descriptor of a cold symbol or (in an abbreviation for the
;;; most common usage pattern) an ordinary symbol, which will be
;;; automatically cold-interned.
(declaim (ftype (function ((or symbol sb!vm::cold-symbol) sb!vm::cold-object))
                cold-set))
(defun cold-set (cold-or-warm-symbol value)
  (setf (sb!vm::cold-symbol-value
         (etypecase cold-or-warm-symbol
           (sb!vm::cold-symbol cold-or-warm-symbol)
           (symbol (cold-intern cold-or-warm-symbol))))
        value))

;;;; layouts and type system pre-initialization

;;; Since we want to be able to dump structure constants and
;;; predicates with reference layouts, we need to create layouts at
;;; cold-load time. We use the name to intern layouts by, and dump a
;;; list of all cold layouts in *!INITIAL-LAYOUTS* so that type system
;;; initialization can find them. The only thing that's tricky [sic --
;;; WHN 19990816] is initializing layout's layout, which must point to
;;; itself.

;;; a map from class names to lists of
;;;    `(,descriptor ,name ,length ,inherits ,depth)
;;; KLUDGE: It would be more understandable and maintainable to use
;;; DEFSTRUCT (:TYPE LIST) here. -- WHN 19990823
(defvar *cold-layouts* (make-hash-table :test 'equal))

;;; a map from COLD-INSTANCEs of cold layouts to the name, for inverting
;;; mapping
(defvar *cold-layout-names* (make-hash-table))

;;; FIXME: *COLD-LAYOUTS* and *COLD-LAYOUT-NAMES* should be
;;; initialized by binding in GENESIS.

;;; the descriptor for layout's layout (needed when making layouts)
(defvar *layout-layout*)

(defconstant target-layout-length
  (layout-length (find-layout 'layout)))

(defun target-layout-index (slot-name)
  ;; KLUDGE: this is a little bit sleazy, but the tricky thing is that
  ;; structure slots don't have a terribly firm idea of their names.
  ;; At least here if we change LAYOUT's package of definition, we
  ;; only have to change one thing...
  (let* ((name (find-symbol (symbol-name slot-name) "SB!KERNEL"))
         (layout (find-layout 'layout))
         (dd (layout-info layout))
         (slots (dd-slots dd))
         (dsd (find name slots :key #'dsd-name)))
    (aver dsd)
    (dsd-index dsd)))

;;; Return a list of names created from the cold layout INHERITS data
;;; in X.
(defun listify-cold-inherits (cold-vector)
  (let ((len (descriptor-fixnum (sb!vm::cold-vector-length cold-vector)))
        (data (sb!vm::cold-vector-data cold-vector)))
    (collect ((res))
      (dotimes (index len)
        (let* ((des (elt data index))
               (found (gethash des *cold-layout-names*)))
          (if found
              (res found)
              (error "unknown descriptor at index ~S (~A)" index des))))
      (res))))

(defun cold-get-layout-slot (cold-layout slot-name)
  (elt (sb!vm::cold-instance-slots cold-layout)
       (target-layout-index slot-name)))

(defun (setf cold-get-layout-slot) (newval cold-layout slot-name)
  (setf (elt (sb!vm::cold-instance-slots cold-layout)
             (target-layout-index slot-name))
        newval))

;; fixme: remove this older function in favour of the (setf) variant
(defun cold-set-layout-slot (cold-layout slot-name value)
  (setf (cold-get-layout-slot cold-layout slot-name) value))

(declaim (ftype (function (symbol sb!vm::cold-object sb!vm::cold-object sb!vm::cold-object sb!vm::cold-object)
                          sb!vm::cold-object)
                make-cold-layout))
(defun make-cold-layout (name length inherits depthoid nuntagged)
  (let* ((slot-data-vector (make-array (1+ target-layout-length)))
         (result (sb!vm::make-cold-instance
                  ;; KLUDGE: Why 1+? -- WHN 19990901
                  ;; header word? -- CSR 20051204
                  ;; no, slot 0, the layout -- DFL 20090129
                  :slots slot-data-vector)))
    ;; KLUDGE: The offsets into LAYOUT below should probably be pulled out
    ;; of the cross-compiler's tables at genesis time instead of inserted
    ;; by hand as bare numeric constants. -- WHN ca. 19990901

    ;; Set slot 0 = the layout of the layout.
    (setf (elt slot-data-vector 0) *layout-layout*)

    ;; Set the CLOS hash value.
    ;;
    ;; Note: CMU CL didn't set these in genesis, but instead arranged
    ;; for them to be set at cold init time. That resulted in slightly
    ;; kludgy-looking code, but there were at least two things to be
    ;; said for it:
    ;;   1. It put the hash values under the control of the target Lisp's
    ;;      RANDOM function, so that CLOS behavior would be nearly
    ;;      deterministic (instead of depending on the implementation of
    ;;      RANDOM in the cross-compilation host, and the state of its
    ;;      RNG when genesis begins).
    ;;   2. It automatically ensured that all hash values in the target Lisp
    ;;      were part of the same sequence, so that we didn't have to worry
    ;;      about the possibility of the first hash value set in genesis
    ;;      being precisely equal to the some hash value set in cold init time
    ;;      (because the target Lisp RNG has advanced to precisely the same
    ;;      state that the host Lisp RNG was in earlier).
    ;; Point 1 should not be an issue in practice because of the way we do our
    ;; build procedure in two steps, so that the SBCL that we end up with has
    ;; been created by another SBCL (whose RNG is under our control).
    ;; Point 2 is more of an issue. If ANSI had provided a way to feed
    ;; entropy into an RNG, we would have no problem: we'd just feed
    ;; some specialized genesis-time-only pattern into the RNG state
    ;; before using it. However, they didn't, so we have a slight
    ;; problem. We address it by generating the hash values using a
    ;; different algorithm than we use in ordinary operation.
    (let (;; The expression here is pretty arbitrary, we just want
          ;; to make sure that it's not something which is (1)
          ;; evenly distributed and (2) not foreordained to arise in
          ;; the target Lisp's (RANDOM-LAYOUT-CLOS-HASH) sequence
          ;; and show up as the CLOS-HASH value of some other
          ;; LAYOUT.
          (hash-value
           (1+ (mod (logxor (logand   (random-layout-clos-hash) 15253)
                            (logandc2 (random-layout-clos-hash) 15253)
                            1)
                    (1- sb!kernel:layout-clos-hash-limit)))))
      (cold-set-layout-slot result 'clos-hash
                            (make-fixnum-descriptor hash-value)))

    ;; Set other slot values.
    ;;
    ;; leave CLASSOID uninitialized for now
    (cold-set-layout-slot result 'invalid *nil-descriptor*)
    (cold-set-layout-slot result 'inherits inherits)
    (cold-set-layout-slot result 'depthoid depthoid)
    (cold-set-layout-slot result 'length length)
    (cold-set-layout-slot result 'info *nil-descriptor*)
    (cold-set-layout-slot result 'pure *nil-descriptor*)
    (cold-set-layout-slot result 'n-untagged-slots nuntagged)
    (cold-set-layout-slot result 'for-std-class-p *nil-descriptor*)

    (setf (gethash name *cold-layouts*)
          (list result
                name
                (descriptor-fixnum length)
                (listify-cold-inherits inherits)
                (descriptor-fixnum depthoid)
                (descriptor-fixnum nuntagged)))
    (setf (gethash result *cold-layout-names*) name)

    result))

(defun initialize-layouts ()

  (clrhash *cold-layouts*)

  ;; We initially create the layout of LAYOUT itself with NIL as the LAYOUT and
  ;; #() as INHERITS,
  (setq *layout-layout* *nil-descriptor*)
  (let ((xlayout-layout (find-layout 'layout)))
    (aver (= 0 (layout-n-untagged-slots xlayout-layout)))
    (setq *layout-layout*
          (make-cold-layout 'layout
                            (number-to-core target-layout-length)
                            (vector-in-core)
                            (number-to-core (layout-depthoid xlayout-layout))
                            (number-to-core 0)))
    (setf (elt (sb!vm::cold-instance-slots *layout-layout*) 0)
          *layout-layout*)

  ;; Then we create the layouts that we'll need to make a correct INHERITS
  ;; vector for the layout of LAYOUT itself..
  ;;
  ;; FIXME: The various LENGTH and DEPTHOID numbers should be taken from
  ;; the compiler's tables, not set by hand.
  (let* ((t-layout
          (make-cold-layout 't
                            (number-to-core 0)
                            (vector-in-core)
                            (number-to-core 0)
                            (number-to-core 0)))
         (so-layout
          (make-cold-layout 'structure-object
                            (number-to-core 1)
                            (vector-in-core t-layout)
                            (number-to-core 1)
                            (number-to-core 0)))
         (bso-layout
          (make-cold-layout 'structure!object
                            (number-to-core 1)
                            (vector-in-core t-layout so-layout)
                            (number-to-core 2)
                            (number-to-core 0)))
         (layout-inherits (vector-in-core t-layout
                                          so-layout
                                          bso-layout)))

    ;; ..and return to backpatch the layout of LAYOUT.
    (setf (fourth (gethash 'layout *cold-layouts*))
          (listify-cold-inherits layout-inherits))
    (cold-set-layout-slot *layout-layout* 'inherits layout-inherits))))

;;;; interning symbols in the cold image

;;; In order to avoid having to know about the package format, we
;;; build a data structure in *COLD-PACKAGE-SYMBOLS* that holds all
;;; interned symbols along with info about their packages. The data
;;; structure is a list of sublists, where the sublists have the
;;; following format:
;;;   (<make-package-arglist>
;;;    <internal-symbols>
;;;    <external-symbols>
;;;    <imported-internal-symbols>
;;;    <imported-external-symbols>
;;;    <shadowing-symbols>
;;;    <package-documentation>)
;;;
;;; KLUDGE: It would be nice to implement the sublists as instances of
;;; a DEFSTRUCT (:TYPE LIST). (They'd still be lists, but at least we'd be
;;; using mnemonically-named operators to access them, instead of trying
;;; to remember what THIRD and FIFTH mean, and hoping that we never
;;; need to change the list layout..) -- WHN 19990825

;;; an alist from packages to lists of that package's symbols to be dumped
(defvar *cold-package-symbols*)
(declaim (type list *cold-package-symbols*))

;;; a map from descriptors to symbols, so that we can back up. The key
;;; is the cold object.
(defvar *cold-symbols*)
(declaim (type hash-table *cold-symbols*))

;;; sanity check for a symbol we're about to create on the target
;;;
;;; Make sure that the symbol has an appropriate package. In
;;; particular, catch the so-easy-to-make error of typing something
;;; like SB-KERNEL:%BYTE-BLT in cold sources when what you really
;;; need is SB!KERNEL:%BYTE-BLT.
(defun package-ok-for-target-symbol-p (package)
  (let ((package-name (package-name package)))
    (or
     ;; Cold interning things in these standard packages is OK. (Cold
     ;; interning things in the other standard package, CL-USER, isn't
     ;; OK. We just use CL-USER to expose symbols whose homes are in
     ;; other packages. Thus, trying to cold intern a symbol whose
     ;; home package is CL-USER probably means that a coding error has
     ;; been made somewhere.)
     (find package-name '("COMMON-LISP" "KEYWORD") :test #'string=)
     ;; Cold interning something in one of our target-code packages,
     ;; which are ever-so-rigorously-and-elegantly distinguished by
     ;; this prefix on their names, is OK too.
     (string= package-name "SB!" :end1 3 :end2 3)
     ;; This one is OK too, since it ends up being COMMON-LISP on the
     ;; target.
     (string= package-name "SB-XC")
     ;; Anything else looks bad. (maybe COMMON-LISP-USER? maybe an extension
     ;; package in the xc host? something we can't think of
     ;; a valid reason to cold intern, anyway...)
     )))

;;; like SYMBOL-PACKAGE, but safe for symbols which end up on the target
;;;
;;; Most host symbols we dump onto the target are created by SBCL
;;; itself, so that as long as we avoid gratuitously
;;; cross-compilation-unfriendly hacks, it just happens that their
;;; SYMBOL-PACKAGE in the host system corresponds to their
;;; SYMBOL-PACKAGE in the target system. However, that's not the case
;;; in the COMMON-LISP package, where we don't get to create the
;;; symbols but instead have to use the ones that the xc host created.
;;; In particular, while ANSI specifies which symbols are exported
;;; from COMMON-LISP, it doesn't specify that their home packages are
;;; COMMON-LISP, so the xc host can keep them in random packages which
;;; don't exist on the target (e.g. CLISP keeping some CL-exported
;;; symbols in the CLOS package).
(defun symbol-package-for-target-symbol (symbol)
  ;; We want to catch weird symbols like CLISP's
  ;; CL:FIND-METHOD=CLOS::FIND-METHOD, but we don't want to get
  ;; sidetracked by ordinary symbols like :CHARACTER which happen to
  ;; have the same SYMBOL-NAME as exports from COMMON-LISP.
  (multiple-value-bind (cl-symbol cl-status)
      (find-symbol (symbol-name symbol) *cl-package*)
    (if (and (eq symbol cl-symbol)
             (eq cl-status :external))
        ;; special case, to work around possible xc host weirdness
        ;; in COMMON-LISP package
        *cl-package*
        ;; ordinary case
        (let ((result (symbol-package symbol)))
          (unless (package-ok-for-target-symbol-p result)
            (bug "~A in bad package for target: ~A" symbol result))
          result))))

;;; Return a handle on an interned symbol. If necessary allocate the
;;; symbol and record which package the symbol was referenced in. When
;;; we allocate the symbol, make sure we record a reference to the
;;; symbol in the home package so that the package gets set.
(defun cold-intern (symbol
                    &optional
                    (package (symbol-package-for-target-symbol symbol)))

  (aver (package-ok-for-target-symbol-p package))

  ;; Anything on the cross-compilation host which refers to the target
  ;; machinery through the host SB-XC package should be translated to
  ;; something on the target which refers to the same machinery
  ;; through the target COMMON-LISP package.
  (let ((p (find-package "SB-XC")))
    (when (eq package p)
      (setf package *cl-package*))
    (when (eq (symbol-package symbol) p)
      (setf symbol (intern (symbol-name symbol) *cl-package*))))

  (let (;; Information about each cold-interned symbol is stored
        ;; in COLD-INTERN-INFO.
        ;;   (CAR COLD-INTERN-INFO) = descriptor of symbol
        ;;   (CDR COLD-INTERN-INFO) = list of packages, other than symbol's
        ;;                            own package, referring to symbol
        ;; (*COLD-PACKAGE-SYMBOLS* and *COLD-SYMBOLS* store basically the
        ;; same information, but with the mapping running the opposite way.)
        (cold-intern-info (get symbol 'cold-intern-info)))
    (unless cold-intern-info
      (cond ((eq (symbol-package-for-target-symbol symbol) package)
             (let ((handle (allocate-symbol (symbol-name symbol))))
               (setf (gethash handle *cold-symbols*) symbol)
               (when (eq package *keyword-package*)
                 (cold-set handle handle))
               (setq cold-intern-info
                     (setf (get symbol 'cold-intern-info) (cons handle nil)))))
            (t
             (cold-intern symbol)
             (setq cold-intern-info (get symbol 'cold-intern-info)))))
    (unless (or (null package)
                (member package (cdr cold-intern-info)))
      (push package (cdr cold-intern-info))
      (let* ((old-cps-entry (assoc package *cold-package-symbols*))
             (cps-entry (or old-cps-entry
                            (car (push (list package)
                                       *cold-package-symbols*)))))
        (unless old-cps-entry
          (/show "created *COLD-PACKAGE-SYMBOLS* entry for" package symbol))
        (push symbol (rest cps-entry))))
    (car cold-intern-info)))

(defun make-unbound-marker-descriptor ()
  (sb!vm::make-cold-unbound-marker))

;;; Construct and return a value for use as *NIL-DESCRIPTOR*.
(defun make-nil-descriptor ()
  (let ((result (sb!vm::make-cold-symbol
                 :value :dummy
                 :hash :dummy
                 :plist :dummy
                 :name (base-string-to-core "NIL")
                 :package :dummy)))
    (setf (sb!vm::cold-symbol-value result) result)
    (setf (sb!vm::cold-symbol-hash result) result)
    (setf (sb!vm::cold-symbol-plist result) result)
    (setf (sb!vm::cold-symbol-package result) result)
    (setf (get nil 'cold-intern-info)
          (cons result nil))
    (cold-intern nil)
    result))

(defvar *cold-static-symbols*)

;;; Since the initial symbols must be allocated before we can intern
;;; anything else, we intern those here. We also set the value of T.
(defun initialize-non-nil-symbols ()
  #!+sb-doc
  "Initialize the cold load symbol-hacking data structures."
  (let ()
    ;; Intern the others.
    (dolist (symbol sb!vm:*static-symbols*)
      (vector-push (cold-intern symbol) *cold-static-symbols*)
      (let ((offset-wanted (sb!vm:static-symbol-offset symbol))
            (offset-found (length *cold-static-symbols*)))
        (aver (= offset-wanted offset-found))))
    ;; Establish the value of T.
    (let ((t-symbol (cold-intern t)))
      (cold-set t-symbol t-symbol))))

;;; a helper function for FINISH-SYMBOLS: Return a cold alist suitable
;;; to be stored in *!INITIAL-LAYOUTS*.
(defun cold-list-all-layouts ()
  (let ((result *nil-descriptor*))
    (maphash (lambda (key stuff)
               (cold-push (cold-cons (cold-intern key)
                                     (first stuff))
                          result))
             *cold-layouts*)
    result))

;;; Establish initial values for magic symbols.
;;;
;;; Scan over all the symbols referenced in each package in
;;; *COLD-PACKAGE-SYMBOLS* making that for each one there's an
;;; appropriate entry in the *!INITIAL-SYMBOLS* data structure to
;;; intern it.
(defun finish-symbols ()

  ;; I think the point of setting these functions into SYMBOL-VALUEs
  ;; here, instead of using SYMBOL-FUNCTION, is that in CMU CL
  ;; SYMBOL-FUNCTION reduces to FDEFINITION, which is a pretty
  ;; hairy operation (involving globaldb.lisp etc.) which we don't
  ;; want to invoke early in cold init. -- WHN 2001-12-05
  ;;
  ;; FIXME: So OK, that's a reasonable reason to do something weird like
  ;; this, but this is still a weird thing to do, and we should change
  ;; the names to highlight that something weird is going on. Perhaps
  ;; *MAYBE-GC-FUN*, *INTERNAL-ERROR-FUN*, *HANDLE-BREAKPOINT-FUN*,
  ;; and *HANDLE-FUN-END-BREAKPOINT-FUN*...
  (dolist (symbol sb!vm::*c-callable-static-symbols*)
    (cold-set symbol (cold-fdefinition-object (cold-intern symbol))))

  (cold-set 'sb!vm::*current-catch-block*          (make-fixnum-descriptor 0))
  (cold-set 'sb!vm::*current-unwind-protect-block* (make-fixnum-descriptor 0))

  (cold-set '*free-interrupt-context-index* (make-fixnum-descriptor 0))

  (cold-set '*!initial-layouts* (cold-list-all-layouts))

  (/show "dumping packages" (mapcar #'car *cold-package-symbols*))
  (let ((initial-symbols *nil-descriptor*))
    (dolist (cold-package-symbols-entry *cold-package-symbols*)
      (let* ((cold-package (car cold-package-symbols-entry))
             (symbols (cdr cold-package-symbols-entry))
             (shadows (package-shadowing-symbols cold-package))
             (documentation (base-string-to-core (documentation cold-package t)))
             (internal-count 0)
             (external-count 0)
             (internal *nil-descriptor*)
             (external *nil-descriptor*)
             (imported-internal *nil-descriptor*)
             (imported-external *nil-descriptor*)
             (shadowing *nil-descriptor*))
        (declare (type package cold-package)) ; i.e. not a target descriptor
        (/show "dumping" cold-package symbols)

        ;; FIXME: Add assertions here to make sure that inappropriate stuff
        ;; isn't being dumped:
        ;;   * the CL-USER package
        ;;   * the SB-COLD package
        ;;   * any internal symbols in the CL package
        ;;   * basically any package other than CL, KEYWORD, or the packages
        ;;     in package-data-list.lisp-expr
        ;; and that the structure of the KEYWORD package (e.g. whether
        ;; any symbols are internal to it) matches what we want in the
        ;; target SBCL.

        ;; FIXME: It seems possible that by looking at the contents of
        ;; packages in the target SBCL we could find which symbols in
        ;; package-data-lisp.lisp-expr are now obsolete. (If I
        ;; understand correctly, only symbols which actually have
        ;; definitions or which are otherwise referred to actually end
        ;; up in the target packages.)

        (dolist (symbol symbols)
          (let ((handle (car (get symbol 'cold-intern-info)))
                (imported-p (not (eq (symbol-package-for-target-symbol symbol)
                                     cold-package))))
            (multiple-value-bind (found where)
                (find-symbol (symbol-name symbol) cold-package)
              (unless (and where (eq found symbol))
                (error "The symbol ~S is not available in ~S."
                       symbol
                       cold-package))
              (when (memq symbol shadows)
                (cold-push handle shadowing))
              (case where
                (:internal (if imported-p
                               (cold-push handle imported-internal)
                               (progn
                                 (cold-push handle internal)
                                 (incf internal-count))))
                (:external (if imported-p
                               (cold-push handle imported-external)
                               (progn
                                 (cold-push handle external)
                                 (incf external-count))))))))
        (let ((r *nil-descriptor*))
          (cold-push documentation r)
          (cold-push shadowing r)
          (cold-push imported-external r)
          (cold-push imported-internal r)
          (cold-push external r)
          (cold-push internal r)
          (cold-push (make-make-package-args cold-package
                                             internal-count
                                             external-count)
                     r)
          ;; FIXME: It would be more space-efficient to use vectors
          ;; instead of lists here, and space-efficiency here would be
          ;; nice, since it would reduce the peak memory usage in
          ;; genesis and cold init.
          (cold-push r initial-symbols))))
    (cold-set '*!initial-symbols* initial-symbols))

  (cold-set '*!initial-fdefn-objects* (list-all-fdefn-objects))

  (cold-set '*!reversed-cold-toplevels* *current-reversed-cold-toplevels*))

;;; Make a cold list that can be used as the arg list to MAKE-PACKAGE in
;;; order to make a package that is similar to PKG.
(defun make-make-package-args (pkg internal-count external-count)
  (let* ((use *nil-descriptor*)
         (cold-nicknames *nil-descriptor*)
         (res *nil-descriptor*))
    (dolist (u (package-use-list pkg))
      (when (assoc u *cold-package-symbols*)
        (cold-push (base-string-to-core (package-name u)) use)))
    (let* ((pkg-name (package-name pkg))
           ;; Make the package nickname lists for the standard packages
           ;; be the minimum specified by ANSI, regardless of what value
           ;; the cross-compilation host happens to use.
           (warm-nicknames (cond ((string= pkg-name "COMMON-LISP")
                                  '("CL"))
                                 ((string= pkg-name "COMMON-LISP-USER")
                                  '("CL-USER"))
                                 ((string= pkg-name "KEYWORD")
                                  '())
                                 ;; For packages other than the
                                 ;; standard packages, the nickname
                                 ;; list was specified by our package
                                 ;; setup code, not by properties of
                                 ;; what cross-compilation host we
                                 ;; happened to use, and we can just
                                 ;; propagate it into the target.
                                 (t
                                  (package-nicknames pkg)))))
      (dolist (warm-nickname warm-nicknames)
        (cold-push (base-string-to-core warm-nickname) cold-nicknames)))

    ;; INTERNAL-COUNT and EXTERNAL-COUNT are the number of symbols that
    ;; the package contains in the core. We arrange for the package
    ;; symbol tables to be created somewhat larger so that they don't
    ;; need to be rehashed so easily when additional symbols are
    ;; interned during the warm build.
    (cold-push (number-to-core (truncate internal-count 0.8)) res)
    (cold-push (cold-intern :internal-symbols) res)
    (cold-push (number-to-core (truncate external-count 0.8)) res)
    (cold-push (cold-intern :external-symbols) res)

    (cold-push cold-nicknames res)
    (cold-push (cold-intern :nicknames) res)

    (cold-push use res)
    (cold-push (cold-intern :use) res)

    (cold-push (base-string-to-core (package-name pkg)) res)
    res))

;;;; functions and fdefinition objects

;;; a hash table mapping from fdefinition names to descriptors of cold
;;; objects
;;;
;;; Note: Since fdefinition names can be lists like '(SETF FOO), and
;;; we want to have only one entry per name, this must be an 'EQUAL
;;; hash table, not the default 'EQL.
(defvar *cold-fdefn-objects*)

;;; Given a cold representation of a symbol, return a warm
;;; representation.
(defun warm-symbol (des)
  ;; Note that COLD-INTERN is responsible for keeping the
  ;; *COLD-SYMBOLS* table up to date, so if DES happens to refer to an
  ;; uninterned symbol, the code below will fail. But as long as we
  ;; don't need to look up uninterned symbols during bootstrapping,
  ;; that's OK..
  (multiple-value-bind (symbol found-p)
      (gethash des *cold-symbols*)
    (declare (type symbol symbol))
    (unless found-p
      (error "no warm symbol"))
    symbol))

(defun cold-null (des)
  (eq des *nil-descriptor*))

;;; Given a cold representation of a function name, return a warm
;;; representation.
(declaim (ftype (function (sb!vm::cold-object) (or symbol list)) warm-fun-name))
(defun warm-fun-name (des)
  (let ((result
         (etypecase des
           (sb!vm::cold-cons
            ;; Do cold (DESTRUCTURING-BIND (COLD-CAR COLD-CADR) DES ..).
            (let* ((car-des (sb!vm::cold-cons-car des))
                   (cdr-des (sb!vm::cold-cons-cdr des))
                   (cadr-des (sb!vm::cold-cons-car cdr-des))
                   (cddr-des (sb!vm::cold-cons-cdr cdr-des)))
              (aver (cold-null cddr-des))
              (list (warm-symbol car-des)
                    (warm-symbol cadr-des))))
           (sb!vm::cold-symbol
            (aver (not (cold-null des))) ; function named NIL? please no..
            (or (warm-symbol des) (aver nil))))))
    (legal-fun-name-or-type-error result)
    result))

(defun cold-fdefinition-object (cold-name &optional leave-fn-raw)
  (declare (type sb!vm::cold-object cold-name))
  (/show0 "/cold-fdefinition-object")
  (let ((warm-name (warm-fun-name cold-name)))
    #!+sb-show
    (format t "cold-fdefinition-object; warm name: ~A~%" warm-name)
    (or (gethash warm-name *cold-fdefn-objects*)
        (multiple-value-bind (fun raw-addr)
            (if leave-fn-raw
                (values :uninitialized-cold-slot :uninitialized-cold-slot)
                (values *nil-descriptor* "fixme: undefined_tramp"))
          #!+sb-show
          (format t "cold-fdefinition-object; not cached~%" warm-name)
          (setf (gethash warm-name *cold-fdefn-objects*)
                (sb!vm::make-cold-fdefn
                 :name cold-name
                 :fun fun
                 :raw-addr raw-addr))))))

;;; Handle the at-cold-init-time, fset-for-static-linkage operation
;;; requested by FOP-FSET.
(defun static-fset (cold-name defn)
  (declare (type sb!vm::cold-object cold-name))
  (let ((fdefn (cold-fdefinition-object cold-name t)))
    #!+sb-show
    (format t "static-fset ~A ~A ~A~%"
            fdefn
            (sb!vm::cold-fdefn-name fdefn)
            defn)
    (setf (sb!vm::cold-fdefn-fun fdefn) defn)
    (setf (sb!vm::cold-fdefn-raw-addr fdefn)
          (etypecase defn
            (sb!vm::cold-simple-fun
             (/show0 "static-fset (simple-fun)")
             defn)
            (sb!vm::cold-closure
             (/show0 "/static-fset (closure)")
             "fixme: closure-tramp")))
    fdefn))

(defvar *cold-static-funs*)

(defun initialize-static-fns ()
  (let ()
    (dolist (sym sb!vm:*static-funs*)
      (vector-push (cold-fdefinition-object (cold-intern sym))
                   *cold-static-funs*)
      (let ((desired (sb!vm:static-fun-offset sym))
            (offset (length *cold-static-funs*)))
        (unless (= offset desired)
          (error "Offset from FDEFN ~S to ~S is ~W, not ~W."
                 sym nil offset desired))))))

(defun list-all-fdefn-objects ()
  (let ((result *nil-descriptor*))
    (maphash (lambda (key value)
               (declare (ignore key))
               (cold-push value result))
             *cold-fdefn-objects*)
    result))

;;;; fixups and related stuff

;; ;;; *COLD-FOREIGN-SYMBOL-TABLE* becomes *!INITIAL-FOREIGN-SYMBOLS* in
;; ;;; the core. When the core is loaded, !LOADER-COLD-INIT uses this to
;; ;;; create *STATIC-FOREIGN-SYMBOLS*, which the code in
;; ;;; target-load.lisp refers to.
;; (defun foreign-symbols-to-core ()
;;   (let ((result *nil-descriptor*))
;;     (maphash (lambda (symbol value)
;;                (cold-push (cold-cons (base-string-to-core symbol)
;;                                      (number-to-core value))
;;                           result))
;;              *cold-foreign-symbol-table*)
;;     (cold-set (cold-intern 'sb!kernel:*!initial-foreign-symbols*) result))
;;   (let ((result *nil-descriptor*))
;;     (dolist (rtn *cold-assembler-routines*)
;;       (cold-push (cold-cons (cold-intern (car rtn))
;;                             (number-to-core (cdr rtn)))
;;                  result))
;;     (cold-set (cold-intern '*!initial-assembler-routines*) result)))


;;;; general machinery for cold-loading FASL files

;;; FOP functions for cold loading
(defvar *cold-fop-funs*
  ;; We start out with a copy of the ordinary *FOP-FUNS*. The ones
  ;; which aren't appropriate for cold load will be destructively
  ;; modified.
  (copy-seq *fop-funs*))

(defvar *normal-fop-funs*)

;;; Cause a fop to have a special definition for cold load.
;;;
;;; This is similar to DEFINE-FOP, but unlike DEFINE-FOP, this version
;;;   (1) looks up the code for this name (created by a previous
;;        DEFINE-FOP) instead of creating a code, and
;;;   (2) stores its definition in the *COLD-FOP-FUNS* vector,
;;;       instead of storing in the *FOP-FUNS* vector.
(defmacro define-cold-fop ((name &key (pushp t) (stackp t)) &rest forms)
  (aver (member pushp '(nil t)))
  (aver (member stackp '(nil t)))
  (let ((code (get name 'fop-code))
        (fname (symbolicate "COLD-" name)))
    (unless code
      (error "~S is not a defined FOP." name))
    `(progn
       (defun ,fname ()
         ,@(if stackp
               `((with-fop-stack ,pushp ,@forms))
               forms))
       (setf (svref *cold-fop-funs* ,code) #',fname))))

(defmacro clone-cold-fop ((name &key (pushp t) (stackp t))
                          (small-name)
                          &rest forms)
  (aver (member pushp '(nil t)))
  (aver (member stackp '(nil t)))
  `(progn
    (macrolet ((clone-arg () '(read-word-arg)))
      (define-cold-fop (,name :pushp ,pushp :stackp ,stackp) ,@forms))
    (macrolet ((clone-arg () '(read-byte-arg)))
      (define-cold-fop (,small-name :pushp ,pushp :stackp ,stackp) ,@forms))))

;;; Cause a fop to be undefined in cold load.
(defmacro not-cold-fop (name)
  `(define-cold-fop (,name)
     (error "The fop ~S is not supported in cold load." ',name)))

;;; COLD-LOAD loads stuff into the core image being built by calling
;;; LOAD-AS-FASL with the fop function table rebound to a table of cold
;;; loading functions.
(defun cold-load (filename)
  #!+sb-doc
  "Load the file named by FILENAME into the cold load image being built."
  (let* ((*normal-fop-funs* *fop-funs*)
         (*fop-funs* *cold-fop-funs*)
         (*cold-load-filename* (etypecase filename
                                 (string filename)
                                 (pathname (namestring filename)))))
    (with-open-file (s filename :element-type '(unsigned-byte 8))
      (load-as-fasl s nil nil))))

;;;; miscellaneous cold fops

(define-cold-fop (fop-misc-trap) *unbound-marker*)

(define-cold-fop (fop-short-character)
  (make-character-descriptor (read-byte-arg)))

(define-cold-fop (fop-empty-list) *nil-descriptor*)
(define-cold-fop (fop-truth) (cold-intern t))

(define-cold-fop (fop-normal-load :stackp nil)
  (setq *fop-funs* *normal-fop-funs*))

(define-fop (fop-maybe-cold-load 82 :stackp nil)
  (when *cold-load-filename*
    (setq *fop-funs* *cold-fop-funs*)))

(define-cold-fop (fop-maybe-cold-load :stackp nil))

(clone-cold-fop (fop-struct)
                (fop-small-struct)
  (let* ((size (clone-arg))
         (layout (pop-stack))
         (nuntagged
          (descriptor-fixnum
           (cold-get-layout-slot layout 'n-untagged-slots)))
         (ntagged (- size nuntagged))
         (cold-slot-vector (make-array ntagged))
         (result (sb!vm::make-cold-instance :slots cold-slot-vector)))
    (aver (zerop nuntagged))
    (setf (elt cold-slot-vector 0) layout)
    (do ((index 1 (1+ index)))
        ((eql index size))
      (declare (fixnum index))
      (setf (elt cold-slot-vector index)
            (pop-stack)))
    result))

(define-cold-fop (fop-layout)
  (let* ((nuntagged-des (pop-stack))
         (length-des (pop-stack))
         (depthoid-des (pop-stack))
         (cold-inherits (pop-stack))
         (name (pop-stack))
         (old (gethash name *cold-layouts*)))
    (declare (type sb!vm::cold-object length-des depthoid-des cold-inherits))
    (declare (type symbol name))
    ;; If a layout of this name has been defined already
    (if old
      ;; Enforce consistency between the previous definition and the
      ;; current definition, then return the previous definition.
      (destructuring-bind
          ;; FIXME: This would be more maintainable if we used
          ;; DEFSTRUCT (:TYPE LIST) to define COLD-LAYOUT. -- WHN 19990825
          (old-layout-descriptor
           old-name
           old-length
           old-inherits-list
           old-depthoid
           old-nuntagged)
          old
        (declare (type sb!vm::cold-object old-layout-descriptor))
        (declare (type index old-length old-nuntagged))
        (declare (type fixnum old-depthoid))
        (declare (type list old-inherits-list))
        (aver (eq name old-name))
        (let ((length (descriptor-fixnum length-des))
              (inherits-list (listify-cold-inherits cold-inherits))
              (depthoid (descriptor-fixnum depthoid-des))
              (nuntagged (descriptor-fixnum nuntagged-des)))
          (unless (= length old-length)
            (error "cold loading a reference to class ~S when the compile~%~
                    time length was ~S and current length is ~S"
                   name
                   length
                   old-length))
          (unless (equal inherits-list old-inherits-list)
            (error "cold loading a reference to class ~S when the compile~%~
                    time inherits were ~S~%~
                    and current inherits are ~S"
                   name
                   inherits-list
                   old-inherits-list))
          (unless (= depthoid old-depthoid)
            (error "cold loading a reference to class ~S when the compile~%~
                    time inheritance depthoid was ~S and current inheritance~%~
                    depthoid is ~S"
                   name
                   depthoid
                   old-depthoid))
          (unless (= nuntagged old-nuntagged)
            (error "cold loading a reference to class ~S when the compile~%~
                    time number of untagged slots was ~S and is currently ~S"
                   name
                   nuntagged
                   old-nuntagged)))
        old-layout-descriptor)
      ;; Make a new definition from scratch.
      (make-cold-layout name length-des cold-inherits depthoid-des
                        nuntagged-des))))

;;;; cold fops for loading symbols

;;; Load a symbol SIZE characters long from *FASL-INPUT-STREAM* and
;;; intern that symbol in PACKAGE.
(defun cold-load-symbol (size package)
  (let ((string (make-string size)))
    (read-string-as-bytes *fasl-input-stream* string)
    (cold-intern (intern string package))))

(macrolet ((frob (name pname-len package-len)
             `(define-cold-fop (,name)
                (let ((index (read-arg ,package-len)))
                  (push-fop-table
                   (cold-load-symbol (read-arg ,pname-len)
                                     (svref *current-fop-table* index)))))))
  (frob fop-symbol-in-package-save #.sb!vm:n-word-bytes #.sb!vm:n-word-bytes)
  (frob fop-small-symbol-in-package-save 1 #.sb!vm:n-word-bytes)
  (frob fop-symbol-in-byte-package-save #.sb!vm:n-word-bytes 1)
  (frob fop-small-symbol-in-byte-package-save 1 1))

(clone-cold-fop (fop-lisp-symbol-save)
                (fop-lisp-small-symbol-save)
  (push-fop-table (cold-load-symbol (clone-arg) *cl-package*)))

(clone-cold-fop (fop-keyword-symbol-save)
                (fop-keyword-small-symbol-save)
  (push-fop-table (cold-load-symbol (clone-arg) *keyword-package*)))

(clone-cold-fop (fop-uninterned-symbol-save)
                (fop-uninterned-small-symbol-save)
  (let* ((size (clone-arg))
         (name (make-string size)))
    (read-string-as-bytes *fasl-input-stream* name)
    (let ((symbol-des (allocate-symbol name)))
      (push-fop-table symbol-des))))

;;;; cold fops for loading lists

;;; Make a list of the top LENGTH things on the fop stack. The last
;;; cdr of the list is set to LAST.
(defmacro cold-stack-list (length last)
  `(do* ((index ,length (1- index))
         (result ,last (cold-cons (pop-stack) result)))
        ((= index 0) result)
     (declare (fixnum index))))

(define-cold-fop (fop-list)
  (cold-stack-list (read-byte-arg) *nil-descriptor*))
(define-cold-fop (fop-list*)
  (cold-stack-list (read-byte-arg) (pop-stack)))
(define-cold-fop (fop-list-1)
  (cold-stack-list 1 *nil-descriptor*))
(define-cold-fop (fop-list-2)
  (cold-stack-list 2 *nil-descriptor*))
(define-cold-fop (fop-list-3)
  (cold-stack-list 3 *nil-descriptor*))
(define-cold-fop (fop-list-4)
  (cold-stack-list 4 *nil-descriptor*))
(define-cold-fop (fop-list-5)
  (cold-stack-list 5 *nil-descriptor*))
(define-cold-fop (fop-list-6)
  (cold-stack-list 6 *nil-descriptor*))
(define-cold-fop (fop-list-7)
  (cold-stack-list 7 *nil-descriptor*))
(define-cold-fop (fop-list-8)
  (cold-stack-list 8 *nil-descriptor*))
(define-cold-fop (fop-list*-1)
  (cold-stack-list 1 (pop-stack)))
(define-cold-fop (fop-list*-2)
  (cold-stack-list 2 (pop-stack)))
(define-cold-fop (fop-list*-3)
  (cold-stack-list 3 (pop-stack)))
(define-cold-fop (fop-list*-4)
  (cold-stack-list 4 (pop-stack)))
(define-cold-fop (fop-list*-5)
  (cold-stack-list 5 (pop-stack)))
(define-cold-fop (fop-list*-6)
  (cold-stack-list 6 (pop-stack)))
(define-cold-fop (fop-list*-7)
  (cold-stack-list 7 (pop-stack)))
(define-cold-fop (fop-list*-8)
  (cold-stack-list 8 (pop-stack)))

;;;; cold fops for loading vectors

(clone-cold-fop (fop-base-string)
                (fop-small-base-string)
  (let* ((len (clone-arg))
         (string (make-string len)))
    (read-string-as-bytes *fasl-input-stream* string)
    (base-string-to-core string)))

#!+sb-unicode
(clone-cold-fop (fop-character-string)
                (fop-small-character-string)
  (bug "CHARACTER-STRING dumped by cross-compiler."))

(clone-cold-fop (fop-vector)
                (fop-small-vector)
  (let* ((size (clone-arg))
         (data (make-array size))
         (result
          (make-cold-vector sb!vm:simple-vector-widetag data)))
    (do ((index (1- size) (1- index)))
        ((minusp index))
      (declare (fixnum index))
      (setf (elt data index) (pop-stack)))
    result))

;;; FIXME: this VOP cheats using SB-SYS:READ-N-BYTES.
;;; As a result, we can't bootstrap from non-SBCL currently.
;;;
(define-cold-fop (fop-int-vector)
  (let* ((len (read-word-arg))
         (sizebits (read-byte-arg))
         (widetag (case sizebits
                    (0 sb!vm:simple-array-nil-widetag)
                    (1 sb!vm:simple-bit-vector-widetag)
                    (2 sb!vm:simple-array-unsigned-byte-2-widetag)
                    (4 sb!vm:simple-array-unsigned-byte-4-widetag)
                    (7 (prog1 sb!vm:simple-array-unsigned-byte-7-widetag
                         ;; (setf sizebits 8)
                         ))
                    (8 sb!vm:simple-array-unsigned-byte-8-widetag)
                    (15 (prog1 sb!vm:simple-array-unsigned-byte-15-widetag
                          ;; (setf sizebits 16)
                          ))
                    (16 sb!vm:simple-array-unsigned-byte-16-widetag)
                    (31 (prog1 sb!vm:simple-array-unsigned-byte-31-widetag
                          ;; (setf sizebits 32)
                          ))
                    (32 sb!vm:simple-array-unsigned-byte-32-widetag)
                    #!+#.(cl:if (cl:= 64 sb!vm:n-word-bits) '(and) '(or))
                    (63 (prog1 sb!vm:simple-array-unsigned-byte-63-widetag
                          ;; (setf sizebits 64)
                          ))
                    #!+#.(cl:if (cl:= 64 sb!vm:n-word-bits) '(and) '(or))
                    (64 sb!vm:simple-array-unsigned-byte-64-widetag)
                    (t (error "losing element size: ~W" sizebits))))
         (res (case sizebits
                (0 (make-array len :element-type 'nil))
                (1 (make-array len :element-type 'bit))
                (2 (make-array len :element-type '(unsigned-byte 2)))
                (4 (make-array len :element-type '(unsigned-byte 4)))
                (7 (prog1 (make-array len :element-type '(unsigned-byte 7))
                     (setf sizebits 8)))
                (8 (make-array len :element-type '(unsigned-byte 8)))
                (15 (prog1 (make-array len :element-type '(unsigned-byte 15))
                      (setf sizebits 16)))
                (16 (make-array len :element-type '(unsigned-byte 16)))
                (31 (prog1 (make-array len :element-type '(unsigned-byte 31))
                      (setf sizebits 32)))
                (32 (make-array len :element-type '(unsigned-byte 32)))
                #!+#.(cl:if (cl:= 64 sb!vm:n-word-bits) '(and) '(or))
                (63 (prog1 (make-array len :element-type '(unsigned-byte 63))
                      (setf sizebits 64)))
                (64 (make-array len :element-type '(unsigned-byte 64)))
                (t (error "losing i-vector element size: ~S" sizebits)))))
    (sb-sys:read-n-bytes
     *fasl-input-stream*
     res
     0
     (ceiling (the index (* sizebits len)) sb!vm:n-byte-bits))
    (make-cold-vector widetag res)))

;;; FIXME: this VOP cheats using SB-SYS:READ-N-BYTES.
;;; As a result, we can't bootstrap from non-SBCL currently.
;;;
(define-cold-fop (fop-single-float-vector)
  (let* ((length (read-word-arg))
         (res (make-array length :element-type 'single-float)))
    (sb-sys:read-n-bytes *fasl-input-stream* res 0 (* length 4))
    (make-cold-vector sb!vm:simple-array-single-float-widetag res)))

(not-cold-fop fop-double-float-vector)
#!+long-float (not-cold-fop fop-long-float-vector)
(not-cold-fop fop-complex-single-float-vector)
(not-cold-fop fop-complex-double-float-vector)
#!+long-float (not-cold-fop fop-complex-long-float-vector)

(define-cold-fop (fop-array)
  (let* ((rank (read-word-arg))
         (dimensions (make-array rank))
         (data-vector (pop-stack))
         (total-elements 1))
    (dotimes (axis rank)
      (let ((dim (pop-stack)))
        (setf total-elements (* total-elements (descriptor-fixnum dim)))
        (setf (elt dimensions axis) dim)))
    (sb!vm::make-cold-array :fill-pointer *nil-descriptor*
                            :fill-pointer-p *nil-descriptor*
                            :elements (make-fixnum-descriptor total-elements)
                            :data data-vector
                            :displacement *nil-descriptor*
                            :dimensions dimensions)))


;;;; cold fops for loading numbers

(defmacro define-cold-number-fop (fop)
  `(define-cold-fop (,fop :stackp nil)
     ;; Invoke the ordinary warm version of this fop to push the
     ;; number.
     (,fop)
     ;; Replace the warm fop result with the cold image of the warm
     ;; fop result.
     (with-fop-stack t
       (let ((number (pop-stack)))
         (number-to-core number)))))

(define-cold-number-fop fop-single-float)
(define-cold-number-fop fop-double-float)
(define-cold-number-fop fop-integer)
(define-cold-number-fop fop-small-integer)
(define-cold-number-fop fop-word-integer)
(define-cold-number-fop fop-byte-integer)
(define-cold-number-fop fop-complex-single-float)
(define-cold-number-fop fop-complex-double-float)

(define-cold-fop (fop-ratio)
  (let ((den (pop-stack)))
    (sb!vm::make-cold-ratio :numerator (pop-stack)
                            :denominator den)))

(define-cold-fop (fop-complex)
  (let ((im (pop-stack)))
    (sb!vm::make-cold-complex :real (pop-stack) :imag im)))

;;;; cold fops for calling (or not calling)

(not-cold-fop fop-eval)
(not-cold-fop fop-eval-for-effect)

(defvar *load-time-value-counter*)

(define-cold-fop (fop-funcall)
  (unless (= (read-byte-arg) 0)
    (error "You can't FOP-FUNCALL arbitrary stuff in cold load."))
  (let ((counter *load-time-value-counter*))
    (cold-push (cold-cons
                (cold-intern :load-time-value)
                (cold-cons
                 (pop-stack)
                 (cold-cons
                  (number-to-core counter)
                  *nil-descriptor*)))
               *current-reversed-cold-toplevels*)
    (setf *load-time-value-counter* (1+ counter))
    "oops, fop-funcall result"))

(defun finalize-load-time-value-noise ()
  (cold-set (cold-intern '*!load-time-values*)
            (make-cold-vector sb!vm:simple-vector-widetag
                              (make-array *load-time-value-counter*))))

(define-cold-fop (fop-funcall-for-effect :pushp nil)
  (if (= (read-byte-arg) 0)
      (cold-push (pop-stack)
                 *current-reversed-cold-toplevels*)
      (error "You can't FOP-FUNCALL arbitrary stuff in cold load.")))

;;;; cold fops for fixing up circularities

(define-cold-fop (fop-rplaca :pushp nil)
  (let ((obj (svref *current-fop-table* (read-word-arg)))
        (idx (read-word-arg)))
    (setf (sb!vm::cold-cons-car (cold-nthcdr idx obj)) (pop-stack))))

(define-cold-fop (fop-rplacd :pushp nil)
  (let ((obj (svref *current-fop-table* (read-word-arg)))
        (idx (read-word-arg)))
    (setf (sb!vm::cold-cons-cdr (cold-nthcdr idx obj)) (pop-stack))))

(define-cold-fop (fop-svset :pushp nil)
    (let ((obj (svref *current-fop-table* (read-word-arg)))
        (idx (read-word-arg))
        (newval (pop-stack)))
    (setf (elt (etypecase obj
                 (sb!vm::cold-vector (sb!vm::cold-vector-data obj)))
               idx)
          newval)))

(define-cold-fop (fop-structset :pushp nil)
  (let ((obj (svref *current-fop-table* (read-word-arg)))
        (idx (read-word-arg))
        (newval (pop-stack)))
    (setf (elt (etypecase obj
                 (sb!vm::cold-instance (sb!vm::cold-instance-slots obj)))
               (1+ idx))
          newval)))

;;; In the original CMUCL code, this actually explicitly declared PUSHP
;;; to be T, even though that's what it defaults to in DEFINE-COLD-FOP.
(define-cold-fop (fop-nthcdr)
  (cold-nthcdr (read-word-arg) (pop-stack)))

(defun cold-nthcdr (index obj)
  (dotimes (i index)
    (setq obj (sb!vm::cold-cons-cdr obj)))
  obj)

;;;; cold fops for loading code objects and functions

;;; the names of things which have had COLD-FSET used on them already
;;; (used to make sure that we don't try to statically link a name to
;;; more than one definition)
(defparameter *cold-fset-warm-names*
  ;; This can't be an EQL hash table because names can be conses, e.g.
  ;; (SETF CAR).
  (make-hash-table :test 'equal))

(define-cold-fop (fop-fset :pushp nil)
  (let* ((fn (pop-stack))
         (cold-name (pop-stack))
         (warm-name (warm-fun-name cold-name)))
    (if (gethash warm-name *cold-fset-warm-names*)
        (error "duplicate COLD-FSET for ~S" warm-name)
        (setf (gethash warm-name *cold-fset-warm-names*) t))
    (static-fset cold-name fn)))

(define-cold-fop (fop-fdefinition)
  (cold-fdefinition-object (pop-stack)))

(define-cold-fop (fop-sanctify-for-execution)
  (pop-stack))

;;; Setting this variable shows what code looks like before any
;;; fixups (or function headers) are applied.
#!+sb-show (defvar *show-pre-fixup-code-p* nil)

#!+avm2
(defvar *code-component-counter* nil)

;;; FIXME: The logic here should be converted into a function
;;; COLD-CODE-FOP-GUTS (NCONST CODE-SIZE) called by DEFINE-COLD-FOP
;;; FOP-CODE and DEFINE-COLD-FOP FOP-SMALL-CODE, so that
;;; variable-capture nastiness like (LET ((NCONST ,NCONST) ..) ..)
;;; doesn't keep me awake at night.
(defun cold-code-fop-guts (nconst code-size pop-stack
                           local-count
                           max-stack
                           init-scope-depth
                           max-scope-depth)
  (let* ((header-n-words (+ sb!vm:code-trace-table-offset-slot nconst))
         (constants (make-array header-n-words))
         (opcodes (make-array code-size :element-type '(unsigned-byte 8)))
         (des (sb!vm::make-cold-code
               :widetag sb!vm:code-header-widetag
               :code-size (make-fixnum-descriptor
                           (ash (+ code-size (1- (ash 1 sb!vm:word-shift)))
                                (- sb!vm:word-shift)))
               :entry-points *nil-descriptor*
               :debug-info (funcall pop-stack)
               :constants constants
               :opcodes opcodes
               :trace-table-offset *nil-descriptor*)))
    (do ((index (1- header-n-words) (1- index)))
        ((< index sb!vm:code-trace-table-offset-slot))
      (setf (elt constants index) (funcall pop-stack)))
    (read-sequence opcodes *fasl-input-stream*)
    (when *code-component-counter*
      (push
       (list '(:qname "common-lisp-user" "codeComponent")
             0                          ;name in method struct?
             (loop repeat 3 collect 0)  ;arg types, 0 = t/*/any
             0                          ;return type, 0 = any
             0                          ;restp, 0 = no
             (make-instance 'avm2-asm::method-body
                            'avm2-asm::local-count local-count
                            'avm2-asm::max-stack max-stack
                            'avm2-asm::init-scope-depth init-scope-depth
                            'avm2-asm::max-scope-depth max-scope-depth
                            'avm2-asm::code opcodes))
       (gethash (format nil "codeComponent~D" *code-component-counter*)
                (avm2-compiler::functions avm2-compiler::*symbol-table*)
                (list))))
    #!+sb-show
    (when *show-pre-fixup-code-p*
      (format *trace-output*
              "~&/raw code from code-fop ~W ~W: ~X~%"
              nconst
              code-size
              opcodes))
    des))

(defmacro define-cold-code-fop (name nconst code-size)
  `(define-cold-fop (,name)
     (cold-code-fop-guts ,nconst ,code-size (lambda () (pop-stack))
                         (read-word-arg)
                         (read-word-arg)
                         (read-word-arg)
                         (read-word-arg))))

(define-cold-code-fop fop-code (read-word-arg) (read-word-arg))

(define-cold-code-fop fop-small-code (read-byte-arg) (read-halfword-arg))

(clone-cold-fop (fop-alter-code :pushp nil)
                (fop-byte-alter-code)
  (let ((slot (- (clone-arg)
                 sb!vm:code-constants-offset))
        (value (pop-stack))
        (code (pop-stack)))
    (setf (elt (sb!vm::cold-code-constants code) slot) value)))

(define-cold-fop (fop-fun-entry)
  (let* ((xrefs (pop-stack))
         (type (pop-stack))
         (arglist (pop-stack))
         (name (pop-stack))
         (code-object (pop-stack))
         (offset (read-word-arg))
         (next (sb!vm::cold-code-entry-points code-object))
         (fn (sb!vm::make-cold-simple-fun
              :xep-hack offset
              :next next
              :self :dummy
              :name name
              :arglist arglist
              :type type
              :xrefs xrefs
              :code-component code-object
              ;; there is no code slot, but let's make objdef.lisp happy:
              :code *nil-descriptor*)))
    (setf (sb!vm::cold-simple-fun-self fn) fn)
    (setf (sb!vm::cold-code-entry-points code-object) fn)
    fn))

(define-cold-fop (fop-foreign-fixup)
  (let* ((kind (pop-stack))
         (code-object (pop-stack))
         (len (read-byte-arg))
         (sym (make-string len)))
    (read-string-as-bytes *fasl-input-stream* sym)
    (warn "ignoring ~A fop-foreign-fixup in ~A for ~A" kind code-object sym)
    (let ((offset (read-word-arg))))
   code-object))

;; #!+linkage-table
;; (define-cold-fop (fop-foreign-dataref-fixup)
;;   (let* ((kind (pop-stack))
;;          (code-object (pop-stack))
;;          (len (read-byte-arg))
;;          (sym (make-string len)))
;;     (read-string-as-bytes *fasl-input-stream* sym)
;;     (maphash (lambda (k v)
;;                (format *error-output* "~&~S = #X~8X~%" k v))
;;              *cold-foreign-symbol-table*)
;;     (error "shared foreign symbol in cold load: ~S (~S)" sym kind)))

(define-cold-fop (fop-assembler-code)
  (let ((length (read-word-arg)))
    (aver (zerop length))))

(define-cold-fop (fop-assembler-routine)
  (let* ((routine (pop-stack))
         (des (pop-stack))
         (offset (read-word-arg)))
    (error "fop-assembler-routine ~A ~A ~A" routine des offset)))

(define-cold-fop (fop-assembler-fixup)
  (let* ((routine (pop-stack))
         (kind (pop-stack))
         (code-object (pop-stack))
         (offset (read-word-arg)))
    (error "fop-assembler-fixup ~A ~A ~A ~A"
           routine code-object offset kind)))

(define-cold-fop (fop-code-object-fixup)
  (let* ((kind (pop-stack))
         (code-object (pop-stack))
         (offset (read-word-arg)))
    (error "fop-code-object-fixup ~A ~A ~A"
           code-object offset kind)
    code-object))

(defmethod print-object ((object sb!vm::cold-object) stream)
  ;; avoid gigantic structure instance output issues
  (print-unreadable-object (object stream :type t :identity t)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +int32+ 1)
  (defconstant +uint32+ 2)
  (defconstant +double-float+ 3)
  (defconstant +string+ 4)
  (defconstant +array+ 5)
  (defconstant +byte-array+ 6)
  (defconstant +object+ 7))

(defstruct (xmf-output-stream
             (:constructor make-xmf-output-stream (target objects))
             (:conc-name "XMF-"))
  (target)
  (objects))

(defun %put-byte (i s)
  (write-byte i (xmf-target s))
  i)

(defun %put-bytes (bytes s)
  (write-sequence bytes (xmf-target s)))

(defun put-object-1 (data s)
  (funcall (etypecase data
             ((integer 0 #.(1- (expt 2 32))) 'put-uint32)
             ((integer #.(- (expt 2 31)) #.(1- (expt 2 31))) 'put-int32)
             (double-float 'put-xmf-double-float)
             (string 'put-xmf-string)
             ((array (unsigned-byte 8) (*)) 'put-xmf-byte-array)
             (vector 'put-xmf-array-1)
             (sb!vm::cold-object 'put-cold-object-1))
           data
           s))

(defun put-object-2 (data s)
  ;; for debugging purposes, insert a magic byte that we can check for:
  (%put-byte 42 s)
  (typecase data
    ((or string (array (unsigned-byte 8) (*))))
    (vector (put-xmf-array-2 data s))
    (sb!vm::cold-object (put-cold-object-2 data s))))

(defun avm2name (sym)
  (substitute #\_ #\- (string-downcase sym)))

(defun put-cold-object-1 (object s)
  (%put-byte +object+ s)
  (%put-byte (position (sb!vm::cold-class-to-primitive-object
                        (class-of object))
                       sb!vm:*primitive-objects*)
             s))

(defun put-cold-object-2 (object s)
  (let* ((primitive-object
          (sb!vm::cold-class-to-primitive-object (class-of object)))
         (slots
          (sb!vm::primitive-object-slots primitive-object)))
    (dolist (slot slots)
      (let ((slot-value (funcall (sb!vm::slot-reader slot) object)))
        (%put-int32 (gethash slot-value (xmf-objects s)) s)))))

(defun put-xmf-double-float (data s)
  (%put-byte +double-float+ s)
  (let ((hi (sb-kernel:double-float-high-bits data))
        (lo (sb-kernel:double-float-low-bits data)))
    (%put-byte (ldb (byte 8 24) hi) s)
    (%put-byte (ldb (byte 8 16) hi) s)
    (%put-byte (ldb (byte 8 08) hi) s)
    (%put-byte (ldb (byte 8 00) hi) s)
    (%put-byte (ldb (byte 8 24) lo) s)
    (%put-byte (ldb (byte 8 16) lo) s)
    (%put-byte (ldb (byte 8 08) lo) s)
    (%put-byte (ldb (byte 8 00) lo) s)))

(defun %put-int32 (data s)
  (%put-byte (ldb (byte 8 24) data) s)
  (%put-byte (ldb (byte 8 16) data) s)
  (%put-byte (ldb (byte 8 08) data) s)
  (%put-byte (ldb (byte 8 00) data) s))

(defun put-uint32 (data s)
  (%put-byte +uint32+ s)
  (%put-int32 data s))

(defun put-int32 (data s)
  (%put-byte +int32+ s)
  (%put-int32 data s))

(defun %put-string-bytes (bytes s)
  (%put-int32 (length bytes) s)
  (%put-bytes bytes s))

(defun %put-string (str s)
  (%put-string-bytes (sb-ext:string-to-octets str :external-format :utf-8) s))

(defun put-xmf-string (data s)
  (%put-byte +string+ s)
  (%put-string data s))

(defun put-xmf-array-1 (data s)
  (%put-byte +array+ s)
  (%put-int32 (length data) s))

(defun put-xmf-byte-array (data s)
  (%put-byte +byte-array+ s)
  (%put-int32 (length data) s)
  (write-sequence data (xmf-target s)))

(defun put-xmf-array-2 (data s)
  (dotimes (i (length data))
    (%put-int32 (gethash (elt data i) (xmf-objects s)) s)))

(defun list-cold-object-slots (object)
  (let* ((primitive-object
          (sb!vm::cold-class-to-primitive-object (class-of object)))
         (slots
          (sb!vm::primitive-object-slots primitive-object)))
    (mapcar (lambda (slot)
              (funcall (sb!vm::slot-reader slot) object))
            slots)))

(defun list-all-cold-objects (graph)
  (let* ((head (list :head graph))
         (pointer head)
         (seen (make-hash-table))
         (nraw 0)
         (narrays 0)
         (narrayelements 0)
         (nbytearrays 0)
         (nbytearraybytes 0)
         (n-objects-by-type (make-hash-table)))
    (setf (gethash graph seen) 0)
    (do ()
        ((null (cdr pointer)))
      (pop pointer)
      (let ((next (car pointer)))
        (flet ((collect (y)
                 (unless (gethash y seen)
                   (setf (gethash y seen) t)
                   (push y (cdr pointer)))))
          (etypecase next
            ((or integer double-float string)
             (incf nraw)
             (collect next))
            ((array (unsigned-byte 8) (*))
             (incf nbytearrays)
             (incf nbytearraybytes (length next)))
            (vector
             (incf narrays)
             (incf narrayelements (length next))
             (loop for x across next do (collect x)))
            (sb!vm::cold-object
             (incf (gethash (type-of next) n-objects-by-type 0))
             (loop for x in (list-cold-object-slots next) do
                  (collect x)))))))
    (format t "  nobjects~14Ttype~%")
    (format t "----------------------------------------~%")
    (format t "~12D  +array+~%~12D  ... (total elements in +array+s)~%~
               ~12D  +byte-array+~%~12D  ... (total bytes in +byte-array+s)~%~
               ~12D  raw strings, integers, floats~%"
            narrays narrayelements nbytearrays nbytearraybytes nraw)
    (maphash (lambda (k v) (format t "~12D  ~A~%" v k))
             n-objects-by-type)
    (cdr head)))

(declaim (ftype (function (string sb!vm::cold-object sequence sequence))
                write-initial-core-file))
(defun write-initial-core-file
    (filename initial-fun static-symbols static-funs swf-bytes)
  (let ((filenamestring (namestring filename))
        (flat-objects
         (list-all-cold-objects
          (vector
           ;; fixme: might want to add these here:
           ;;   sbcl-core-version-integer
           ;;   build-id
           initial-fun
           static-symbols
           static-funs
           swf-bytes)))
        (table (make-hash-table)))
    (loop
       for i from 0
       for o in flat-objects
       do (setf (gethash o table) i))
    (format t
            "[building initial core file in ~S: ~%"
            filenamestring)
    (force-output)
    (with-open-file (s filenamestring
                       :direction :output
                       :element-type '(unsigned-byte 8)
                       :if-exists :rename-and-delete)
      (let ((r (make-xmf-output-stream s table)))
        (%put-int32 (length sb!vm:*primitive-objects*) r)
        (dolist (primitive-object sb!vm:*primitive-objects*)
          (%put-string
           (format nil "org.sbcl::~A"
                   (avm2name
                    (sb!vm::primitive-object-name primitive-object)))
           r))
        (%put-int32 (length flat-objects) r)
        (dolist (object flat-objects)
          (put-object-1 object r))
        (dolist (object flat-objects)
          (put-object-2 object r)))))
  (format t "done]~%")
  (force-output)
  (/show "leaving WRITE-INITIAL-CORE-FILE")
  (values))

(defun write-primitive-object (obj)
  ;; writing primitive object layouts
  (format t "package org.sbcl {~%  ~
               public class ~A extends LispObject {~%"
          (avm2name (sb!vm:primitive-object-name obj)))
  (dolist (slot (sb!vm:primitive-object-slots obj))
    (format t "    public var _~A : Object;~%"
            (avm2name (sb!vm:slot-name slot))))
  (format t "    override public function initFromStream(s : XMFReader) ~
             : void {~%")
  (dolist (slot (sb!vm:primitive-object-slots obj))
    (format t "      _~A = s.readNextSlot();~%"
            (avm2name (sb!vm:slot-name slot))))
  (format t "    }~%")
  (format t "  }~%")
  (format t "}~%"))

;;;; the actual GENESIS function

;;; Read the FASL files in OBJECT-FILE-NAMES and produce a Lisp core,
;;; and/or information about a Lisp core, therefrom.
;;;
;;; input file arguments:
;;;   SYMBOL-TABLE-FILE-NAME names a UNIX-style .nm file *with* *any*
;;;     *tab* *characters* *converted* *to* *spaces*. (We push
;;;     responsibility for removing tabs out to the caller it's
;;;     trivial to remove them using UNIX command line tools like
;;;     sed, whereas it's a headache to do it portably in Lisp because
;;;     #\TAB is not a STANDARD-CHAR.) If this file is not supplied,
;;;     a core file cannot be built (but a C header file can be).
;;;
;;; output files arguments (any of which may be NIL to suppress output):
;;;   CORE-FILE-NAME gets a Lisp core.
;;;   C-HEADER-FILE-NAME gets a C header file, traditionally called
;;;     internals.h, which is used by the C compiler when constructing
;;;     the executable which will load the core.
;;;   MAP-FILE-NAME gets (?) a map file. (dunno about this -- WHN 19990815)
;;;
;;; FIXME: GENESIS doesn't belong in SB!VM. Perhaps in %KERNEL for now,
;;; perhaps eventually in SB-LD or SB-BOOT.
(defun sb!vm:genesis (&key object-file-names
                           core-file-name
                           map-file-name
                           generated-classes-dir-name)
  (setf generated-classes-dir-name
        "src/runtime/vmruntime/org/sbcl")
  (format t "~&beginning GENESIS, creating core ~S~%" core-file-name)
  (let (;; don't attempt the pprint the entire cold-object graph...:
        (*print-level* 1))
    ;; Now that we've successfully read our only input file (by
    ;; loading the symbol table, if any), it's a good time to ensure
    ;; that there'll be someplace for our output files to go when
    ;; we're done.
    (flet ((frob (filename)
             (when filename
               (ensure-directories-exist filename :verbose t))))
      (frob core-file-name)
      (frob map-file-name))

    ;; (This shouldn't matter in normal use, since GENESIS normally
    ;; only runs once in any given Lisp image, but it could reduce
    ;; confusion if we ever experiment with running, tweaking, and
    ;; rerunning genesis interactively.)
    (do-all-symbols (sym)
      (remprop sym 'cold-intern-info))

    (let* ((*foreign-symbol-placeholder-value* (if core-file-name nil 0))
           (*load-time-value-counter* 0)
           (*cold-fdefn-objects* (make-hash-table :test 'equal))
           (*cold-symbols* (make-hash-table))
           (*cold-static-symbols*
            (make-array (length sb!vm:*static-symbols*)
                        :fill-pointer 0
                        :initial-element :missing-static-symbol))
           (*cold-static-funs*
            (make-array (length sb!vm:*static-funs*)
                        :fill-pointer 0
                        :initial-element :missing-static-fun))
           (*cold-package-symbols* nil)
           (*nil-descriptor* (make-nil-descriptor))
           (*current-reversed-cold-toplevels* *nil-descriptor*)
           (*unbound-marker* (make-unbound-marker-descriptor))
           ;; *cold-assembler-fixups*
           ;; *cold-assembler-routines*
           )

      ;; Prepare for cold load.
      (initialize-non-nil-symbols)
      (initialize-layouts)
      (initialize-static-fns)

      ;; Initialize the *COLD-SYMBOLS* system with the information
      ;; from package-data-list.lisp-expr and
      ;; common-lisp-exports.lisp-expr.
      ;;
      ;; Why do things this way? Historically, the *COLD-SYMBOLS*
      ;; machinery was designed and implemented in CMU CL long before
      ;; I (WHN) ever heard of CMU CL. It dumped symbols and packages
      ;; iff they were used in the cold image. When I added the
      ;; package-data-list.lisp-expr mechanism, the idea was to
      ;; centralize all information about packages and exports. Thus,
      ;; it was the natural place for information even about packages
      ;; (such as SB!PCL and SB!WALKER) which aren't used much until
      ;; after cold load. This didn't quite match the CMU CL approach
      ;; of filling *COLD-SYMBOLS* with symbols which appear in the
      ;; cold image and then dumping only those symbols. By explicitly
      ;; putting all the symbols from package-data-list.lisp-expr and
      ;; from common-lisp-exports.lisp-expr into *COLD-SYMBOLS* here,
      ;; we feed our centralized symbol information into the old CMU
      ;; CL code without having to change the old CMU CL code too
      ;; much. (And the old CMU CL code is still useful for making
      ;; sure that the appropriate keywords and internal symbols end
      ;; up interned in the target Lisp, which is good, e.g. in order
      ;; to make &KEY arguments work right and in order to make
      ;; BACKTRACEs into target Lisp system code be legible.)
      (dolist (exported-name
               (sb-cold:read-from-file "common-lisp-exports.lisp-expr"))
        (cold-intern (intern exported-name *cl-package*)))
      (dolist (pd (sb-cold:read-from-file "package-data-list.lisp-expr"))
        (declare (type sb-cold:package-data pd))
        (let ((package (find-package (sb-cold:package-data-name pd))))
          (labels (;; Call FN on every node of the TREE.
                   (mapc-on-tree (fn tree)
                                 (declare (type function fn))
                                 (typecase tree
                                   (cons (mapc-on-tree fn (car tree))
                                         (mapc-on-tree fn (cdr tree)))
                                   (t (funcall fn tree)
                                      (values))))
                   ;; Make sure that information about the association
                   ;; between PACKAGE and the symbol named NAME gets
                   ;; recorded in the cold-intern system or (as a
                   ;; convenience when dealing with the tree structure
                   ;; allowed in the PACKAGE-DATA-EXPORTS slot) do
                   ;; nothing if NAME is NIL.
                   (chill (name)
                     (when name
                       (cold-intern (intern name package) package))))
            (mapc-on-tree #'chill (sb-cold:package-data-export pd))
            (mapc #'chill (sb-cold:package-data-reexport pd))
            (dolist (sublist (sb-cold:package-data-import-from pd))
              (destructuring-bind (package-name &rest symbol-names) sublist
                (declare (ignore package-name))
                (mapc #'chill symbol-names))))))

      ;; Cold load.
      (let (swf-bytes)
        (flet ((load-object-files ()
                 (dolist (file-name object-file-names)
                   (write-line (namestring file-name))
                   (cold-load file-name))))
          (if core-file-name
              (let ((*code-component-counter* 0)
                    (swf-file-name "output/cold-sbcl.swf"))
                (with-open-file (s swf-file-name
                                   :direction :io
                                   :if-exists :rename-and-delete
                                   :element-type '(unsigned-byte 8))
                  (avm2-compiler::with-compilation-to-stream s
                      ("frame1" `((0 "codeComponentHelper")) :swf-version 10)
                    (avm2-compiler::def-swf-class :code-component-helper
                        "testns"
                      %flash:Object
                      ()
                      (()
                       ;;
                       ))
                    (load-object-files))
                  ;; Kludge: Here we are writing to a temporary file,
                  ;; although we actually need the data as a byte array
                  ;; in memory only.  Flexi-streams would help, if
                  ;; FILE-POSITION was implemented.
                  (file-position s 0)
                  (setf swf-bytes
                        (make-array (file-length s)
                                    :element-type '(unsigned-byte 8)))
                  (read-sequence swf-bytes s)))
              (load-object-files)))

        ;; Tidy up loose ends left by cold loading. ("Postpare from cold load?")
        ;;       (resolve-assembler-fixups)
        ;;       (foreign-symbols-to-core)
        (finish-symbols)
        (/show "back from FINISH-SYMBOLS")
        (finalize-load-time-value-noise)

        ;; KLUDGE: make the diff in make-genesis-2.sh happy by setting up
        ;; the directories it is looking for.  Currently our output actually
        ;; goes to src/runtime/vmruntime/org/sbcl.
        (ensure-directories-exist "src/runtime/genesis/")
        (ensure-directories-exist "output/genesis-2/")

        (when generated-classes-dir-name
          ;; Write results to files.
          ;;
          ;; FIXME: I dislike this approach of redefining
          ;; *STANDARD-OUTPUT* instead of putting the new stream in a
          ;; lexical variable, and it's annoying to have WRITE-MAP (to
          ;; *STANDARD-OUTPUT*) not be parallel to WRITE-INITIAL-CORE-FILE
          ;; (to a stream explicitly passed as an argument).
          (macrolet ((out-to (name &body body)
                       `(let ((fn (format nil "~A/~A.as"
                                          generated-classes-dir-name
                                          ,name)))
                          (ensure-directories-exist fn)
                          (with-open-file (*standard-output*
                                           fn
                                           :if-exists :supersede
                                           :direction :output)
                            ,@body))))

            (let ((structs (sort (copy-list sb!vm:*primitive-objects*) #'string<
                                 :key (lambda (obj)
                                        (symbol-name
                                         (sb!vm:primitive-object-name obj))))))
              (dolist (obj structs)
                (out-to
                 (string-downcase (avm2name (sb!vm:primitive-object-name obj)))
                 (write-primitive-object obj))))))
        (when core-file-name
          (let* ((cold-name (cold-intern '!cold-init))
                 (cold-fdefn (cold-fdefinition-object cold-name))
                 (initial-fun (sb!vm::cold-fdefn-fun cold-fdefn)))
            (format t "initial fdefn, fun: ~A, ~A~%" cold-fdefn initial-fun)
            (aver (not (eq initial-fun *nil-descriptor*)))
            (format t "swf size: ~D~%" (length swf-bytes))
            (write-initial-core-file core-file-name
                                     initial-fun
                                     *cold-static-symbols*
                                     *cold-static-funs*
                                     swf-bytes)))))))
