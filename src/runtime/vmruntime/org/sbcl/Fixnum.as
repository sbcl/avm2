// -*- mode: c; c-basic-offset: 4 -*-

package org.sbcl {
    [Bindable]
    [RemoteClass(alias="fixnum")]
    public class Fixnum {
	public var value : int;
    }
}
