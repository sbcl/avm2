// -*- mode: c; c-basic-offset: 4 -*-

package org.sbcl {
    import flash.filesystem.File;
    import flash.filesystem.FileStream;
    import flash.filesystem.FileMode;
    import flash.utils.getTimer;
    import flash.utils.getTimer;
    import flash.desktop.NativeApplication;

    import flash.system.ApplicationDomain;
    import flash.utils.ByteArray;
    import flash.system.LoaderContext;
    import flash.system.Security;
    import flash.system.SecurityDomain;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.SecurityErrorEvent;

    public class Runtime {
	public function vectorAsCharacters(x : vector) : String {
	    // fixme: StringBuilder?
	    var data : Array = x._data as Array;
	    var chars : String = "";
	    for (var i : uint = 0; i < data.length; i++)
		chars += String.fromCharCode(data[i]);
	    return chars;
	}

	public function writeToString(x : Object, escape : Boolean) : String {
	    try {
		if (x is symbol) {
		    var _package : instance = instance(x._package);
		    var packageName : vector = vector(_package._slots[1]);
		    var short : String = ""
			+ vectorAsCharacters(packageName)
			+ ":"
			+ vectorAsCharacters(vector(x._name));
		    if (escape)
			return "#<symbol " + short + ">";
		    return short;
		}
		if (x is vector)
		    return "#<vector of length " + x._length + ">";
		if (x is code)
		    return "#<code component>";
		if (x is simple_fun)
		    return "#<simple-fun "
			+ writeToString(x._name, false)
			+ ">";
/* 		if (x is instance) { */
/* 		    var layout : instance = instance(x._slots[0]); */
/* 		} */
		return x.toString();
	    } catch (e : Object) {
		var result : String
		      = "#<printer error " + e + " in " + x + ">";
		trace(result);
		if (e is Error)
		    trace(e.getStackTrace());
		return result;
	    }
	    return "wtf";
	}

	public function describe(x : Object) : void {
	    try {
		trace(writeToString(x, true));
		if (x is symbol) {
		    trace("  _value: " + x._value);
		    trace("  _hash: " + x._hash);
		    trace("  _plist: " + x._plist);
		    trace("  _name: " + x._name);
		    trace("  _package: " + x._package);
		} else if (x is vector)
		    trace("  _data: " + x._data);
		else if (x is code) {
		    trace("  _code_size: " + x._code_size);
		    trace("  _entry_points: " + x._entry_points);
		    trace("  _debug_info: " + x._debug_info);
		    trace("  _opcodes: " + x._opcodes.length);
		    trace("  _trace_table_offset: " + x._trace_table_offset);
		    trace("  _constants: " + x._constants);
		} else
		    trace(x);
	    } catch (e : Object) {
		trace("Error: " + e);
		trace(e.getStackTrace());
	    }
	}

	// fixme:
	protected var initial_fun : simple_fun;
	protected var swf_bytes : ByteArray;

	public function loadCoreFile() : void {
	    trace("//loading core file");
	    var a : int = getTimer(); 
	    var file : File
		= new File("/home/david/src/sbavm2/output/cold-sbcl.core");
	    var stream : FileStream = new FileStream();
	    stream.open(file, FileMode.READ);
	    var x : Array = new XMFReader(stream).read() as Array;
	    var b : int = getTimer();
	    trace("//done in " + (b - a) / 1000 + "s");
	    initial_fun = x[0] as simple_fun;
	    swf_bytes = x[3] as ByteArray;
	    trace("//length of swf data: " + swf_bytes.length);
	}

	public function loadCoreSwf() : void {
	    var c:LoaderContext = new LoaderContext();

	    c.applicationDomain
		= new ApplicationDomain(ApplicationDomain.currentDomain);
	    c.allowLoadBytesCodeExecution = true;

	    var loader : Loader = new Loader();

	    var handler : Function = function(event : Event) : void {
		trace("event: " + event);
	    }

	    loader.contentLoaderInfo.addEventListener(
		Event.INIT, handler);
	    loader.contentLoaderInfo.addEventListener(
		Event.COMPLETE, handler);
	    loader.contentLoaderInfo.addEventListener(
		IOErrorEvent.IO_ERROR, handler);
	    loader.contentLoaderInfo.addEventListener(
		SecurityErrorEvent.SECURITY_ERROR, handler);

	    loader.loadBytes(swf_bytes, c);
	}


	public function Runtime() {
	    try {
		loadCoreFile();
		loadCoreSwf();

/* 		trace("//FUN"); */
/* 		describe(fun); */
/* 		trace("//CODE"); */
/* 		describe(fun._code_component); */

		trace("//fell through");
	    } catch (e : Object) {
		trace("Error: " + e);
		trace(e.getStackTrace());
	    }
	}

/* 	NativeApplication.nativeApplication.exit(1); */
    }
}
