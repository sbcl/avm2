// -*- mode: c; c-basic-offset: 4 -*-

package org.sbcl {
    import flash.filesystem.FileStream;
    import flash.filesystem.FileMode;
    import flash.filesystem.File;
    import flash.utils.getDefinitionByName;
    import flash.utils.ByteArray;

    public class XMFReader {
	protected var stdout : FileStream;

	protected var stream : FileStream;
	protected var objects : Array;

	protected var linking_dummy_array : array;
	protected var linking_dummy_bignum : bignum;
	protected var linking_dummy_binding : binding;
	protected var linking_dummy_catch_block : catch_block;
	protected var linking_dummy_character : character;
	protected var linking_dummy_closure : closure;
	protected var linking_dummy_code : code;
	protected var linking_dummy_complex : complex;
	protected var linking_dummy_complex_double_float : complex_double_float;
	protected var linking_dummy_complex_single_float : complex_single_float;
	protected var linking_dummy_cons : cons;
	protected var linking_dummy_double_float : double_float;
	protected var linking_dummy_fdefn : fdefn;
	protected var linking_dummy_fixnum : fixnum;
	protected var linking_dummy_funcallable_instance : funcallable_instance;
	protected var linking_dummy_instance : instance;
	protected var linking_dummy_ratio : ratio;
	protected var linking_dummy_return_pc : return_pc;
	protected var linking_dummy_sap : sap;
	protected var linking_dummy_simple_fun : simple_fun;
	protected var linking_dummy_single_float : single_float;
	protected var linking_dummy_symbol : symbol;
	protected var linking_dummy_thread : thread;
	protected var linking_dummy_unbound_marker : unbound_marker;
	protected var linking_dummy_unwind_block : unwind_block;
	protected var linking_dummy_value_cell : value_cell;
	protected var linking_dummy_vector : vector;
	protected var linking_dummy_weak_pointer : weak_pointer;

	public function XMFReader(stream : FileStream) {
	    this.stdout = new FileStream();
	    this.stdout.open(new File("/dev/fd/1"), FileMode.WRITE);
	    this.stream = stream;
	}

	public function readString() : String {
	    return stream.readUTFBytes(stream.readUnsignedInt());
	}

	public function readNextSlot() : Object {
	    return objects[stream.readUnsignedInt()];
	}

	public function read() : Object {
	    var nclasses : uint = stream.readUnsignedInt();
	    var classes : Array = new Array(nclasses);
	    for (var i : uint = 0; i < nclasses; i++)
		classes[i] = getDefinitionByName(readString()) as Class;
	    var nobjects : uint = stream.readUnsignedInt();
	    objects = new Array(nobjects);
	    for (i = 0; i < nobjects; i++) {
		var tag : uint = stream.readByte();
		switch (tag) {
		case 1:
		    objects[i] = stream.readInt();
		    break;
		case 2:
		    objects[i] = stream.readUnsignedInt();
		    break;
		case 3:
		    objects[i] = stream.readDouble();
		    break;
		case 4:
		    objects[i] = readString();
		    break;
		case 5:
		    var nelements : uint = stream.readUnsignedInt();
		    objects[i] = new Array(nelements);
		    break;
		case 6:
		    var nbytes : uint = stream.readUnsignedInt();
		    objects[i] = new ByteArray();
		    stream.readBytes(objects[i], 0, nbytes);
		    break;
		case 7:
		    var classIndex : byte = stream.readByte();
		    objects[i] = new classes[classIndex];
		    break;
		default:
		    throw "invalid tag";
		}
	    }
	    for (i = 0; i < nobjects; i++) {
		var x : int = stream.readByte();
		if (x != 42)
		    throw "42 !=" + x + " at object " + i
			+ ", position" + stream.position;
		var a : Object = objects[i] as Array;
		if (a != null) {
		    for (var j : uint = 0; j < a.length; j++) {
			var ptr : uint = stream.readUnsignedInt();
			a[j] = objects[ptr];
		    }
		} else {
		    var l : Object = objects[i] as LispObject;
		    if (l != null)
			l.initFromStream(this);
		}
	    }
	    return objects[0];
	}
    }
}
