#!/bin/sh
set -e

# portable argv[0] handling routine, taken from clbuild
readlink_e() {
    self="$0"
    while test -h "$self"; do
	cd "$(dirname $self)"
	self=`readlink "$self"`
    done
    cd "$(dirname $self)"
    pwd
}

export SBCL_AVM2_HOME=$(readlink_e)

exec adl $SBCL_AVM2_HOME/vmruntime/sbcl.xml
